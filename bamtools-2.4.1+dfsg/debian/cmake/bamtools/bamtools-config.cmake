# - Config file for the BamTools package
#################################################
#  Found at
#    https://bitbucket.org/lunacab/ccdeep/raw/229ad7adb3700eec272a6cce36966b458bdaaa7a/bamtools-config.cmake
#  Installed to /usr/share/cmake/bamtools
#    according to the advise given in #761516 which links to the doc
#     http://www.cmake.org/cmake/help/v2.8.12/cmake.html#command%3afind_package
#  Andreas Tille <tille@debian.org>
#################################################
# It defines the following variables
#  bamtools_FOUND - System has bamtols
#  bamtools_INCLUDE_DIRS - include directories for BamTools
#  bamtools_LIBRARIES    - libraries to link against
#  bamtools_EXECUTABLE   - the bar executable
 

include(FindPackageHandleStandardArgs)
find_path(bamtools_INCLUDE_DIR api/BamReader.h 
                  HINTS ENV BAMTOOLS_INC
		  PATH_SUFFIXES bamtools
		  )

find_library(bamtools_LIBRARY NAMES bamtools
             HINTS ${CMAKE_SYSTEM_LIB_PATH}
             PATH_SUFFIXES bamtools
             )

get_filename_component(bamtools_LIB_DIR ${bamtools_LIBRARY}
                       PATH)
                       
set(bamtools_LIBRARIES ${bamtools_LIB_DIR})
set(bamtools_INCLUDE_DIRS ${bamtools_INCLUDE_DIR})
get_filename_component(bamtools_REAL_LIBRARY ${bamtools_LIBRARY} REALPATH)
get_filename_component(bamtools_LIB_FILE ${bamtools_REAL_LIBRARY} NAME)

find_package_handle_standard_args(bamtools  DEFAULT_MSG
                                  bamtools_LIBRARY bamtools_INCLUDE_DIR)

mark_as_advanced(bamtools_LIBRARY bamtools_INCLUDE_DIR )

