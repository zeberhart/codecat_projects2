/*
 * This file is part of GtkHotkey.
 * Copyright Mikkel Kamstrup Erlandsen, March, 2008
 *
 *   GtkHotkey is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   GtkHotkey is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with GtkHotkey.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <glib.h>
#include <glib-object.h>
#import <glib/gtestutils.h>

#import "src/gtk-hotkey-registry.c"
#import "src/gtk-hotkey-key-file-registry.c"

/* Shortcut to defining a test */
#define TEST(func) g_test_add ("/registry/"#func, Fixture, NULL, (void (*) (Fixture*, gconstpointer))setup, test_##func, teardown);

#define TEST_APP "gtk-hotkey-test"
#define TEST_KEY "gtk-hotkey-test-key"
#define TEST_SIGN "<Alt>F3"
#define TEST_SIGN_ALT "<Control><Shift>F3"

/* Test fixture passed to all tests */
typedef struct {
	GtkHotkeyRegistry	*registry;
	gboolean			was_stored_emitted;
	gboolean			was_deleted_emitted;
	gulong				stored_handle;
	gulong				deleted_handle;
} Fixture;

void
hotkey_stored_cb (GtkHotkeyRegistry	*storage,
				  GtkHotkeyInfo		*info,
				  gpointer			*data);

void
hotkey_stored_cb (GtkHotkeyRegistry	*storage,
				  GtkHotkeyInfo		*info,
				  gpointer			*data)
{
	Fixture *fix;
	
	g_return_if_fail (GTK_HOTKEY_IS_REGISTRY(storage));
	g_return_if_fail (GTK_HOTKEY_IS_INFO(info));
	g_return_if_fail (data != NULL);
	
	fix = (Fixture*) data;
	fix->was_stored_emitted = TRUE;
}

static void
hotkey_deleted_cb (GtkHotkeyRegistry	*storage,
				   GtkHotkeyInfo	*info,
				   gpointer			*data)
{
	Fixture *fix;
	
	g_return_if_fail (GTK_HOTKEY_IS_REGISTRY(storage));
	g_return_if_fail (GTK_HOTKEY_IS_INFO(info));
	g_return_if_fail (data != NULL);
	
	fix = (Fixture*) data;
	fix->was_deleted_emitted = TRUE;
}

static void
setup (Fixture		 *fix,
	   gconstpointer test_data)
{
	fix->registry = gtk_hotkey_registry_get_default ();
	fix->was_stored_emitted = FALSE;
	fix->was_deleted_emitted = FALSE;
	
	g_assert (GTK_HOTKEY_IS_REGISTRY(fix->registry));
	
	fix->stored_handle = g_signal_connect (fix->registry, "hotkey-stored",
										   G_CALLBACK(hotkey_stored_cb), fix);
	fix->deleted_handle = g_signal_connect (fix->registry, "hotkey-deleted",
											G_CALLBACK(hotkey_deleted_cb), fix);
}

static void
teardown (Fixture		*fix,
		  gconstpointer test_data)
{
	g_signal_handler_disconnect (fix->registry, fix->stored_handle);
	g_signal_handler_disconnect (fix->registry, fix->deleted_handle);
	
	g_object_unref (fix->registry);
}

static void
test_emit_stored (Fixture		*fix,
				  gconstpointer	test_data)
{
	GtkHotkeyInfo   *hotkey;
	
	hotkey = gtk_hotkey_info_new (TEST_APP, TEST_KEY, TEST_SIGN, NULL);
	gtk_hotkey_registry_hotkey_stored (fix->registry, hotkey);
	
	if (!fix->was_stored_emitted)
		g_critical ("'hotkey-stored' was not emitted by "
					"gtk_hotkey_registry_hotkey_stored()");
	
}

static void
test_emit_deleted (Fixture		*fix,
				  gconstpointer	test_data)
{
	GtkHotkeyInfo   *hotkey;
	
	hotkey = gtk_hotkey_info_new (TEST_APP, TEST_KEY, TEST_SIGN, NULL);
	gtk_hotkey_registry_hotkey_deleted (fix->registry, hotkey);
	
	if (!fix->was_deleted_emitted)
		g_critical ("'hotkey-deleted' was not emitted by "
					"gtk_hotkey_registry_hotkey_deleted()");
	
}

/**
 * Test that has_hotkey() returns false on bogus inpout
 */
static void
test_has_non_existing_hotkey	(Fixture		*fix,
								 gconstpointer	test_data)
{
	g_assert (!gtk_hotkey_registry_has_hotkey (fix->registry,
											  "foobar",
											  "blah"));
	g_assert (!fix->was_stored_emitted && !fix->was_deleted_emitted);
}

static void
test_get_non_existing_hotkey	(Fixture		*fix,
								 gconstpointer	test_data)
{
	/* Without GError */
	g_assert (gtk_hotkey_registry_get_hotkey (fix->registry, "foobar",
											 "blah", NULL) == NULL);
	g_assert (!fix->was_stored_emitted && !fix->was_deleted_emitted);
	
	/* With error */
	GError *error = NULL;
	g_assert (gtk_hotkey_registry_get_hotkey (fix->registry, "foobar",
											 "blah", &error) == NULL);
	g_assert (error != NULL);
	g_error_free (error);
	g_assert (!fix->was_stored_emitted && !fix->was_deleted_emitted);
}

static void
test_delete_non_existing_hotkey	(Fixture		*fix,
								 gconstpointer	test_data)
{
	/* Without GError */
	g_assert (!gtk_hotkey_registry_delete_hotkey (fix->registry, "foobar",
												 "blah", NULL));
	g_assert (!fix->was_stored_emitted && !fix->was_deleted_emitted);
	
	/* With error */
	GError *error = NULL;
	g_assert (!gtk_hotkey_registry_delete_hotkey (fix->registry, "foobar",
												"blah", &error));
	g_assert (error != NULL);
	g_error_free (error);
	g_assert (!fix->was_stored_emitted && !fix->was_deleted_emitted);
}

static void
test_save_load_single	(Fixture		*fix,
						 gconstpointer	test_data)
{
	GtkHotkeyInfo   *hotkey, *fookey;
	GError			*error;
	
	hotkey = gtk_hotkey_info_new (TEST_APP, TEST_KEY, TEST_SIGN, NULL);
	
	g_assert (!gtk_hotkey_registry_has_hotkey (fix->registry,
											  TEST_APP,
											  TEST_KEY));
	
	g_assert (!fix->was_stored_emitted && !fix->was_deleted_emitted);
	
	error = NULL;
	gtk_hotkey_registry_store_hotkey (fix->registry,
									 hotkey,
									 &error);
	if (error)
		g_critical ("Failed to store hotkey: %s", error->message);
	
	g_assert (fix->was_stored_emitted);
	
	g_assert (gtk_hotkey_registry_has_hotkey (fix->registry,
											 TEST_APP,
											 TEST_KEY));
	
	error = NULL;
	fookey = gtk_hotkey_registry_get_hotkey (fix->registry,
											TEST_APP, TEST_KEY, &error);
	
	if (error)
		g_critical ("Failed to read hotkey: %s", error->message);
	
	g_assert_cmpstr (TEST_APP, == , gtk_hotkey_info_get_application_id(fookey));
	g_assert_cmpstr (TEST_KEY, == , gtk_hotkey_info_get_key_id(fookey));
	g_assert_cmpstr (TEST_SIGN, == , gtk_hotkey_info_get_signature(fookey));
	
	error = NULL;
	gtk_hotkey_registry_delete_hotkey (fix->registry,
									  TEST_APP, TEST_KEY, &error);
	
	if (error)
		g_critical ("Failed to delete hotkey: %s", error->message);
	
	g_assert (fix->was_deleted_emitted);
	g_assert (!gtk_hotkey_registry_has_hotkey (fix->registry,
											  TEST_APP,
											  TEST_KEY));
	
	g_object_unref (hotkey);
	g_object_unref (fookey);
}

static void
test_hotkey_equals (Fixture		*fix,
					gconstpointer	test_data)
{
	GtkHotkeyInfo *k1, *k2;
	
	k1 = gtk_hotkey_info_new (TEST_APP, TEST_KEY, TEST_SIGN, NULL);
	k2 = gtk_hotkey_info_new (TEST_APP, TEST_KEY, TEST_SIGN, NULL);
	
	g_assert (gtk_hotkey_info_equals (k1, k1, TRUE));
	g_assert (gtk_hotkey_info_equals (k1, k1, FALSE));
	g_assert (gtk_hotkey_info_equals (k1, k2, TRUE));
	g_assert (gtk_hotkey_info_equals (k1, k2, FALSE));
	
	gtk_hotkey_info_set_description (k1, "Hello");
	g_assert (gtk_hotkey_info_equals (k1, k2, TRUE)); /* Still sloppy equality */
	g_assert (!gtk_hotkey_info_equals (k1, k2, FALSE)); /* No strict == */
	
	gtk_hotkey_info_set_description (k2, "Hello");
	g_assert (gtk_hotkey_info_equals (k1, k2, TRUE)); /* Still sloppy equality */
	g_assert (gtk_hotkey_info_equals (k1, k2, FALSE)); /* Also strict == */
	
	/* FIXME: Check with GAppInfo set too */
	
	g_object_unref (k1);
	g_object_unref (k2);
}

static void
test_get_application_hotkeys (Fixture		*fix,
							  gconstpointer	test_data)
{
	GtkHotkeyInfo   *hotkey, *fookey;
	GList			*hotkeys;
	GError			*error;
	
	hotkey = gtk_hotkey_info_new (TEST_APP, TEST_KEY"1", TEST_SIGN, NULL);
	fookey = gtk_hotkey_info_new (TEST_APP, TEST_KEY"2", TEST_SIGN_ALT, NULL);
	
	/* Store first hotkey */
	error = NULL;
	gtk_hotkey_registry_store_hotkey (fix->registry,
									 hotkey,
									 &error);
	if (error) g_critical ("Failed to store key 1: %s", error->message);
	
	/* Store second hotkey */
	error = NULL;
	gtk_hotkey_registry_store_hotkey (fix->registry,
									 fookey,
									 &error);
	if (error) g_critical ("Failed to store key 2: %s", error->message);
	
	/* Retrieve all hotkeys */
	error = NULL;
	hotkeys = gtk_hotkey_registry_get_application_hotkeys (fix->registry, TEST_APP,
														 &error);
	if (error) g_critical ("Failed to get application hotkeys : %s",
						   error->message);
	
	g_assert_cmpint (g_list_length (hotkeys), == , 2);
	g_list_foreach (hotkeys, (GFunc)g_object_unref, NULL);
	g_list_free (hotkeys);
	
	/* Delete the first of the hotkeys */
	error = NULL;
	gtk_hotkey_registry_delete_hotkey (fix->registry,
									  TEST_APP, TEST_KEY"1", &error);
	
	if (error) g_critical ("Failed to delete first hotkey: %s", error->message);
	
	/* Assert that we have exactly one hotkey and that it is the right one */
	error = NULL;
	hotkeys = gtk_hotkey_registry_get_application_hotkeys (fix->registry, TEST_APP,
														 &error);
	if (error) g_critical ("Failed to get application hotkeys : %s",
						   error->message);
	
	g_assert_cmpint (g_list_length (hotkeys), == , 1);
	g_assert (gtk_hotkey_info_equals (fookey, GTK_HOTKEY_INFO(hotkeys->data), FALSE));
	
	g_list_foreach (hotkeys, (GFunc)g_object_unref, NULL);
	g_list_free (hotkeys);
	
	/* Clean up */
	/* Delete the first of the hotkeys */
	gtk_hotkey_registry_delete_hotkey (fix->registry,
									  TEST_APP, TEST_KEY"2", NULL);
	g_assert (!gtk_hotkey_registry_has_hotkey (fix->registry,
											  TEST_APP, TEST_KEY"2"));
	
	g_object_unref (hotkey);
	g_object_unref (fookey);
}

static void
test_get_all_hotkeys (Fixture		*fix,
					  gconstpointer	test_data)
{
	GtkHotkeyInfo   *hotkey, *fotkey, *botkey;
	GList			*hotkeys;
	GError			*error;
	
	g_printf ("This test will fail unless you have deleted all your hotkeys\n");	
		
	hotkey = gtk_hotkey_info_new (TEST_APP"1", TEST_KEY"1", TEST_SIGN, NULL);
	fotkey = gtk_hotkey_info_new (TEST_APP"1", TEST_KEY"2", TEST_SIGN_ALT, NULL);
	botkey = gtk_hotkey_info_new (TEST_APP"2", TEST_KEY"1", TEST_SIGN_ALT, NULL);
	
	/* Store first hotkey */
	error = NULL;
	gtk_hotkey_registry_store_hotkey (fix->registry,
									 hotkey,
									 &error);
	if (error) g_critical ("Failed to store key 1: %s", error->message);
	
	/* Store second hotkey */
	error = NULL;
	gtk_hotkey_registry_store_hotkey (fix->registry,
									 fotkey,
									 &error);
	if (error) g_critical ("Failed to store key 2: %s", error->message);
	
	/* Store third hotkey */
	error = NULL;
	gtk_hotkey_registry_store_hotkey (fix->registry,
									 botkey,
									 &error);
	if (error) g_critical ("Failed to store key 3: %s", error->message);
	
	/* Retrieve all hotkeys */
	error = NULL;
	hotkeys = gtk_hotkey_registry_get_all_hotkeys (fix->registry);
	
	g_assert_cmpint (g_list_length (hotkeys), == , 3);
	g_list_foreach (hotkeys, (GFunc)g_object_unref, NULL);
	g_list_free (hotkeys);
	
	/* Delete the first of the hotkeys */
	error = NULL;
	gtk_hotkey_registry_delete_hotkey (fix->registry,
									  TEST_APP"1", TEST_KEY"1", &error);
	
	if (error) g_critical ("Failed to delete first hotkey: %s", error->message);
	
	/* Assert that we have two hotkeys left */
	error = NULL;
	hotkeys = gtk_hotkey_registry_get_all_hotkeys (fix->registry);
	
	g_assert_cmpint (g_list_length (hotkeys), == , 2);
	g_list_foreach (hotkeys, (GFunc)g_object_unref, NULL);
	g_list_free (hotkeys);
	
	/* Clean up */
	gtk_hotkey_registry_delete_hotkey (fix->registry,
									  TEST_APP"1", TEST_KEY"2", NULL);
	g_assert (!gtk_hotkey_registry_has_hotkey (fix->registry,
											  TEST_APP"1", TEST_KEY"2"));
	
	gtk_hotkey_registry_delete_hotkey (fix->registry,
									  TEST_APP"2", TEST_KEY"1", NULL);
	g_assert (!gtk_hotkey_registry_has_hotkey (fix->registry,
											  TEST_APP"2", TEST_KEY"1"));
	
	g_object_unref (hotkey);
	g_object_unref (fotkey);
	g_object_unref (botkey);
	
}

int
main (int   argc,
      char *argv[])
{
	g_type_init ();
	g_test_init (&argc, &argv, NULL);

	g_test_bug_base ("http://bugzilla.gnome.org/");
	TEST (emit_stored);
	TEST (emit_deleted);
	TEST (has_non_existing_hotkey);
	TEST (get_non_existing_hotkey);
	TEST (delete_non_existing_hotkey);
	TEST (save_load_single);
	TEST (hotkey_equals);
	TEST (get_application_hotkeys);
	TEST (get_all_hotkeys);
	
	
  return g_test_run();
}
