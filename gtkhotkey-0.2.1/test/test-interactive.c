/*
 * This file is part of GtkHotkey.
 * Copyright Mikkel Kamstrup Erlandsen, March, 2008
 *
 *   GtkHotkey is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   GtkHotkey is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with GtkHotkey.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gtk/gtk.h>
#include <glib-object.h>
#include <glib.h>
#include <gio/gio.h>

#include "src/gtk-hotkey-info.h"

static gchar*	opt_signature = NULL;

static GOptionEntry option_entries[] = {
	{"signature", 's', 0, G_OPTION_ARG_STRING, &opt_signature,
	 "Set the signature of the key to bind"},
	{NULL}
};

/* Global var to check whether notify::bound was emitted */
gboolean got_bound_notify = FALSE;
static void
hotkey_bound_cb (GtkHotkeyInfo *hotkey, GParamSpec *pspec, gpointer data)
{
	g_return_if_fail (GTK_HOTKEY_IS_INFO(hotkey));
	g_return_if_fail (G_IS_PARAM_SPEC(pspec));
	
	if (g_str_equal (pspec->name, "bound")) {
		got_bound_notify = TRUE;
	} else {
		g_warning ("Got unexpected notify for property '%s'", pspec->name);
	}
}


static void
hotkey_activated_cb (GtkHotkeyInfo *hotkey, guint event_time, gpointer data)
{
	GError	*error;
	
	g_return_if_fail (GTK_HOTKEY_IS_INFO(hotkey));
	
	g_printf ("Hotkey %s activated with event time %d\n",
			  gtk_hotkey_info_get_signature(hotkey), event_time);
	
	got_bound_notify = FALSE;
	error = NULL;
	if (!gtk_hotkey_info_unbind (hotkey, &error)) {
		g_critical ("Failed to unbind hotkey %s: %s",
					gtk_hotkey_info_get_signature(hotkey),
					error->message);
		g_error_free (error);
		return;
	}
	
	g_assert (!gtk_hotkey_info_is_bound (hotkey));
	g_assert (got_bound_notify);
	
	g_printf ("Hotkey succesfully unbound\n");
	
	gtk_main_quit ();
}

int
main (int argc, char *argv[])
{
	GError *error;
	GOptionContext *options;
	gchar *signature;
	GtkHotkeyInfo   *hot;
	
	gtk_init (&argc, &argv);
	
	/* Set up and parse options */
	options = g_option_context_new (NULL);
	g_option_context_add_main_entries (options, option_entries,
									   GETTEXT_PACKAGE);
	g_option_context_add_group (options, gtk_get_option_group (TRUE));
	
	error = NULL;
	if (!g_option_context_parse (options, &argc, &argv, &error))
	{
		g_print ("Bad command line: %s\n", error->message);
		return 1;
	}
	
	/* Take recognized options into account */
	if (opt_signature) {
		signature = opt_signature;
	} else {
		signature  = g_strdup("<Control><Shift>1");
	}
	g_printf ("Using signature: %s\n", signature);
	
	/* Actual program */
	hot = gtk_hotkey_info_new ("gtkhotkey-test", "gtkhotkey-test-key",
							   signature, NULL);
	g_assert (GTK_HOTKEY_IS_INFO(hot));
	g_assert (!gtk_hotkey_info_is_bound(hot));
	
	g_signal_connect (hot, "notify::bound", G_CALLBACK(hotkey_bound_cb), NULL);
	
	error = NULL;
	gtk_hotkey_info_bind (hot, &error);
	g_signal_connect (hot, "activated", G_CALLBACK(hotkey_activated_cb),
							  "W00t - main.c caught key\n");
	if (error) {
		g_critical ("Got error: %s", error->message);
		g_error_free (error);
	}
	
	g_assert (gtk_hotkey_info_is_bound(hot));
	g_assert (got_bound_notify);
	
	guint accel_key;
	GdkModifierType mod_type;
	gtk_accelerator_parse (signature, &accel_key, &mod_type);
	
	g_printf ("GtkAccel:\n\tkey: %d\n\tmod: %d\n%d\n", accel_key, mod_type, (2&2));
	
	gtk_main();
	
	g_free (signature);
	
	return 0;
}
