/*
 * This file is part of GtkHotkey.
 * Copyright Mikkel Kamstrup Erlandsen, March, 2008
 *
 *   GtkHotkey is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   GtkHotkey is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with GtkHotkey.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gtk/gtk.h>
#include <glib-object.h>
#include <glib.h>
#include <gio/gio.h>

#include "src/gtk-hotkey-info.h"

static void
hotkey_activated_cb (GtkHotkeyInfo *hotkey, guint event_time, gpointer data)
{
	GError	*error;
	
	g_return_if_fail (GTK_HOTKEY_IS_INFO(hotkey));
	
	g_printf ("Hotkey %s activated with event time %d\n",
			  gtk_hotkey_info_get_signature(hotkey), event_time);
}

int
main (int argc, char *argv[])
{
	GError *error;
	GtkHotkeyInfo   *hot;
	
	gtk_init (&argc, &argv);
	
	/* Hotkey 1 */
	hot = gtk_hotkey_info_new ("gtkhotkey-test", "gtkhotkey-test-key-1",
							   "<Super>1", NULL);
	error = NULL;
	gtk_hotkey_info_bind (hot, &error);
	if (error) {
		g_critical ("Got error: %s", error->message);
		g_error_free (error);
	}	
	g_signal_connect (hot, "activated", G_CALLBACK(hotkey_activated_cb), NULL);
	
	/* Hotkey 2 */
	hot = gtk_hotkey_info_new ("gtkhotkey-test", "gtkhotkey-test-key-2",
							   "<Super>2", NULL);
	error = NULL;
	gtk_hotkey_info_bind (hot, &error);
	if (error) {
		g_critical ("Got error: %s", error->message);
		g_error_free (error);
	}	
	g_signal_connect (hot, "activated", G_CALLBACK(hotkey_activated_cb), NULL);
	
	/* Hotkey 3 */
	hot = gtk_hotkey_info_new ("gtkhotkey-test", "gtkhotkey-test-key-3",
							   "<Super>3", NULL);
	error = NULL;
	gtk_hotkey_info_bind (hot, &error);
	if (error) {
		g_critical ("Got error: %s", error->message);
		g_error_free (error);
	}	
	g_signal_connect (hot, "activated", G_CALLBACK(hotkey_activated_cb), NULL);
	
	g_printf ("Interactive test ready\n");
	
	gtk_main();
	
	return 0;
}
