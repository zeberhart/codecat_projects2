libbind_cflags=""
libbind_ldflags=""
libbind_libs="-lresolv"

AC_SUBST([libbind_cflags])
AC_SUBST([libbind_ldflags])
AC_SUBST([libbind_libs])

AC_DEFINE([HAVE_LIBBIND], [1], [Define to 1 if libbind works.])
