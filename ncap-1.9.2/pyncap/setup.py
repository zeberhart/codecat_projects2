from distutils.core import setup
from distutils.extension import Extension
from Pyrex.Distutils import build_ext
setup(
  name = "ncap",
  ext_modules=[ 
    Extension("ncap", ["ncap.pyx", "wrap.c"],
              libraries = ["ncap"],
              library_dirs = ["../.libs"],
              include_dirs = [".."]
              )
    ],
  cmdclass = {'build_ext': build_ext}
)
