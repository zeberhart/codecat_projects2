// AUTOMATICALLY GENERATED -- DO NOT EDIT
%ignore identity;
%ignore iveccyl;
%ignore mirror;
%ignore one_ivec;
%ignore one_vec;
%ignore rotate2;
%ignore rotate4;
%ignore r_to_minus_r_symmetry;
%ignore veccyl;
%ignore vol1d;
%ignore vol2d;
%ignore vol3d;
%ignore volcyl;
%ignore volone;
%ignore voltwo;
%ignore zero_ivec;
%ignore zero_vec;
