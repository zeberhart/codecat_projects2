/*
 * Copyright (c) Members of the IGE Project 2012
 * See http://ige-project.eu for details on the copyright holders.
 * For license conditions see the license file
 */

/*
 * Copyright (c) Members of the EGEE Collaboration. 2004.
 * See http://eu-egee.org/partners/ for details on the copyright holders.
 * For license conditions see the license file or
 * http://eu-egee.org/license.html
 */

/*
 * Copyright (c) 2001 EU DataGrid.
 * For license conditions see http://www.eu-datagrid.org/license.html
 *
 * Copyright (c) 2001, 2002, 2003 by
 *     Oscar Koeroo <okoeroo@nikhef.nl>
 *     NIKHEF Amsterdam, the Netherlands
 */

/*****************************************************************************
                            Include header files
******************************************************************************/

#include "jobrep_odbc_api.h"


/******************************************************************************
                                Definitions
******************************************************************************/

/******************************************************************************
                          Module specific prototypes
******************************************************************************/

static int
ODBC_Errors (struct jr_db_handle_s *, short, char *);

/******************************************************************************
                       Define module specific variables
******************************************************************************/

int
SQL_IgnoreErrors_Set(struct jr_db_handle_s *db_handle) {
    if (db_handle == NULL) {
        lcmaps_log(LOG_ERR, "%s called without a database handle.\n", __func__);
        return -1;
    }
    db_handle->ignore_errors = 1;
    return 0;
}

int
SQL_IgnoreErrors_Reset(struct jr_db_handle_s *db_handle) {
    if (db_handle == NULL) {
        lcmaps_log(LOG_ERR, "%s called without a database handle.\n", __func__);
        return -1;
    }
    db_handle->ignore_errors = 0;
    return 0;
}


static int
ODBC_Errors (struct jr_db_handle_s *db_handle, short last_error, char *where) {
    unsigned char  buf[250];
    unsigned char  sqlstate[15];
    unsigned char *search;
    size_t  i = 0;

    if (where) {
        lcmaps_log(LOG_ERR, "Error detected in %s with return code: %s(%d)\n",
                            where, SQL_ERROR_CODE_STRING(last_error), last_error);
    } else {
        lcmaps_log(LOG_ERR, "Error detected with return code: %s(%d)\n",
                            SQL_ERROR_CODE_STRING(last_error), last_error);
    }

    if (db_handle == NULL) {
        lcmaps_log(LOG_ERR, "%s: ODBC_Errors() called without a database handle.\n", __func__);
        return 0;
    }

    /* Get statement errors */
    if (db_handle->jobrep_henv && db_handle->jobrep_hdbc && db_handle->jobrep_hstmt) {
        while (SQLError (db_handle->jobrep_henv, db_handle->jobrep_hdbc, db_handle->jobrep_hstmt,
                         sqlstate, NULL, buf, sizeof(buf), NULL) == SQL_SUCCESS) {
            search = buf;
            for (i = 0; i < sizeof(buf); i++) {
                if (strncmp((char *)search, "Duplicate", strlen("Duplicate")) == 0) {
                    return 1;
                } else {
                    search++;
                }
            }
            lcmaps_log(LOG_ERR, "[SQL/Statement Error @ %s : %s]%s, SQLSTATE=%s\n",
                                where, SQL_ERROR_CODE_STRING(last_error), buf, sqlstate);
        }
    }

    /* Get connection errors */
    if (db_handle->jobrep_henv && db_handle->jobrep_hdbc) {
        while (SQLError (db_handle->jobrep_henv, db_handle->jobrep_hdbc, SQL_NULL_HSTMT,
                         sqlstate, NULL, buf, sizeof(buf), NULL) == SQL_SUCCESS) {
            lcmaps_log(LOG_ERR, "[Connection Error @ %s : %s]%s, SQLSTATE=%s\n",
                                where, SQL_ERROR_CODE_STRING(last_error), buf, sqlstate);
        }
    }

    /* Get environmental errors */
    if (db_handle->jobrep_henv) {
        while (SQLError (db_handle->jobrep_henv, SQL_NULL_HDBC, SQL_NULL_HSTMT,
                         sqlstate, NULL, buf, sizeof(buf), NULL) == SQL_SUCCESS) {
            lcmaps_log(LOG_ERR, "[Environmental Error @ %s : %s]%s, SQLSTATE=%s\n",
                                where, SQL_ERROR_CODE_STRING(last_error), buf, sqlstate);
        }
    }
    return 0;
}

int
ODBC_Test (char *dsn, char *username, char *password)
{
    struct jr_db_handle_s *db_handle = NULL;
    db_handle = ODBC_Connect (dsn, username, password);
    if (db_handle == NULL) {
        return -1;
    }
    lcmaps_log_debug(LOG_INFO, "%s: Successfully connected to DSN \"%s\"\n", __func__, dsn);

    if (db_handle->jobrep_connected) {
        if (ODBC_Disconnect(db_handle) < 0) {
            lcmaps_log(LOG_ERR, "%s: Failed to gracefully disconnect from DSN \"%s\"\n", __func__, dsn);
            return -1;
        }
    }
    return 0;
}

struct jr_db_handle_s *
ODBC_Connect (char *dsn, char *username, char *password) {
    SQLRETURN V_OD_erg = 0;
    unsigned char V_OD_msg[200];
    unsigned char V_OD_stat[10];
    SQLINTEGER V_OD_err;
    SQLSMALLINT V_OD_mlen;
    struct jr_db_handle_s *jr_db_handle = NULL;

    /* Allocate */
    jr_db_handle = malloc(sizeof(struct jr_db_handle_s));
    if (jr_db_handle == NULL) {
        lcmaps_log(LOG_ERR, "%s: Could not allocate %u bytes for the DB handle.\n",
                            __func__, sizeof(struct jr_db_handle_s));
        goto failure;
    }
    /* Cleanup */
    memset(jr_db_handle, 0, sizeof(struct jr_db_handle_s));

    V_OD_erg = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &(jr_db_handle->jobrep_henv));
    if ((V_OD_erg != SQL_SUCCESS) && (V_OD_erg != SQL_SUCCESS_WITH_INFO)) {
            lcmaps_log(LOG_ERR, "%s: Error AllocHandle\n", __func__);
            goto failure;
    }

    V_OD_erg = SQLSetEnvAttr(jr_db_handle->jobrep_henv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0);
    if ((V_OD_erg != SQL_SUCCESS) && (V_OD_erg != SQL_SUCCESS_WITH_INFO)) {
            lcmaps_log(LOG_ERR, "%s: Error SetEnv\n", __func__);
            SQLFreeHandle(SQL_HANDLE_ENV, jr_db_handle->jobrep_henv);
            goto failure;
    }

    V_OD_erg = SQLAllocHandle(SQL_HANDLE_DBC, jr_db_handle->jobrep_henv, &(jr_db_handle->jobrep_hdbc));
    if ((V_OD_erg != SQL_SUCCESS) && (V_OD_erg != SQL_SUCCESS_WITH_INFO)) {
            lcmaps_log(LOG_ERR, "%s: Error AllocHDB\n", __func__);
            SQLFreeHandle(SQL_HANDLE_ENV, jr_db_handle->jobrep_henv);
            goto failure;
    }

    SQLSetConnectAttr(jr_db_handle->jobrep_hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER *)5, 0);
    V_OD_erg = SQLConnect(jr_db_handle->jobrep_hdbc,
                          (SQLCHAR*) dsn, SQL_NTS,
                          (SQLCHAR*) username, SQL_NTS,
                          (SQLCHAR*) password, SQL_NTS);
    if ((V_OD_erg != SQL_SUCCESS) && (V_OD_erg != SQL_SUCCESS_WITH_INFO)) {
            lcmaps_log(LOG_ERR, "%s: Error SQLConnect\n", __func__);
            SQLGetDiagRec(SQL_HANDLE_DBC, jr_db_handle->jobrep_hdbc, 1,
                          V_OD_stat, &V_OD_err, V_OD_msg, sizeof(V_OD_msg), &V_OD_mlen);
            ODBC_Errors(jr_db_handle, V_OD_erg, (char *)V_OD_msg);
            if (jr_db_handle->jobrep_henv) {
                SQLFreeHandle(SQL_HANDLE_ENV, jr_db_handle->jobrep_henv);
            }
            goto failure;
    }

#if 0
    status = SQLDriverConnect (jobrep_hdbc, 0, (UCHAR *) dataSource, SQL_NTS, (UCHAR *) buf, sizeof (buf), &buflen, SQL_DRIVER_COMPLETE);

    if (status != SQL_SUCCESS && status != SQL_SUCCESS_WITH_INFO)
        return -4;

    // Turn AUTOCOMMIT off so SQL statements can be grouped into a
    // transaction
    status = SQLSetConnectOption(jobrep_hdbc, SQL_AUTOCOMMIT, SQL_AUTOCOMMIT_OFF);
    if (status != SQL_SUCCESS)
    {
        fprintf(stderr, "Unable to turn AUTOCOMMIT off (status=%d)\n", status);
        return -5;
    }

    //SQLSetConnectOption (hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TRANSACTION_SERIALIZABLE);
    //SQLSetConnectOption (hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TRANSACTION_REPEATABLE_READ);
    SQLSetConnectOption (jobrep_hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TRANSACTION_READ_UNCOMMITTED);
    //SQLSetConnectOption (hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TXN_READ_COMMITTED);

    SQLSetConnectOption (jobrep_hdbc, SQL_OPT_TRACEFILE, (UDWORD) "\\SQL.LOG");
    jobrep_connected = 1;
#endif

    jr_db_handle->jobrep_connected = 1;
    return jr_db_handle;

failure:
    if (jr_db_handle) {
        free(jr_db_handle);
    }
    return NULL;
}

int
ODBC_Disconnect(struct jr_db_handle_s *db_handle) {
    if (db_handle == NULL) {
        return -1;
    }

    /* Close and clean up everything */
    SQL_QueryCloseAndClean(db_handle);

    if (db_handle->jobrep_connected) {
        SQLDisconnect(db_handle->jobrep_hdbc);
        db_handle->jobrep_connected = 0;
    }

    if (db_handle->jobrep_hdbc)
        SQLFreeHandle(SQL_HANDLE_DBC, db_handle->jobrep_hdbc);

    if (db_handle->jobrep_henv)
        SQLFreeHandle(SQL_HANDLE_ENV, db_handle->jobrep_henv);

    free(db_handle);
    db_handle = NULL;

    return 0;
}

int
SQL_BeginTransaction(struct jr_db_handle_s *db_handle) {
    SQLRETURN status = 0;

    if (db_handle == NULL) {
        return -1;
    }

    status = SQLSetConnectOption(db_handle->jobrep_hdbc, SQL_AUTOCOMMIT, SQL_AUTOCOMMIT_OFF);
    if (status != SQL_SUCCESS) {
        lcmaps_log(LOG_ERR, "%s: Unable to turn AUTOCOMMIT off (status=%d)\n", __func__, status);
        ODBC_Errors(db_handle, status, "SQLSetConnectOption");
        return -2;
    }

    /* Set use_transactions */
    db_handle->use_transactions = 1;

    status = SQLSetConnectOption(db_handle->jobrep_hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TRANSACTION_READ_UNCOMMITTED);
    if (status != SQL_SUCCESS) {
        lcmaps_log(LOG_ERR, "%s: Unable to enable the isolation option SQL_TRANSACTION_READ_UNCOMMITTED\n", __func__);
        ODBC_Errors(db_handle, status, "SQLSetConnectOption");
        return -2;
    }

    return 0;
#if 0
    status = SQLDriverConnect (jobrep_hdbc, 0, (UCHAR *) dataSource, SQL_NTS, (UCHAR *) buf, sizeof (buf), &buflen, SQL_DRIVER_COMPLETE);

    status = SQLSetConnectOption(jobrep_hdbc, SQL_AUTOCOMMIT, SQL_AUTOCOMMIT_OFF);
    if (status != SQL_SUCCESS && status != SQL_SUCCESS_WITH_INFO)
        return -4;

    // Turn AUTOCOMMIT off so SQL statements can be grouped into a
    // transaction

    //SQLSetConnectOption (hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TRANSACTION_SERIALIZABLE);
    //SQLSetConnectOption (hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TRANSACTION_REPEATABLE_READ);
    SQLSetConnectOption (jobrep_hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TRANSACTION_READ_UNCOMMITTED);
    //SQLSetConnectOption (hdbc, SQL_TXN_ISOLATION_OPTION, SQL_TXN_READ_COMMITTED);

    SQLSetConnectOption (jobrep_hdbc, SQL_OPT_TRACEFILE, (UDWORD) "\\SQL.LOG");
    jobrep_connected = 1;
#endif
}

/******************************************************************************
Function:   SQL_Commit
Description:
    Commits the active transaction
Parameters:
Returns:
    SQL_SUCCESS : succes (no results)
   !SQL_SUCCESS : failure
******************************************************************************/
int
SQL_Commit(struct jr_db_handle_s *db_handle) {
    if (db_handle == NULL) {
        return -1;
    }
    if (SQLTransact(db_handle->jobrep_henv, db_handle->jobrep_hdbc, SQL_COMMIT) == SQL_SUCCESS) {
        return 0;
    } else {
        return -2;
    }
}

/******************************************************************************
Function:   SQL_Rollback
Description:
    Rolls back the transaction
Parameters:
Returns:
    SQL_SUCCESS : succes (no results)
   !SQL_SUCCESS : failure
******************************************************************************/
int
SQL_Rollback(struct jr_db_handle_s *db_handle) {
    if (db_handle == NULL) {
        return -1;
    }
    if (SQLTransact(db_handle->jobrep_henv, db_handle->jobrep_hdbc, SQL_ROLLBACK) == SQL_SUCCESS) {
        return 0;
    } else {
        return -2;
    }
}

/******************************************************************************
Function:   SQL_BindParam
Description:
    SQLBindParameter wrapper to bind parameters to an SQLExecute or SQLExecDirect after an SQLPrepare
Parameters:
Returns:
    0  : Success
    -1 : Initialization error
    -2 : SQLBindParameter() failure
******************************************************************************/
int
SQL_BindParam(struct jr_db_handle_s *db_handle,
                    SQLUSMALLINT param_number,
                    SQLSMALLINT c_type, SQLSMALLINT sql_type,
                    SQLPOINTER data) {
    SQLRETURN res;

    if (db_handle == NULL) {
        return -1;
    }

    res = SQLBindParameter(db_handle->jobrep_hstmt,
          param_number,
          SQL_PARAM_INPUT,
          c_type,
          sql_type,
          0,
          0,
          data,
          0,
          NULL);
    if (res != SQL_SUCCESS) {
        ODBC_Errors(db_handle, res, "SQLBindParam");
        return -2;
    }

    return 0;
}

/******************************************************************************
Function:   SQL_Prepare
Description:
    Prepare the SQL query, can be followed by an SQL_BindParam and SQL_Query
Parameters:
Returns:
    0   : succes
    -1  : init error
    -2  : SQLPrepare failure
******************************************************************************/
int
SQL_Prepare (struct jr_db_handle_s *db_handle, SQLCHAR *sql) {
    SQLRETURN res = 0;

    if ((db_handle == NULL) || (sql == NULL)) {
        return -1;
    }

    if (SQL_QueryCloseAndClean(db_handle) != 0) {
        lcmaps_log(LOG_ERR, "%s: the SQL_QueryCloseAndClean() failed.\n", __func__);
        return -1;
    }

    /* Allocate the statement handle */
    res = SQLAllocHandle(SQL_HANDLE_STMT, db_handle->jobrep_hdbc, &(db_handle->jobrep_hstmt));
    if ((res != SQL_SUCCESS) && (res != SQL_SUCCESS_WITH_INFO)) {
        ODBC_Errors(db_handle, res, "Failure in AllocStatement\n");
        return -1;
    }

    /* Prepare */
    res = SQLPrepare (db_handle->jobrep_hstmt, sql, SQL_NTS);
    if (res != SQL_SUCCESS) {
        ODBC_Errors(db_handle, res, "SQLPrepare");
        return -2;
    }

    db_handle->querystate = PREPARED;
    return 0;
}

/******************************************************************************
Function:   SQL_Exec
Description:
    Executes an SQL statement without a resultset, only a boolean will be returned
    Example: Update, insert, create, drop
Parameters:
Returns:
    0  : Success
    -1 : Initialization error
    -2 : SQLExecDirect() error
    -3 : Cleanup error
******************************************************************************/
int
SQL_Exec(struct jr_db_handle_s *db_handle) {
    SQLRETURN res = 0;

    if (db_handle == NULL) {
        return -1;
    }

    /* Check if the query is prepared */
    if (db_handle->querystate != PREPARED) {
        lcmaps_log(LOG_ERR, "%s: SQL_Exec() called without SQL_Prepare()\n", __func__);
        res = -1;
        goto cleanup;
    }
    /* In Query */
    db_handle->querystate = QUERY;

    /* SQL Execute! */
    if ((res = SQLExecute (db_handle->jobrep_hstmt)) != SQL_SUCCESS) {
        if (!db_handle->ignore_errors) {
            ODBC_Errors(db_handle, res, "SQLExecute\n");
        }
        res = -2;
        goto cleanup;
    }

    /* Release handle */
    SQL_QueryClose(db_handle);
cleanup:
    db_handle->querystate = NONE;
    return res;
}


/******************************************************************************
Function:   SQL_Query
Description:
    Must be preceeded by an SQL_Prepare and optionally an SQL_BindParam.
Parameters:
Returns:
    0  : succes
    -1 : failure
    -2 : failure
******************************************************************************/
SQLRETURN
SQL_Query(struct jr_db_handle_s *db_handle) {
    SQLRETURN res = SQL_SUCCESS;
    SQLULEN   i = 0;
    SQLUSMALLINT colCnt,colNum=0;
    SQLULEN   rowCnt,rowNum=0;
    char      colName[255];
    short     colType = 0;
    short     colCType = 0;
    SQLULEN   colPrecision = 0;
    SQLLEN    colIndicator = 0;
    short     colScale = 0;
    short     colNullable = 0;

    if (db_handle == NULL) {
        res = SQL_INVALID_HANDLE;
        goto sql_query_cleanup;
    }

    if (db_handle->querystate != PREPARED) {
        lcmaps_log(LOG_ERR, "%s: SQL_Query() called without SQL_Prepare()\n", __func__);
        res = SQL_ERROR;
        goto sql_query_cleanup;
    }
    /* In Query */
    db_handle->querystate = QUERY;

    /* SQL Execute! */
    if ((res = SQLExecute (db_handle->jobrep_hstmt)) != SQL_SUCCESS) {
        ODBC_Errors(db_handle, res, "SQLExecute");
        goto sql_query_cleanup;
    }

    /* Got a Query, clean the ResultSet, if any is set. */
    if (db_handle->resultset != NULL) {
        lcmaps_log(LOG_ERR, "%s: The memory for the SQL resultset was not cleaned.\n", __func__);
        res = SQL_ERROR;
        goto sql_query_cleanup;
    }

    /* New resultset */
    db_handle->resultset = malloc(sizeof(TResultSet));
    if (db_handle->resultset == NULL) {
        lcmaps_log(LOG_ERR, "%s: Unable to allocate the resultset\n", __func__);
        res = SQL_ERROR;
        goto sql_query_cleanup;
    }

    /* Get column count */
    res = SQLNumResultCols(db_handle->jobrep_hstmt, &(db_handle->resultset->colCnt));
    if (res != SQL_SUCCESS || db_handle->resultset->colCnt<0) {
        ODBC_Errors(db_handle, res, "SQLNumResultCols");
        goto sql_query_cleanup;
    }

    /* Get the row count */
    res = SQLRowCount(db_handle->jobrep_hstmt, &(db_handle->resultset->rowCnt));
    if (res != SQL_SUCCESS || db_handle->resultset->rowCnt<0) {
        ODBC_Errors(db_handle, res, "SQLRowCount");
        goto sql_query_cleanup;
    }

    /* Build the ResultSet Grid *
     * Note: colCnt is successfully retrieved, hence will be non-negative so
     * cast explicitly to unsigned */
    colCnt=(SQLUSMALLINT)db_handle->resultset->colCnt;
    rowCnt=(SQLULEN)db_handle->resultset->rowCnt;

    db_handle->resultset->columns = malloc(sizeof(TColumn) * colCnt);
    if (db_handle->resultset->columns == NULL) {
        lcmaps_log(LOG_ERR, "%s: Error in allocating %u bytes\n",
                            __func__, sizeof(TColumn) * colCnt);
	res = SQL_ERROR;
        goto sql_query_cleanup;
    }
    memset(db_handle->resultset->columns, 0, sizeof(TColumn) * colCnt);


    db_handle->resultset->data = malloc(sizeof(TRow) * rowCnt);
    if (db_handle->resultset->data == NULL) {
        lcmaps_log(LOG_ERR, "%s: Error in allocating %u bytes\n",
                            __func__, sizeof(TRow) * rowCnt);
	res = SQL_ERROR;
        goto sql_query_cleanup;
    }
    memset(db_handle->resultset->data, 0, sizeof(TRow) * rowCnt);

    for (i = 0; i < rowCnt; i++) {
        db_handle->resultset->data[i] = malloc(sizeof(TField) * colCnt);
        if (db_handle->resultset->data[i] == NULL) {
            lcmaps_log(LOG_ERR, "%s: Error in allocating %u bytes\n",
                                __func__, sizeof(TField) * colCnt);
	    res = SQL_ERROR;
            goto sql_query_cleanup;
        }
        memset(db_handle->resultset->data[i], 0, sizeof(TField) * colCnt);
    }

    /* Let's get us a dataset */
    for (rowNum=0; rowNum<rowCnt; rowNum++) {
	res = SQLFetch (db_handle->jobrep_hstmt);
	if (res == SQL_NO_DATA_FOUND) {
	    goto sql_query_cleanup;
	}
	if (res != SQL_SUCCESS) {
	    ODBC_Errors(db_handle, res, "SQLFetch");
	    goto sql_query_cleanup;
	}

	/* Read a row, column by column */
	for (colNum = 0; colNum < colCnt; colNum++) {
	    res = SQLDescribeCol(db_handle->jobrep_hstmt, (SQLUSMALLINT)(colNum + 1),
				(UCHAR *) colName, sizeof (colName),
				NULL, &colType, &colPrecision,
				&colScale, &colNullable);
	    if (res != SQL_SUCCESS) {
		ODBC_Errors(db_handle, res, "SQLDescribeCol");
		goto sql_query_cleanup;
	    }

	    void *data;
	    SQLLEN bufsize_bytes;
	    bufsize_bytes = 0;
	    data = NULL;

	    switch(colType) {
		case SQL_UNKNOWN_TYPE    :
		    /* Error? */
		    break;
		case SQL_DECIMAL         :
		case SQL_NUMERIC         :
		    colCType = SQL_C_CHAR;
		    bufsize_bytes = sizeof(char) * 2;
		    break;
		case SQL_BIGINT          :
		case SQL_INTEGER         :
		    colCType = SQL_C_LONG;
		    bufsize_bytes = sizeof(long);
		    break;
		case SQL_SMALLINT        :
		    colCType = SQL_C_SHORT;
		    bufsize_bytes = sizeof(short);
		    break;
		case SQL_FLOAT           :
		case SQL_REAL            :
		    colCType = SQL_C_FLOAT;
		    bufsize_bytes = sizeof(float);
		    break;
		case SQL_DOUBLE          :
		    colCType = SQL_C_DOUBLE;
		    bufsize_bytes = sizeof(double);
		    break;
#if 0
	    #if (ODBCVER >= 0x0300)
		case SQL_DATETIME        :

		    break;
	    #endif
#endif
		case SQL_CHAR            :
		case SQL_VARCHAR         :
		    colCType = SQL_C_CHAR;
		    bufsize_bytes = sizeof(char) * 50;
		    break;
		default :
		    lcmaps_log(LOG_ERR, "%s: Unknown datatype of value %d\n", __func__, colType);
		    res = SQL_ERROR;
		    goto sql_query_cleanup;
	    }


	    /********************************
	    Start of getting data with SQLGetData() The data will be read and made
	    available in the 'data_final' field. This can be casted to what ever
	    the colCType indicates.
	    ********************************/
	    data = malloc((size_t)bufsize_bytes);

	    void *data_final = NULL;

	    void *data2 = NULL;
	    void *data3 = NULL;
	    size_t total_buf = 0;
	    SQLLEN offset = 0;
	    int   needs_concat = 0;

	    data_final = data;
	    do {
		/* Fetch data for this field */
		memset(data, 0, (size_t)bufsize_bytes);
		res = SQLGetData (db_handle->jobrep_hstmt,
				  (SQLUSMALLINT)(colNum + 1),
				  colCType, data, bufsize_bytes,
				  &colIndicator);
		if (res == SQL_SUCCESS) {
		    if (needs_concat) {
			/* If there is more data available for a given field, continue here */
			total_buf += (size_t)bufsize_bytes;

			/* Expand memory into a new buffer and copy the current buffer in it. */
			if (data2 == NULL) {
			    data3 = malloc(sizeof(char) * total_buf);
			} else {
			    data3 = realloc(data2, sizeof(char) * total_buf);
			}
			if (data3 == NULL) {
			    /* Allocation error */
			    free(data2);
			    res = SQL_ERROR;
			    goto sql_query_cleanup;
			}
			data2 = data3;
			memcpy((char*)data2+offset, data, (size_t)bufsize_bytes);

			/* Move the offset with the buffer size minus 1, because the last
			 * byte is a \0 */
			offset += bufsize_bytes - 1;

			data_final = data2;
		    }
		    break; /* Done */
		} else if (res != SQL_SUCCESS_WITH_INFO) {
		    ODBC_Errors(db_handle, res, "SQLGetData");
		    goto sql_query_cleanup;
		}

		/* Signal that the last read buffer needs to be concattinated as a final step */
		needs_concat = 1;

		/* If there is more data available for a given field, continue here */
		total_buf += (size_t)bufsize_bytes;

		/* Expand memory into a new buffer and copy the current buffer in it. */
		if (data2 == NULL) {
		    data3 = malloc(sizeof(char) * total_buf);
		} else {
		    data3 = realloc(data2, sizeof(char) * total_buf);
		}
		if (data3 == NULL) {
		    /* Allocation error */
		    free(data2);
		    res = SQL_ERROR;
		    goto sql_query_cleanup;
		}
		data2 = data3;
		memcpy((char*)data2+offset, data, (size_t)bufsize_bytes);
		/* Move the offset with the buffer size minus 1, because the last
		 * byte is a \0 */
		offset += bufsize_bytes - 1;

		data_final = data2;
	    } while (res == SQL_SUCCESS_WITH_INFO);

	    /* Hook up the fetched results into the TResultSet */
	    db_handle->resultset->columns[colNum].columnname     = strdup(colName);
	    db_handle->resultset->columns[colNum].type           = colType;
	    db_handle->resultset->data[rowNum][colNum].fieldname = strdup(colName);
	    db_handle->resultset->data[rowNum][colNum].type      = colCType;
	    db_handle->resultset->data[rowNum][colNum].byte_size = total_buf;
	    switch(colCType) {
		case SQL_C_CHAR :
		    db_handle->resultset->data[rowNum][colNum].v_string = (char *)data_final;
		    break;
		case SQL_C_LONG :
		    db_handle->resultset->data[rowNum][colNum].v_long = *((long *)data_final);
		    break;
		case SQL_C_SHORT:
		    db_handle->resultset->data[rowNum][colNum].v_short = *((short *)data_final);
		    break;
		case SQL_C_FLOAT:
		    db_handle->resultset->data[rowNum][colNum].v_float = *((float *)data_final);
		    break;
		case SQL_C_DOUBLE:
		    db_handle->resultset->data[rowNum][colNum].v_double = *((double *)data_final);
		    break;
	    }
	}
    } /* row loop */

sql_query_cleanup:
    SQL_QueryClose(db_handle);

    return res;
}

/******************************************************************************
Function:   SQL_QueryCloseAndClean
Description:
    Closing a query.
Parameters:
Returns:
    0  : success
    -1 : failure
******************************************************************************/
int
SQL_QueryCloseAndClean(struct jr_db_handle_s *db_handle) {
    if (db_handle == NULL) {
        return -1;
    }
    SQL_QueryClose(db_handle);
    SQL_TResultSet_free(db_handle->resultset);
    db_handle->resultset = NULL;

    return 0;
}

/******************************************************************************
Function:   SQL_QueryCloseAndClean
Description:
    Closing a query.
Parameters:
Returns:
    0  : succes
    -1 : failure
******************************************************************************/
int
SQL_QueryClose(struct jr_db_handle_s *db_handle) {
    SQLRETURN sts = 0;

    if (db_handle == NULL) {
        return -1;
    }

    if (db_handle->querystate == QUERY) {
        if (db_handle->jobrep_hstmt) {
            sts = SQLCloseCursor(db_handle->jobrep_hstmt);
            if (sts != SQL_SUCCESS) {
                ODBC_Errors(db_handle, sts, "CloseCursor");
                return -2;
            }
        }
        db_handle->querystate = PREPARED;
    }

    if (db_handle->querystate == PREPARED) {
        SQLFreeHandle (SQL_HANDLE_STMT, db_handle->jobrep_hstmt);
        db_handle->jobrep_hstmt = NULL;
    }
    /* Resetting state */
    db_handle->querystate = NONE;
    SQL_IgnoreErrors_Reset(db_handle);

    return 0;
}

/******************************************************************************
Function:   SQL_TResultSet_free
Description:
    Frees a TResultSet structure.
Parameters:
    TResultSet *
Returns:
    void
******************************************************************************/
void
SQL_TResultSet_free(TResultSet *resultset) {
    int i = 0, j = 0;

    if (resultset == NULL)
        return;

    if ((resultset->colCnt <= 0) && (resultset->rowCnt <= 0)) {
        free(resultset);
        resultset = NULL;
        return;
    }

    /* Free columnnames */
    for (i = 0; i < resultset->colCnt; i++) {
        if (resultset->columns[i].columnname != NULL) {
            free(resultset->columns[i].columnname);
        }
    }

    /* Free data from all TField structures with it's TField structs */
    for (i = 0; i < resultset->rowCnt; i++) {
        for (j = 0; j < resultset->colCnt; j++) {
            free(resultset->data[i][j].fieldname);
            if (resultset->data[i][j].type == SQL_C_CHAR) {
                free(resultset->data[i][j].v_string);
            }
        }
        free(resultset->data[i]);
        resultset->data[i] = NULL;
    }

    if (resultset->data) free(resultset->data);
    resultset->data = NULL;
    if (resultset->columns) free(resultset->columns);
    resultset->columns = NULL;
    free(resultset);
    resultset = NULL;
}


/******************************************************************************
Function:   SQL_printfResultSet
Description:
    prints the resultset to the stdout
Parameters:
    TResultSet * result
Returns:
    0        : succes (no results)
   !0        : failure
******************************************************************************/
int
SQL_printfResultSet (TResultSet *resultset) {
     return SQL_fprintfResultSet (stdout, resultset);
}

/******************************************************************************
Function:   SQL_fprintfResultSet
Description:
    prints the resultset to the filestream, 'stream'
Parameters:
    TResultSet * result
    FILE *       stream
Returns:
    0        : succes (no results)
   !0        : failure
******************************************************************************/
int
SQL_fprintfResultSet (FILE *stream, TResultSet *resultset)
{
    int i = 0, j = 0;

    if (resultset == NULL)
        return -1;

    if ( (resultset->colCnt == 0) || ( resultset->rowCnt == 0) )
        return -2;

    for (j = 0; j < resultset->colCnt; j++) {
        fprintf(stream, "|%25s|", resultset->columns[j].columnname);
    }

    fprintf(stream, "\n");
    for (i = 0; i < (resultset->colCnt * 25 + resultset->colCnt + 3); i++)
        fprintf(stream, "-");
    fprintf(stream, "\n");

    for (i = 0; i < resultset->rowCnt; i++) {
        for (j = 0; j < resultset->colCnt; j++) {
            switch (resultset->data[i][j].type) {
                case SQL_C_CHAR :
                    fprintf(stream, "|%25s|", resultset->data[i][j].v_string);
                    break;
                case SQL_C_LONG :
                    fprintf(stream, "|%25ld|", resultset->data[i][j].v_long);
                    break;
                case SQL_C_SHORT:
                    fprintf(stream, "|%25d|", resultset->data[i][j].v_short);
                    break;
                case SQL_C_FLOAT:
                    fprintf(stream, "|%25f|", resultset->data[i][j].v_float);
                    break;
                case SQL_C_DOUBLE:
                    fprintf(stream, "|%25f|", resultset->data[i][j].v_double);
                    break;
                default:
                    fprintf(stream, "|%25ld|", resultset->data[i][j].v_long);
                    break;
            }
        }
        fprintf(stream, "\n");
    }

    return 0;
}

#if 0

TParamType * globalParameters = NULL;

/*
 *  Connect to the datasource
 *
 *  The connect string can have the following parts and they refer to
 *  the values in the odbc.ini file
 *
 *	DSN=<data source name>		[mandatory]
 *	HOST=<server host name>		[optional - value of Host]
 *	SVT=<database server type>	[optional - value of ServerType]
 *	DATABASE=<database path>	[optional - value of Database]
 *	OPTIONS=<db specific opts>	[optional - value of Options]
 *	UID=<user name>			[optional - value of LastUser]
 *	PWD=<password>			[optional]
 *	READONLY=<N|Y>			[optional - value of ReadOnly]
 *	FBS=<fetch buffer size>		[optional - value of FetchBufferSize]
 *
 *   Examples:
 *
 *	HOST=star;SVT=Informix 5;UID=demo;PWD=demo;DATABASE=stores5
 *
 *	DSN=stores5_informix;PWD=demo
 */



/******************************************************************************
Function:   SQL_SetLock
Description:
    Sets a lock on the given table with the kindOfLock Specified.

Parameters:
    char       * table
    int          kindOfLock (LOCK_READ, LOCK_WRITE)
Returns:
    0             : succes (no results)
   !0             : failure
******************************************************************************/

int SQL_SetLock (char * table, int kindOfLock)
{
    char R[1000];
    char W[1000];

    if (kindOfLock == LOCK_WRITE)
    {
        sprintf(W, "lock tables %s write", table);
        return SQL_Query(W);
    }

    else if (kindOfLock == LOCK_READ)
    {
        sprintf(R, "lock tables %s read", table);
        return SQL_Query(R);
    }

    else if (kindOfLock == (LOCK_READ + LOCK_WRITE))
    {
        sprintf(R, "lock tables %s read", table);
        sprintf(W, "lock tables %s write", table);

        return (SQL_Query(R) + SQL_Query(W));
    }

    return -1;
}

/******************************************************************************
Function:   SQL_UnLock
Description:
    Unlocks the tables that are lock by this connection.

Parameters:
Returns:
    0             : succes (no results)
   !0             : failure
******************************************************************************/

int SQL_UnLock()
{
    return  SQL_Query("unlock tables");
}

#endif
