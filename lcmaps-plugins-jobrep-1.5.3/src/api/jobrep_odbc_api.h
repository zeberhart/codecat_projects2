/*
 * Copyright (c) Members of the IGE Project 2012
 * See http://eu-egee.org/partners/ for details on the copyright holders.
 * For license conditions see the license file or
 * http://eu-egee.org/license.html
 */

/*
 * Copyright (c) 2001 EU DataGrid.
 * For license conditions see http://www.eu-datagrid.org/license.html
 *
 * Copyright (c) 2001, 2002, 2003 by
 *     Oscar Koeroo <okoeroo@nikhef.nl>
 *     NIKHEF Amsterdam, the Netherlands
 */


#ifndef JOBREP_ODBC_API_H
    #define JOBREP_ODBC_API_H

/*****************************************************************************
                            Include header files
******************************************************************************/

#include "lcmaps_jobrep_config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>

#include <lcmaps/lcmaps_modules.h>
#include <lcmaps/lcmaps_arguments.h>
#include <lcmaps/lcmaps_cred_data.h>
#include <lcmaps/lcmaps_vo_data.h>


/******************************************************************************
                                Definitions
******************************************************************************/

#define SQL_ERROR_CODE_STRING(x) \
            x == SQL_SUCCESS ? "SQL_SUCCESS" : \
            x == SQL_SUCCESS_WITH_INFO ? "SQL_SUCCESS_WITH_INFO" : \
            x == SQL_ERROR ? "SQL_ERROR" : \
            x == SQL_INVALID_HANDLE ? "SQL_INVALID_HANDLE" : \
            x == SQL_NEED_DATA ? "SQL_NEED_DATA" : \
            x == SQL_NO_DATA_FOUND ? "SQL_NO_DATA_FOUND" : \
                "unknown"

#define SQL_C_TYPE_STRING(x) \
            x == SQL_C_CHAR ? "SQL_C_CHAR" : \
            x == SQL_C_LONG ? "SQL_C_LONG" : \
            x == SQL_C_SHORT ? "SQL_C_SHORT" : \
            x == SQL_C_FLOAT ? "SQL_C_FLOAT" : \
            x == SQL_C_DOUBLE ? "SQL_C_DOUBLE" : \
                "unknown"

typedef struct TField_s
{
    char * fieldname;
    int    type;
    size_t byte_size;
    union {
        char    v_char;
        char   *v_string;
        int     v_int;
        short   v_short;
        long    v_long;
        float   v_float;
        double  v_double;
        void   *raw;
    };
} TField;

typedef TField * TRow;

typedef struct TColumn_s
{
    char *columnname;
    int   type;
} TColumn;

typedef struct TResultSet_s
{
    TRow       *data;
    TColumn    *columns;
    SQLSMALLINT colCnt;
    SQLLEN     rowCnt;
} TResultSet;

typedef enum QueryState_e {
    NONE,
    PREPARED,
    QUERY
} QueryState;

struct jr_db_handle_s {
    SQLHENV     jobrep_henv;
    SQLHDBC     jobrep_hdbc;
    SQLHSTMT    jobrep_hstmt;
    short       jobrep_connected;
    QueryState  querystate;
    short       ignore_errors;
    short       use_transactions;
    TResultSet *resultset;
};


/******************************************************************************
                          Module specific prototypes
******************************************************************************/

struct jr_db_handle_s *ODBC_Connect (char *dsn, char *username, char *password);
int ODBC_Test (char *, char *, char *);
int ODBC_Disconnect(struct jr_db_handle_s *db_handle);
int SQL_BeginTransaction(struct jr_db_handle_s *db_handle);
int SQL_Commit(struct jr_db_handle_s *db_handle);
int SQL_Rollback(struct jr_db_handle_s *db_handle);
int SQL_BindParam(struct jr_db_handle_s *db_handle,
                    SQLUSMALLINT param_number,
                    SQLSMALLINT c_type, SQLSMALLINT sql_type,
                    SQLPOINTER data);
int SQL_Prepare (struct jr_db_handle_s *db_handle, SQLCHAR *sql);
int SQL_Exec(struct jr_db_handle_s *db_handle);
SQLRETURN SQL_Query (struct jr_db_handle_s *db_handle);
int SQL_QueryClose(struct jr_db_handle_s *db_handle);
int SQL_QueryCloseAndClean(struct jr_db_handle_s *db_handle);
void SQL_TResultSet_free (TResultSet *resultset);

int SQL_printfResultSet (TResultSet *resultset);
int SQL_fprintfResultSet (FILE *stream, TResultSet *resultset);

int SQL_IgnoreErrors_Set(struct jr_db_handle_s *db_handle);
int SQL_IgnoreErrors_Reset(struct jr_db_handle_s *db_handle);

#if 0

#include <openssl/x509.h>
#include <openssl/asn1.h>

/******************************************************************************
                                Definitions
******************************************************************************/

#define MAXCOLS         32
#define EMPTYSET        5

#define LOCK_READ       1
#define LOCK_WRITE      2


typedef struct TField_s
{
    char * fieldname;
    char * value;
    int    type;
} TField;

typedef TField *  TRow;

typedef struct TColumn_s
{
    char * columnname;
    int    type;
} TColumn;

typedef struct TResultSet_s
{
    TRow *    data;
    TColumn * columns;
    int       colCnt;
    int       rowCnt;
} TResultSet;


typedef struct cert_s
{
    X509      * X509cert;
    char      * certStr;
    char      * PEM;

    char      * subject_name;
    char      * issuer_name;
    char      * strNotBefore;
    char      * strNotAfter;

} TSimpleCert;


typedef struct params_s
{
    char      * id;
    char      * value;
    int         type;
} TParamType;


/******************************************************************************
                          Module specific prototypes
******************************************************************************/

void ODBC_MesgHandler (char *);

int  SQL_SetLock (char * table, int kindOfLock);
int  SQL_UnLock ();
int  SQL_Commit ();
int  SQL_Rollback ();

int  SQL_Query (char *);
int  SQL_SetParameter (char *);
int  SQL_GetQueryResult(TResultSet **);
int  SQL_GetValue (TResultSet *, char *, char **);

int  SQL_printfResultSet (TResultSet *);
int  SQL_fprintfResultSet (FILE *, TResultSet *);

void TResultSet_free (TResultSet *);
void TSimpleCert_free (TSimpleCert *, int);
/******************************************************************************
                       Define module specific variables
******************************************************************************/

#endif

#endif /* JOBREP_ODBC_API_H */
