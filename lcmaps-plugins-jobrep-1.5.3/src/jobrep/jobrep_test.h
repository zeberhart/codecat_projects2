/*                                                                                                            
 * Copyright (c) Members of the EGEE Collaboration. 2004.
 * See http://eu-egee.org/partners/ for details on the copyright holders.
 * For license conditions see the license file or
 * http://eu-egee.org/license.html
 */

extern int plugin_introspect(
        int * argc,
        lcmaps_argument_t ** argv
);
extern int plugin_initialize(
        int argc,
        char ** argv
);
extern int plugin_run(
        int argc,
        lcmaps_argument_t * argv
);
extern int plugin_terminate();
