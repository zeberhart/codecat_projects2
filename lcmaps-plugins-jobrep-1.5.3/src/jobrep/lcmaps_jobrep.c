/*
 * Copyright (c) Members of the IGE Project. 2012.
 * See http://ige-project.eu for details on the copyright holders.
 * For license conditions see the license file or
 */

/*
 * Copyright (c) 2001 EU DataGrid.
 * For license conditions see http://www.eu-datagrid.org/license.html
 *
 * Copyright (c) 2001, 2002, 2003, 2012 (and beyond) by
 *     Oscar Koeroo <okoeroo@nikhef.nl>
 *     NIKHEF Amsterdam, the Netherlands
 */

/*!
    \page lcmaps_jobrep.mod Job Repository plugin

    \section posixsynopsis SYNOPSIS
    \b lcmaps_jobrep.mod -vomsdir \<path\> -certdir \<path\> [server=\<MySQL hostname/dns-name\>] [dsn=\<DSN-name\>]
    [port=\<port nr.\>] [driver=\<path/file\>] [database=\<database-name\>] [username=\<database username\>]
    [password=\<database password\>] [-jr_config \<path/file\>]

    \section posixdesc DESCRIPTION

The Job Repository module collects the user, job and user-mapping information when a job has been assigned
to a fabric and handled by LCMAPS. This information is stored into a SQL database. It is essential that
this information is kept secure due to the nature of the data it stores.


    \section posixoptions OPTIONS

    \subsection dns1 dsn=\<DSN-name\>
        This will select the Data Source Name (DSN) that has been set in a odbc.ini file. In the odbc.ini file are one or more Database sources described with the needed settings to connect to a database. (Note: also partial connection settings are supported (3rd party support))

    \subsection server1 server=\<dns-name\>
         specifies the host that has the database to connect to.

    \subsection portnr1 port=\<port nr.\>
         specifies the port number on the host to override the default portnumber

    \subsection driver1 driver=\<path/file\>
         specifies the path and filename of the shared object distributed by the MyODBC package. ODBC needs a driver file to know what kind of database it will be connecting to.

    \subsection database1 database=\<database-name\>
         specifies the name of the database that has to be connected to. One machine can host several databases. The default name to fill in is: "JobRepository".

    \subsection username1 username=\<database username\>
         specifies the database username that the LCMAPS module must use to authorize itself with.

    \subsection password1 password=\<database password\>
         specifies the database password that the LCMAPS module must use to authorize itself with.
                                         (Note: Disadviced to use this in the lcmaps.db file because it is publicly accessable)

    \subsection jrconfig1 -jr_config \<path/file\>
         The path and file specification leads to the root-only-readable file that should contain the database password (or more things like the username) to prevent that a simple user could comprimse the information that is in the database. There is a specific format for this file. All these database parameters must be semi-colon delimited. The same parameters as states above are used. This means that in the lcmaps.db all these parameters could be in this file. Also that is not really adviced due to practical reasons (shifting of servers for example) then you'll just have to edit one file. The lcmaps.db file in this case.
This means that you can mix these parameters as you like between the files. All these parameters are concatinated after each other. When parameters are more then ounce defined the last one in the concatinated string will be effectivly used.
                                         (default: /opt/edg/etc/lcmaps/jobrep_config)

    \subsection vomsdir1 -vomsdir \<path\>
         This is the same directory as the LCMAPS VOMS Extract module looks at. This where the VO certificates are stored.
                                         (default: /etc/grid-security/vomsdir/).
                                         Note: this parameter can not be used in the jobrep_config file.

    \subsection certdir1 -certdir \<path\>
         This is the same directory as the LCMAPS VOMS Extract module looks at. This where the user certificates end up (temporarily)
                                         (default: /etc/grid-security/certificates/).
                                         Note: this parameter can not be used in the jobrep_config file.
*/


/*!
    \file   lcmaps_jobrep.c
    \brief  Plugin to get data for the Job Repository Database
    \author Oscar Koeroo for the EU DataGrid.
*/





/*****************************************************************************
                            Include header files
******************************************************************************/
#include "lcmaps_jobrep_config.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <openssl/x509.h>

#include <lcmaps/lcmaps_modules.h>
#include <lcmaps/lcmaps_arguments.h>
#include <lcmaps/lcmaps_cred_data.h>
#include <lcmaps/lcmaps_vo_data.h>

#include "jobrep_odbc_api.h"
#include "jobrep_data_handling.h"


/******************************************************************************
                                Definitions
******************************************************************************/

#define PLUGIN_RUN      0
#define PLUGIN_VERIFY   1


/******************************************************************************
                          Module specific prototypes
******************************************************************************/
static int plugin_run_or_verify(int, lcmaps_argument_t *, int);


/******************************************************************************
                       Define module specific variables
******************************************************************************/

static char * jr_config_file = NULL;
static char * jr_db_dsn      = NULL;
static char * jr_db_user     = NULL;
static char * jr_db_pass     = NULL;

static int    test_jobrep = 0;

/*****************************************************************************/


/******************************************************************************
Function:   plugin_initialize
Description:
    Initialize plugin
Parameters:
    argc, argv
    argv[0]: the name of the plugin
Returns:
    LCMAPS_MOD_SUCCESS : succes
    LCMAPS_MOD_FAIL    : failure
    LCMAPS_MOD_NOFILE  : db file not found (will halt LCMAPS initialization)
******************************************************************************/
int plugin_initialize(
        int argc,
        char ** argv
)
{
    char *logstr = PACKAGE_TARNAME "-" "plugin_initialize()";
    int i;

    lcmaps_log_debug(5,"%s: passed arguments:\n", logstr);

    /* Run through argv array */
    for (i = 1; i < argc; i++) {
        if (((strcasecmp(argv[i], "-test") == 0) || (strcasecmp(argv[i], "--test") == 0))) {
            test_jobrep = 1;
        } else if ((strcasecmp(argv[i], "--odbcini") == 0) && (i + 1 < argc)) {
            setenv("ODBCINI", argv[i + 1], 1);
            i++;
        } else if ((strcasecmp(argv[i], "--dsn") == 0) && (i + 1 < argc)) {
            jr_db_dsn = argv[i + 1];
            i++;
        } else if ((strcasecmp(argv[i], "--username") == 0) && (i + 1 < argc)) {
            jr_db_user = argv[i + 1];
            i++;
        } else if ((strcasecmp(argv[i], "--password") == 0) && (i + 1 < argc)) {
            jr_db_pass = argv[i + 1];
            i++;
        } else {
            lcmaps_log(LOG_ERR,"%s: Error in initialization parameter: %s (failure)\n", logstr, argv[i]);
            return LCMAPS_MOD_FAIL;
        }
/*
        else if (
                  (strncmp(argv[i], "DSN=",      4) == 0) ||
                  (strncmp(argv[i], "HOST=",     5) == 0) ||
                  (strncmp(argv[i], "USER=",     5) == 0) ||
                  (strncmp(argv[i], "PASS=",     5) == 0) ||
                  (strncmp(argv[i], "DRIVER=",   7) == 0) ||
                  (strncmp(argv[i], "DATABASE=", 9) == 0) ||
                  (strncmp(argv[i], "PORT=",     5) == 0) ||
                  (strncmp(argv[i], "PASSWORD=", 9) == 0) ||
                  (strncmp(argv[i], "SERVER=",   7) == 0) ||
                  (strncmp(argv[i], "driver=",   7) == 0) ||
                  (strncmp(argv[i], "dsn=",      4) == 0) ||
                  (strncmp(argv[i], "host=",     5) == 0) ||
                  (strncmp(argv[i], "user=",     5) == 0) ||
                  (strncmp(argv[i], "port=",     5) == 0) ||
                  (strncmp(argv[i], "database=", 9) == 0) ||
                  (strncmp(argv[i], "server=",   7) == 0) ||
                  (strncmp(argv[i], "port=",     5) == 0) ||
                  (strncmp(argv[i], "password=", 9) == 0) ||
                  (strncmp(argv[i], "pass=",     5) == 0) )
        {
            if (strlen(connStr) == 0)
                strncpy(connStr, argv[i], strlen(argv[i]));
            else
                sprintf(connStr, "%s;%s", connStr, argv[i]);
        }
*/
    }

    /* Post checks */
    if (jr_db_dsn == NULL) {
        lcmaps_log(LOG_ERR, "%s: No DSN or Data Source Name provided in the lcmaps.db configuration file. Use --dsn <dsn>.\n", logstr);
        return LCMAPS_MOD_FAIL;
    }
    if (jr_db_user == NULL) {
        lcmaps_log(LOG_ERR, "%s: No User provided in the lcmaps.db configuration file. Use --username <username>.\n", logstr);
        return LCMAPS_MOD_FAIL;
    }

    lcmaps_log_debug(3,"%s: Initialization succeeded\n", logstr);
    return LCMAPS_MOD_SUCCESS;
}


/******************************************************************************
Function:   plugin_introspect
Description:
    return list of required arguments
Parameters:

Returns:
    LCMAPS_MOD_SUCCESS : succes
    LCMAPS_MOD_FAIL    : failure
******************************************************************************/
int plugin_introspect(
        int * argc,
        lcmaps_argument_t ** argv
)
{
    char *logstr = PACKAGE_TARNAME "-" "plugin_introspect()";
    static lcmaps_argument_t argList[] = {
        { "user_dn"     , "char *"           ,  1, NULL},
        {"px509_chain"  , "STACK_OF(X509) *" ,  0, NULL},
        { "job_request" , "char *"           ,  0, NULL},
        { NULL          , NULL               , -1, NULL}
    };

    lcmaps_log_debug(4,"%s: introspecting\n", logstr);

    *argv = argList;
    lcmaps_log_debug(5,"%s: before lcmaps_cntArgs()\n", logstr);
    *argc = lcmaps_cntArgs(argList);
    lcmaps_log_debug(5,"%s: address first argument: 0x%x\n", logstr,argList);

    lcmaps_log_debug(5,"%s: Introspect succeeded\n", logstr);
    return LCMAPS_MOD_SUCCESS;
}


/******************************************************************************
Function:   plugin_run
Description:
    Gather credentials for LCMAPS
Parameters:
    argc: number of arguments
    argv: list of arguments
Returns:
    LCMAPS_MOD_SUCCESS: authorization succeeded
    LCMAPS_MOD_FAIL   : authorization failed
******************************************************************************/
int plugin_run(
        int argc,
        lcmaps_argument_t * argv
)
{
    return plugin_run_or_verify(argc, argv, PLUGIN_RUN);
}

/******************************************************************************
Function:   plugin_verify
Description:
    Verify if user is entitled to use local credentials based on his grid
    credentials. This means that the site should already have been set up
    by, e.g., LCMAPS in a previous run. This method will not try to setup
    account leases, modify (distributed) passwd/group files, etc. etc.
    The outcome should be identical to that of plugin_run().
    In this particular case "plugin_verify()" is identical to "plugin_run()"

Parameters:
    argc: number of arguments
    argv: list of arguments
Returns:
    LCMAPS_MOD_SUCCESS: authorization succeeded
    LCMAPS_MOD_FAIL   : authorization failed
******************************************************************************/
int plugin_verify(
        int argc,
        lcmaps_argument_t * argv
)
{
    return plugin_run_or_verify(argc, argv, PLUGIN_VERIFY);
}

static int plugin_run_or_verify(
        int argc,
        lcmaps_argument_t * argv,
        int lcmaps_mode
)
{
    char                  *logstr = PACKAGE_TARNAME "-" "plugin_run()";
    struct jr_db_handle_s *db_handle = NULL;
    void                  *lcmaps_dummy_arg_val = NULL;
    STACK_OF(X509)        *px509_chain = NULL;
    char                  *dn = NULL, *gatekeeper_jm_id;
    long                    user_id = -1;
    long                    unix_uid_id = -1;
    long                    eff_cred_id = -1;

    /* The beginning */
    if (lcmaps_mode != PLUGIN_RUN)  {
        lcmaps_log(LOG_ERR, "%s: attempt to run plugin in an unsupported mode: %d\n", logstr, lcmaps_mode);
        goto fail_jobrep;
    }

    /* Test the connection to the ODBC coupling, and */
    if (test_jobrep) {
        /* Connect */
        if ((db_handle = ODBC_Connect(jr_db_dsn, jr_db_user, jr_db_pass)) == NULL) {
            lcmaps_log(LOG_ERR, "%s: Failed to connect to DSN \"%s\" with user \"%s\" and %s\n",
                                logstr, jr_db_dsn, jr_db_user, jr_db_pass ? "a password" : "no password");
            goto fail_jobrep;
        } else {
            lcmaps_log(LOG_INFO, "%s: Succeeded to connect to DSN \"%s\" with user \"%s\" and %s\n",
                                 logstr, jr_db_dsn, jr_db_user, jr_db_pass ? "a password" : "no password");
        }

        /* Disconnect from the database - Done testing */
        ODBC_Disconnect(db_handle);
        return LCMAPS_MOD_SUCCESS;
    }

    /* Fetch the X509 chain from the LCMAPS framework */
    lcmaps_dummy_arg_val = lcmaps_getArgValue("px509_chain", "STACK_OF(X509) *", argc, argv);
    if (lcmaps_dummy_arg_val && (px509_chain = *(STACK_OF(X509) **) lcmaps_dummy_arg_val)) {
        lcmaps_log_debug(5,"%s: found X.509 chain.\n", logstr);
    } else {
        lcmaps_log (LOG_ERR,"%s: could not get the value of an X.509 chain or a PEM string. " \
                            "This is a bug or the framework is used from a services that didn't " \
                            "provide a certificate chain (via GSI credential or PEM string) as input. " \
                            "Unconfigure this plug-in and call for support.\n", logstr);
        goto fail_jobrep;
    }

    /* Connect */
    if ((db_handle = ODBC_Connect(jr_db_dsn, jr_db_user, jr_db_pass)) == NULL) {
        lcmaps_log(LOG_ERR, "%s: Failed to connect to DSN \"%s\" with user \"%s\" and %s\n",
                            logstr, jr_db_dsn ? jr_db_dsn : "<dsn n/a>", jr_db_user ? jr_db_user : "<username n/a>", jr_db_pass ? "a password" : "no password");
        goto fail_jobrep;
    } else {
        lcmaps_log_debug(1, "%s: Succeeded to connected to DSN \"%s\" with user \"%s\" and %s\n",
                            logstr, jr_db_dsn, jr_db_user, jr_db_pass ? "a password" : "no password");
    }

    /* Push Unix UID, Primary/Secondary GID Credentials to the database */
    unix_uid_id = jobrep_push_unix_cred(db_handle);
    if (unix_uid_id < 0) {
        goto fail_jobrep;
    }

    /* Push certificate stack information into the database */
    if (jobrep_push_certificates(db_handle, px509_chain) < 0) {
        goto fail_jobrep;
    }

    /* Get the DN */
    lcmaps_dummy_arg_val = lcmaps_getArgValue("user_dn", "char *", argc, argv);
    if (lcmaps_dummy_arg_val && (dn = *(char **)lcmaps_dummy_arg_val)) {
        lcmaps_log_debug(5,"%s: found dn: %s\n", logstr, dn);
    } else {
        lcmaps_log_debug(1,"%s: could not get value of dn !\n", logstr);
    }

    /* Pinpoint the EEC certificate in the chain and select/assign a user_id to
     * link-up other user specific information - Result is negative or a user_id */
    user_id = jobrep_assign_userid(db_handle, px509_chain, dn);
    if (user_id < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to assign a user_id\n", logstr);
        goto fail_jobrep;
    }

    /* Begin Transaction */
    if (SQL_BeginTransaction(db_handle) < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to enable the use of transactions.\n", __func__);
        goto fail_jobrep;
    }

    /* Create one Effective Credential ID record to hookup all the credentials */
    eff_cred_id = jobrep_create_effective_credentials_main(db_handle);
    if (eff_cred_id < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to create an effective credential record.\n", logstr);
        goto fail_jobrep;
    }

    /* The GATEKEEPER_JM_ID is set in the file:
     * gt5.2.2-all-source-installer/source-trees/gatekeeper/source/globus_gatekeeper.c:1473:
     */
    gatekeeper_jm_id = getenv("GATEKEEPER_JM_ID");
    if (gatekeeper_jm_id) {
        lcmaps_log(LOG_DEBUG, "%s: The $GATEKEEPER_JM_ID is: %s\n", logstr, gatekeeper_jm_id);
        if (jobrep_push_compute_job_info(db_handle, eff_cred_id, gatekeeper_jm_id) < 0) {
            lcmaps_log(LOG_ERR, "%s: Failed to insert compute job information.\n", logstr);
            goto fail_jobrep;
        }
    }

    /* Push the VOMS FQANs from LCMAPS into the DB and hook them up to the Effective Credential ID */
    if (jobrep_push_voms_fqans(db_handle, eff_cred_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to insert VOMS FQANs\n", logstr);
        goto fail_jobrep;
    }

    /* Push the combination of the user_id and the eff_cred_id */
    if (jobrep_push_effective_credential_user(db_handle, user_id, eff_cred_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to insert the user_id and eff_cred_id combination\n", logstr);
        goto fail_jobrep;
    }

    /* Push the combination of the unix_uid_id and the eff_cred_id
     * Requires the jobrep_push_unix_cred to have finished  */
    if (jobrep_push_effective_credential_unix_uid(db_handle, unix_uid_id, eff_cred_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to insert the unix_uid_id and eff_cred_id combination\n", logstr);
        goto fail_jobrep;
    }

    /* Push all the Unix GIDs as mapped without a specific VOMS FQAN link.
     * This could hold extra Unix GIDs that are orignating from other plug-ins
     * then the native LCMAPS VOMS */
    if (jobrep_push_effective_credential_unix_gids(db_handle, eff_cred_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to insert the list of mapped Unix GIDs\n", __func__);
        goto fail_jobrep;
    }

    /* End Transaction - Commit */
    if (db_handle->use_transactions) {
        SQL_Commit(db_handle);
    }

    /* Disconnect from the database */
    ODBC_Disconnect(db_handle);

    lcmaps_log(LOG_INFO, "%s: jobrep plugin succeeded\n", logstr);

    return LCMAPS_MOD_SUCCESS;

fail_jobrep:
    /* Disconnect from the database - Should Rollback */
    if (db_handle && db_handle->use_transactions) {
        /* Rollback if in Transaction */
        SQL_Rollback(db_handle);
    }
    if (db_handle) {
        ODBC_Disconnect(db_handle);
    }
    return LCMAPS_MOD_FAIL;
}


/******************************************************************************
Function:   plugin_terminate
Description:
    Terminate plugin
Parameters:

Returns:
    LCMAPS_MOD_SUCCESS : succes
    LCMAPS_MOD_FAIL    : failure
******************************************************************************/
int plugin_terminate(void)
{
    char *logstr = PACKAGE_TARNAME "-" "plugin_terminate()";
    lcmaps_log_debug(1,"%s: terminating\n", logstr);

    if (jr_config_file) {
        free(jr_config_file);
    }

    return LCMAPS_MOD_SUCCESS;
}


