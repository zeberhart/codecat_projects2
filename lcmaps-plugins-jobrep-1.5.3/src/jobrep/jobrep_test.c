/*                                                                                                            
 * Copyright (c) Members of the EGEE Collaboration. 2004.
 * See http://eu-egee.org/partners/ for details on the copyright holders.
 * For license conditions see the license file or
 * http://eu-egee.org/license.html
 */

/* LCMAPS includes */
#include <lcmaps/lcmaps_types.h>
#include <lcmaps/lcmaps_log.h>
/*#include "../../src/pluginmanager/_lcmaps_log.h"*/
#include <lcmaps/lcmaps_arguments.h>
/*#include "../../src/pluginmanager/_lcmaps_cred_data.h"*/
#include <stdlib.h>
#include <stdio.h>
#include "jobrep_test.h"
#include <gssapi.h>
#include <globus_gss_assist.h>
#include <pwd.h>

#define DO_USRLOG           ((unsigned short)0x0001) /*!< flag to indicate that
                                      user logging has to be done \internal */
#define DO_SYSLOG           ((unsigned short)0x0002) /*!< flag to indicate that
                                      syslogging has to be done \internal */

#define USE_VOMS_TEST_PROXY 1

#if USE_VOMS_TEST_PROXY
static gss_cred_id_t credential_handle = GSS_C_NO_CREDENTIAL;
#endif


int main()
{
    int argc;
    char *              username    = "okoeroo";
    char *              job_request = "JOBREQUEST \" blabla \", dkdkdka";
    struct passwd       *user_info  = NULL;
    int                 i           = 0;
    /* int                 cnt_sec_gid = 0; */
    gid_t               sec_gid     = 0; 
    lcmaps_argument_t *args      = NULL;
    char              *init_argv[] = {"voms.mod", "-vomsdir", "/etc/grid-security", "-certdir", "/etc/grid-security/certificates",
//                                      "DSN=jobrep", "HOST=tbn13.nikhef.nl", "USER=root", "PASS=nikhef"};
                                      "HOST=tbn13.nikhef.nl", "USER=lcmaps_jobrep", "PASS=jobrep_lcmaps", "DRIVER=/usr/local/lib/libmyodbc3.so"};


    char              *user_dn   = "/O=dutchgrid/O=users/O=nikhef/CN=Oscar Koeroo";
        setenv("X509_USER_PROXY","/tmp/x509up_u505",1); // okoeroo
//    char              *user_dn   = "/O=dutchgrid/O=users/O=nikhef/CN=Martijn Steenbakkers";
//        setenv("X509_USER_PROXY","/home/okoeroo/x509up_u500",1); // martijn
//    char              *user_dn   = "/O=dutchgrid/O=users/O=nikhef/CN=Gerben Michiel Venekamp";
//        setenv("X509_USER_PROXY","/home/okoeroo/x509up_u506",1); // venekamp


    if (lcmaps_log_open(NULL,stdout,DO_USRLOG)) return 1;

    lcmaps_log_debug(0,"\n");
    lcmaps_log(LOG_NOTICE,"Initialization test\n");

#if USE_VOMS_TEST_PROXY
    {
        OM_uint32 major_status;
        OM_uint32 minor_status;
    
        major_status = globus_gss_assist_acquire_cred(&minor_status,
                                                      GSS_C_INITIATE, /* or GSS_C_ACCEPT */
                                                      &credential_handle);

        if (major_status != GSS_S_COMPLETE)
        {
            globus_gss_assist_display_status(stderr,
                                             "Some failure message here",
                                             major_status,
                                             minor_status,
                                             0);
            return 1;
        }
    }
#endif

    user_info = getpwnam(username);

    addCredentialData(UID, &(user_info->pw_uid));
    addCredentialData(PRI_GID, &(user_info->pw_gid));
    /*
     * Retrieve secondary group id's
     */
    sec_gid = (user_info->pw_gid);
    for (i = 0; i < 10; i++)
    {
        addCredentialData(SEC_GID, &sec_gid);
        sec_gid++;
    }

    plugin_initialize(9, init_argv);
    plugin_introspect(&argc, &args);
    

    if (lcmaps_setArgValue("job_request", "char *", (void *) &job_request, argc, &args) != 0)
    {
        lcmaps_log(0,
               "lcmaps.mod-runPluginManager(): could not SET requested variable \"%s\" of type \"%s\" for plugin \"%s\"\n",
               "job_request", "char *", "jobrepository");
        return 1;
    }


    if (lcmaps_setArgValue("user_dn", "char *", (void *) &user_dn, argc, &args) != 0)
    {
        lcmaps_log(0,
               "lcmaps.mod-runPluginManager(): could not SET requested variable \"%s\" of type \"%s\" for plugin \"%s\"\n",
               "user_dn", "char *", "jobrep");
        return 1;
    }
    if (lcmaps_setArgValue("user_cred", "gss_cred_id_t", (void *) &credential_handle, argc, &args) != 0)
    {
        lcmaps_log(0,
               "lcmaps.mod-runPluginManager(): could not SET requested variable \"%s\" of type \"%s\" for plugin \"%s\"\n",
               "user_cred", "gss_cred_id_t", "jobrep");
        return 1;
    }
    plugin_run(argc,args);

    // printCredData();
    if (cleanCredentialData()!=0)
    {
        lcmaps_log(0,"lcmaps.mod-stopPluginManager() error: could not clean credential data list\n");
        return 1;
    }
    plugin_terminate();
    return 0;
}
