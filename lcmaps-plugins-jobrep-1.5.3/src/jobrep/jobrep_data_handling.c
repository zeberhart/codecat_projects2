/*
 * Copyright (c) Members of the IGE Project. 2012.
 * See http://ige-project.eu for details on the copyright holders.
 * For license conditions see the license file or
 */


/*****************************************************************************
                            Include header files
******************************************************************************/
#include "jobrep_data_handling.h"


/*****************************************************************************/

/* Check if certificate can be used as a CA to sign standard X509 certs */
/* Return 1 if true; 0 if not. */
static int
jobrep_grid_x509IsCA(X509 *cert) {
    int idret;

    /* final argument to X509_check_purpose() is whether to check for CAness */
    idret = X509_check_purpose(cert, X509_PURPOSE_SSL_CLIENT, 1);
    if (idret == 1)
        return 1;

    return 0;
}

long
jobrep_assign_userid(struct jr_db_handle_s *db_handle, STACK_OF(X509) *px509_chain, char *eec_subject_dn) {
    long user_id = -1;
    long cert_id = -1;
    int i = 0, depth = 0;
    int found = 0;
    char *subject_DN = NULL, *issuer_DN = NULL;
    X509 *cert = NULL;

    if ((db_handle == NULL) || (px509_chain == NULL) || (eec_subject_dn == NULL)) {
        return -1;
    }

    depth = sk_X509_num(px509_chain);
    for (i = 0; i < depth; i++) {
        if ((cert = sk_X509_value(px509_chain, i))) {
            subject_DN = X509_NAME_oneline(X509_get_subject_name(cert),NULL,0);
            if (subject_DN == NULL) {
                lcmaps_log(LOG_INFO, "%s: No Subject DN found in the certificate.\n", __func__);
                goto fail_jobrep;
            }

            /* Match the EEC or try to find it */
            if (strcmp(subject_DN, eec_subject_dn) != 0) {
                free(subject_DN);
                subject_DN = NULL;
                continue;
            }
            /* Found */
            found = 1;

            /* Find the matching issuer_DN */
            issuer_DN  = X509_NAME_oneline(X509_get_issuer_name(cert),NULL,0);
            if (issuer_DN == NULL) {
                lcmaps_log(LOG_INFO, "%s: No Issuer DN found in the certificate.\n", __func__);
                goto fail_jobrep;
            }

            break;
        }
    }

    if (!found) {
        lcmaps_log(LOG_ERR,
                   "%s: Discovered a bug! The EEC Subject DN from LCMAPS is not found in the provided certificate chain.\n",
                   __func__);
        goto fail_jobrep;
    }

    /*** Query for the certificate id ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"select cert_id from certificates where subject = ? and issuer = ?") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to select the cert_id from a subject and issuer DN\n", __func__);
        goto fail_jobrep;
    }

    if (SQL_BindParam(db_handle, 1, SQL_C_CHAR, SQL_VARCHAR, subject_DN) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Subject DN value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, issuer_DN) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Issuer DN value to the query\n", __func__);
        goto fail_jobrep;
    }

    /* The UNIQUE statement in the DB will purposely cast an error. This query is very unlikely to fail */
    SQL_Query(db_handle);

    /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
    if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
        lcmaps_log(LOG_ERR, "%s: No results from the query to select the cert_id from a subject and issuer DN from certificates.\n", __func__);
        goto fail_jobrep;
    } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
        lcmaps_log(LOG_ERR, "%s: Too many results returned. Either the query got executed wrongly or the database integrity is compromised. Check if the certificates tables has the proper UNIQUE() index statements set.\n", __func__);
        goto fail_jobrep;
    }

    /* One result please, only one */
    if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
        lcmaps_log(LOG_ERR, "%s: result for the \"cert_id\" provided in the wrong datatype. Expected SQL_C_LONG (and equivalent)\n",
                            __func__);
        goto fail_jobrep;
    }

    /* Store the cert_id */
    cert_id = db_handle->resultset->data[0][0].v_long;

    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);

    /*** Insert the cert_id ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"insert into users (cert_id) values (?)") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to insert cert_id into \"users\"\n", __func__);
        goto fail_jobrep;
    }

    /* Bind data to Query */
    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &cert_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"cert_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }

    /* This insert would fail if it already exists. Which wouldn't hurt if it failed */
    SQL_IgnoreErrors_Set(db_handle);
    SQL_Exec(db_handle);

    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);


    /*** Query for the user id ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"select user_id from users where cert_id = ?") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to select the user_id from a cert_id\n", __func__);
        goto fail_jobrep;
    }

    /* Bind data to Query */
    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &cert_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"cert_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }

    /* The UNIQUE statement in the DB will purposely cast an error. This query is very unlikely to fail */
    SQL_Query(db_handle);

    /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
    if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
        lcmaps_log(LOG_ERR, "%s: No results from the query to select the user_id from a cert_id from \"users\".\n", __func__);
        goto fail_jobrep;
    } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
        lcmaps_log(LOG_ERR, "%s: Too many results returned. Either the query got executed wrongly or the database integrity is compromised. Check if the certificates tables has the proper UNIQUE() index statements set.\n", __func__);
        goto fail_jobrep;
    }

    /* One result please, only one */
    if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
        lcmaps_log(LOG_ERR, "%s: result for the \"user_id\" provided in the wrong datatype. Expected SQL_C_LONG (and equivalent)\n",
                            __func__);
        goto fail_jobrep;
    }

    /* Store the user_id */
    user_id = db_handle->resultset->data[0][0].v_long;

    /* Done, need to clean up */
fail_jobrep:
    SQL_QueryCloseAndClean(db_handle);
    if (subject_DN) free(subject_DN);
    if (issuer_DN)  free(issuer_DN);
    return user_id;
}

int
jobrep_push_unix_gid_voms_fqans(struct jr_db_handle_s *db_handle) {
    int                  i = 0;
    lcmaps_vo_mapping_t *vo_mapping = NULL;
    int                  cnt_vo_mapping = 0;

    if (db_handle == NULL) {
        return -1;
    }

    /* VO information */
    vo_mapping = getCredentialData(LCMAPS_VO_CRED_MAPPING, &cnt_vo_mapping);
    for (i = 0; i < cnt_vo_mapping; i++) {
        /* Select existing record of the FQAN */
        /* lcmaps_log_debug (4, "FQAN[%d]: %s, %d(%s)\n", i, vo_mapping[i].vostring, vo_mapping[i].gid, vo_mapping[i].groupname); */

        if (SQL_Prepare(db_handle, (unsigned char *)"insert into voms_fqans (fqan) values (?)") < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to Prepare the query to insert into the voms_fqans\n", __func__);
            goto fail_jobrep;
        }
        if (SQL_BindParam(db_handle, 1, SQL_C_CHAR, SQL_VARCHAR, vo_mapping[i].vostring) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the Subject DN value to the query\n", __func__);
            goto fail_jobrep;
        }

        /* The UNIQUE statement in the DB will purposely cast an error. This query is very unlikely to fail */
        SQL_IgnoreErrors_Set(db_handle);
        SQL_Exec(db_handle);
    }
    return 0;
fail_jobrep:
    return -1;
}

long
jobrep_get_voms_fqan_id_from_fqan(struct jr_db_handle_s *db_handle, char *voms_fqan) {
    long voms_fqan_id = -1;
    SQLRETURN ret;

    if ((db_handle == NULL) || (voms_fqan == NULL)) {
        return -1;
    }

    /*** Query for the certificate id ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"select voms_fqan_id from voms_fqans where fqan = ?") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to select the voms_fqan_id from the voms_fqans\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 1, SQL_C_CHAR, SQL_VARCHAR, voms_fqan) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the VOMS FQAN value to the query\n", __func__);
        goto fail_jobrep;
    }
    /* The VOMS FQAN must exist in the database! */
    ret=SQL_Query(db_handle);
    if (!SQL_SUCCEEDED(ret)) {
        lcmaps_log(LOG_ERR, "%s: Failed to execute the query to fetch the voms_fqan_id from the voms_fqans.\n", __func__);
        goto fail_jobrep;
    }

    /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
    if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
        lcmaps_log(LOG_ERR, "%s: No results from the query to select the voms_fqan_id from the voms_fqans.\n", __func__);
        goto fail_jobrep;
    } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
        lcmaps_log(LOG_ERR, "%s: Too many results returned. Either the query got executed wrongly or the database integrity is compromised. Check if the certificates tables has the proper UNIQUE() index statements set.\n", __func__);
        goto fail_jobrep;
    }

    /* One result please, only one */
    if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
        lcmaps_log(LOG_ERR, "%s: result for the \"voms_fqan_id\" provided in the wrong datatype. Expected SQL_C_LONG (and equivalent)\n",
                            __func__);
        goto fail_jobrep;
    }

    /* Store the voms_fqan_id */
    voms_fqan_id = db_handle->resultset->data[0][0].v_long;

fail_jobrep:
    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);
    return voms_fqan_id;
}

/* Next function is not used, commenting out */
#if 0
int
jobrep_get_unix_uid_id_from_uid(struct jr_db_handle_s *db_handle, uid_t uid, char *uid_name) {
    long unix_uid_id = -1;
    SQLRETURN ret;

    if (db_handle == NULL) {
        return -1;
    }

    if (SQL_Prepare(db_handle,
            (unsigned char *)"select unix_uid_id from unix_uids where uid = ? and uid_name = ?") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to select the unix_uid_id from the unix_uids\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &uid) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix UID value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (uid_name == NULL) {
        if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, "NULL") < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix UID value to the query\n", __func__);
            goto fail_jobrep;
        }
    } else {
        if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, uid_name) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix UID value to the query\n", __func__);
            goto fail_jobrep;
        }
    }
    /* The (Unix) UID must exist in the database! */
    ret=SQL_Query(db_handle);
    if (!SQL_SUCCEEDED(ret)) {
        lcmaps_log(LOG_ERR, "%s: Failed to execute the query to fetch the unix_uid_id from the unix_uids.\n", __func__);
        goto fail_jobrep;
    }

    /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
    if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
        lcmaps_log(LOG_ERR, "%s: No results from the query to select the unix_uid_id from the unix_uids.\n", __func__);
        goto fail_jobrep;
    } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
        lcmaps_log(LOG_ERR, "%s: Too many results returned. Either the query got executed wrongly or the database integrity is compromised. Check if the certificates tables has the proper UNIQUE() index statements set.\n", __func__);
        goto fail_jobrep;
    }

    /* One result please, only one */
    if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
        lcmaps_log(LOG_ERR, "%s: result for the \"unix_uid_id\" provided in the wrong datatype. Expected SQL_C_LONG (and equivalent)\n",
                            __func__);
        goto fail_jobrep;
    }

    /* Store the voms_fqan_id */
    unix_uid_id = db_handle->resultset->data[0][0].v_long;

fail_jobrep:
    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);
    return unix_uid_id;
}
#endif

long
jobrep_get_unix_gid_id_from_gid(struct jr_db_handle_s *db_handle, gid_t gid, char *gid_name) {
    long unix_gid_id = -1;
    SQLRETURN ret;

    if (db_handle == NULL) {
        return -1;
    }

    if (SQL_Prepare(db_handle,
            (unsigned char *)"select unix_gid_id from unix_gids where gid = ? and gid_name = ?") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to select the unix_gid_id from the unix_gids\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &gid) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix GID value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (gid_name == NULL) {
        if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, "NULL") < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix GID value to the query\n", __func__);
            goto fail_jobrep;
        }
    } else {
        if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, gid_name) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix GID value to the query\n", __func__);
            goto fail_jobrep;
        }
    }
    /* The (Unix) GID must exist in the database! */
    ret=SQL_Query(db_handle);
    if (!SQL_SUCCEEDED(ret)) {
        lcmaps_log(LOG_ERR, "%s: Failed to execute the query to fetch the unix_gid_id from the unix_gids.\n", __func__);
        goto fail_jobrep;
    }

    /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
    if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
        lcmaps_log(LOG_ERR, "%s: No results from the query to select the unix_gid_id from the unix_gids.\n", __func__);
        goto fail_jobrep;
    } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
        lcmaps_log(LOG_ERR, "%s: Too many results returned. Either the query got executed wrongly or the database integrity is compromised. Check if the certificates tables has the proper UNIQUE() index statements set.\n", __func__);
        goto fail_jobrep;
    }

    /* One result please, only one */
    if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
        lcmaps_log(LOG_ERR, "%s: result for the \"unix_gid_id\" provided in the wrong datatype. Expected SQL_C_LONG (and equivalent)\n",
                            __func__);
        goto fail_jobrep;
    }

    /* Store the voms_fqan_id */
    unix_gid_id = db_handle->resultset->data[0][0].v_long;

fail_jobrep:
    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);
    return unix_gid_id;
}

long
jobrep_insert_unix_uid_voms_fqans(struct jr_db_handle_s *db_handle, long voms_fqan_id, long unix_uid_id) {
    char                *reg_datetime = NULL;
    long                 unix_uid_voms_fqan_id = -1;

    if ((db_handle == NULL) || (voms_fqan_id < 0) || (unix_uid_id < 0)) {
        return -1;
    }

    /* Insert the Unix GID to FQAN binding */
    if (SQL_Prepare(db_handle,
        (unsigned char *)"insert into unix_uid_voms_fqans (registration_datetime, unix_uid_id, voms_fqan_id)" \
                         "                         values (?,                     ?,           ?)") < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to Prepare the query to insert into the unix_uid_voms_fqans\n", __func__);
        goto fail_jobrep;
    }
    reg_datetime = jobrep_time_to_string(time(NULL));
    if (SQL_BindParam(db_handle, 1, SQL_C_CHAR, SQL_VARCHAR, reg_datetime) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Registration DateTime value to the query\n", __func__);
        goto fail_jobrep;
    }

    if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &unix_uid_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix GID ID value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 3, SQL_C_LONG, SQL_INTEGER, &voms_fqan_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the VOMS FQAN ID value to the query\n", __func__);
        goto fail_jobrep;
    }

    SQL_IgnoreErrors_Set(db_handle);
    SQL_Exec(db_handle);

    /* Get the primary key for the values just entered */
    if (SQL_Prepare(db_handle,
        (unsigned char *)"select unix_uid_voms_fqan_id from unix_uid_voms_fqans" \
                         "                            where unix_uid_id = ? and voms_fqan_id = ?") < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to Prepare the query to select the unix_uid_voms_fqan_id\n", __func__);
        goto fail_jobrep;
    }

    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &unix_uid_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix GID ID value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &voms_fqan_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the VOMS FQAN ID value to the query\n", __func__);
        goto fail_jobrep;
    }
    SQL_Query(db_handle);

    /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
    if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
        lcmaps_log(LOG_ERR, "%s: No results from the query to select the unix_uid_voms_fqan_id from unix_uid_voms_fqans.\n", __func__);
        goto fail_jobrep;
    } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
        lcmaps_log(LOG_ERR, "%s: Too many results returned. Either the query got executed wrongly or the database integrity is compromised. Check if the certificates tables has the proper UNIQUE() index statements set.\n", __func__);
        goto fail_jobrep;
    }

    /* One result please, only one */
    if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
        lcmaps_log(LOG_ERR, "%s: result for the \"unix_uid_voms_fqan_id\" provided in the wrong datatype. Expected SQL_C_LONG (and equivalent)\n",
                            __func__);
        goto fail_jobrep;
    }

    /* Store the cert_id */
    unix_uid_voms_fqan_id = db_handle->resultset->data[0][0].v_long;

    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);

    if (reg_datetime) free(reg_datetime);
    return unix_uid_voms_fqan_id;

fail_jobrep:
    if (reg_datetime) free(reg_datetime);
    return -1;
}

long
jobrep_insert_unix_gid_voms_fqans(struct jr_db_handle_s *db_handle, long voms_fqan_id, long unix_gid_id, int is_primary) {
    char                *reg_datetime = NULL;
    long                  unix_gid_voms_fqan_id = -1;

    if ((db_handle == NULL) || (voms_fqan_id < 0) || (unix_gid_id < 0)) {
        return -1;
    }

    /* Insert the Unix GID to FQAN binding */
    if (SQL_Prepare(db_handle,
        (unsigned char *)"insert into unix_gid_voms_fqans (registration_datetime, unix_gid_id, voms_fqan_id, is_primary)" \
                         "                         values (?,                     ?,           ?,            ?)") < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to Prepare the query to insert into the unix_gid_voms_fqans\n", __func__);
        goto fail_jobrep;
    }
    reg_datetime = jobrep_time_to_string(time(NULL));
    if (SQL_BindParam(db_handle, 1, SQL_C_CHAR, SQL_VARCHAR, reg_datetime) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Registration DateTime value to the query\n", __func__);
        goto fail_jobrep;
    }

    if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &unix_gid_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix GID ID value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 3, SQL_C_LONG, SQL_INTEGER, &voms_fqan_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the VOMS FQAN ID value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 4, SQL_C_LONG, SQL_INTEGER, &is_primary) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Registration DateTime value to the query\n", __func__);
        goto fail_jobrep;
    }

    SQL_IgnoreErrors_Set(db_handle);
    SQL_Exec(db_handle);

    /* Get the primary key for the values just entered */
    if (SQL_Prepare(db_handle,
        (unsigned char *)"select unix_gid_voms_fqan_id from unix_gid_voms_fqans" \
                         "                            where unix_gid_id = ? and voms_fqan_id = ? and is_primary = ?") < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to Prepare the query to select the unix_gid_voms_fqan_id\n", __func__);
        goto fail_jobrep;
    }

    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &unix_gid_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Unix GID ID value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &voms_fqan_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the VOMS FQAN ID value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 3, SQL_C_LONG, SQL_INTEGER, &is_primary) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Registration DateTime value to the query\n", __func__);
        goto fail_jobrep;
    }
    SQL_Query(db_handle);

    /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
    if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
        lcmaps_log(LOG_ERR, "%s: No results from the query to select the unix_gid_voms_fqan_id from unix_gid_voms_fqans.\n", __func__);
        goto fail_jobrep;
    } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
        lcmaps_log(LOG_ERR, "%s: Too many results returned. Either the query got executed wrongly or the database integrity is compromised. Check if the certificates tables has the proper UNIQUE() index statements set.\n", __func__);
        goto fail_jobrep;
    }

    /* One result please, only one */
    if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
        lcmaps_log(LOG_ERR, "%s: result for the \"unix_gid_voms_fqan_id\" provided in the wrong datatype. Expected SQL_C_LONG (and equivalent)\n",
                            __func__);
        goto fail_jobrep;
    }

    /* Store the cert_id */
    unix_gid_voms_fqan_id = db_handle->resultset->data[0][0].v_long;

    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);

    if (reg_datetime) free(reg_datetime);
    return unix_gid_voms_fqan_id;

fail_jobrep:
    if (reg_datetime) free(reg_datetime);
    return -1;
}

long
jobrep_create_effective_credentials_main(struct jr_db_handle_s *db_handle) {
    char *reg_datetime = NULL;
    long eff_cred_id = -1;

    /*** Insert the effective_credentials ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"insert into effective_credentials (registration_datetime) values (?)") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to insert a new effective_credentials record into \"effective_credentials\"\n", __func__);
        goto fail_jobrep;
    }

    /* Bind data to Query */
    reg_datetime = jobrep_time_to_string(time(NULL));
    if (SQL_BindParam(db_handle, 1, SQL_C_CHAR, SQL_VARCHAR, reg_datetime) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the Registration DateTime value to the query\n", __func__);
        goto fail_jobrep;
    }

    /* Fail on existance */
    if (SQL_Exec(db_handle) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to insert a effective_credentials record. This record is crucial.\n", __func__);
        goto fail_jobrep;
    }

    /*** Select last inserted auto-incremented ID ***/
    /* WARNING! MySQL-only */
    if (SQL_Prepare(db_handle,
            (unsigned char *)"select last_insert_id() as eff_cred_id") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query for the last inserted auto_incremente ID for the \"effective_credentials\"\n", __func__);
        goto fail_jobrep;
    }

    /* The UNIQUE statement in the DB will purposely cast an error. This query is very unlikely to fail */
    SQL_Query(db_handle);

    /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
    if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
        lcmaps_log(LOG_ERR, "%s: No results from the query to select the eff_cred_id from \"effective_credentials\"\n", __func__);
        goto fail_jobrep;
    } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
        lcmaps_log(LOG_ERR, "%s: Too many results returned. The last_insert_id() is only expected to return 1 column and 1 row.\n", __func__);
        goto fail_jobrep;
    }

    /* One result please, only one */
    if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
        lcmaps_log(LOG_ERR, "%s: result for the \"eff_cred_id\" provided in the wrong datatype. Expected SQL_C_LONG (and equivalent)\n",
                            __func__);
        goto fail_jobrep;
    }

    /* Store the cert_id */
    eff_cred_id = db_handle->resultset->data[0][0].v_long;

fail_jobrep:
    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);
    return eff_cred_id;
}

int
jobrep_push_compute_job_info(struct jr_db_handle_s *db_handle, long eff_cred_id, char *gatekeeper_jm_id) {
    if (db_handle == NULL || eff_cred_id == -1 || gatekeeper_jm_id == NULL) {
        return -1;
    }

    /*** Insert the effective_credentials ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"insert into compute_jobs (eff_cred_id, gatekeeper_jm_id) values (?, ?)") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to insert a compute_jobs record into \"compute_jobs\"\n", __func__);
        goto fail_jobrep;
    }

    /* Bind data to Query */
    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &eff_cred_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"eff_cred_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, gatekeeper_jm_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"GATEKEEPER_JM_ID\" value to the query\n", __func__);
        goto fail_jobrep;
    }

    /* Fail on existance */
    SQL_Exec(db_handle);

    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);

    return 0;
fail_jobrep:
    return -1;
}

int
jobrep_push_voms_fqans(struct jr_db_handle_s *db_handle, long eff_cred_id) {
    int                  i = 0;
    lcmaps_vo_mapping_t *vo_mapping = NULL;
    int                  cnt_vo_mapping = 0;
    long                  voms_fqan_id = -1;
    long                  unix_gid_id = -1;
    int                  is_primary = 0;
    long                  unix_gid_voms_fqan_id = -1;

    if (db_handle == NULL) {
        return -1;
    }

    /* VO information */
    vo_mapping = getCredentialData(LCMAPS_VO_CRED_MAPPING, &cnt_vo_mapping);
    for (i = 0; i < cnt_vo_mapping; i++) {
        /* Select existing record of the FQAN */
        /* lcmaps_log_debug (4, "FQAN[%d/%d]: %s, %d(%s)\n", */
                            /* i + 1, cnt_vo_mapping, vo_mapping[i].vostring, vo_mapping[i].gid, vo_mapping[i].groupname); */

        if (SQL_Prepare(db_handle, (unsigned char *)"insert into voms_fqans (fqan) values (?)") < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to Prepare the query to insert into the voms_fqans\n", __func__);
            goto fail_jobrep;
        }
        if (SQL_BindParam(db_handle, 1, SQL_C_CHAR, SQL_VARCHAR, vo_mapping[i].vostring) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the Subject DN value to the query\n", __func__);
            goto fail_jobrep;
        }

        /* The UNIQUE statement in the DB will purposely cast an error. This query is very unlikely to fail */
        SQL_IgnoreErrors_Set(db_handle);
        SQL_Exec(db_handle);

        /* Fetch the voms_fqan_id */
        voms_fqan_id = jobrep_get_voms_fqan_id_from_fqan(db_handle, vo_mapping[i].vostring);
        if (voms_fqan_id < 0) {
            lcmaps_log(LOG_ERR,
                "%s: Database integrity error. Failed to find the VOMS FQAN \"%s\" in the database for further processing.\n",
                __func__, vo_mapping[i].vostring);
            goto fail_jobrep;
        }

        /* Fetch unix_gid_id */
        unix_gid_id = jobrep_get_unix_gid_id_from_gid(db_handle, vo_mapping[i].gid, vo_mapping[i].groupname);
        if (unix_gid_id < 0) {
            lcmaps_log(LOG_ERR,
                "%s: Database integrity error. Failed to find the Unix GID \"%d(%s)\" in the database for further processing.\n",
                __func__, vo_mapping[i].gid, vo_mapping[i].groupname);
            goto fail_jobrep;
        }


        /* Insert the VOMS FQAN to Unix GID coupling */
        (i == 0) ? (is_primary = 1) : (is_primary = 0);
        unix_gid_voms_fqan_id = jobrep_insert_unix_gid_voms_fqans(db_handle, voms_fqan_id, unix_gid_id, is_primary);
        if (unix_gid_voms_fqan_id < 0) {
            goto fail_jobrep;
        }

        /*** Insert the effective_credentials_voms ***/
        if (SQL_Prepare(db_handle,
                (unsigned char *)"insert into effective_credentials_unix_gid_voms "\
                                 " (eff_cred_id, unix_gid_voms_fqan_id)" \
                                 " values (?, ?)") < 0) {
            lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to insert eff_cred_id and unix_gid_voms_fqan_id into \"effective_credentials_voms\"\n", __func__);
            goto fail_jobrep;
        }

        /* Bind data to Query */
        if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &eff_cred_id) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the \"eff_cred_id\" value to the query\n", __func__);
            goto fail_jobrep;
        }
        if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &unix_gid_voms_fqan_id) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the \"unix_gid_voms_fqan_id\" value to the query\n", __func__);
            goto fail_jobrep;
        }

        /* Fail on existance */
        SQL_Exec(db_handle);

        /* Clean up */
        SQL_QueryCloseAndClean(db_handle);
    }
    return 0;
fail_jobrep:
    return -1;
}

int
jobrep_push_effective_credential_user(struct jr_db_handle_s *db_handle, long user_id, long eff_cred_id) {
    if ((db_handle == NULL) || (user_id < 0) || (eff_cred_id < 0)) {
        return -1;
    }

    /*** Insert the effective_credentials_users ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"insert into effective_credential_users "\
                             " (eff_cred_id, user_id)" \
                             " values (?, ?)") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to insert eff_cred_id and user_id into \"effective_credential_users\"\n", __func__);
        goto fail_jobrep;
    }

    /* Bind data to Query */
    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &eff_cred_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"eff_cred_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &user_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"user_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }

    /* Fail on existance */
    SQL_Exec(db_handle);

    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);

    return 0;
fail_jobrep:
    return -1;
}

int
jobrep_push_effective_credential_unix_uid(struct jr_db_handle_s *db_handle, long unix_uid_id, long eff_cred_id) {
    int                  i = 0;
    lcmaps_vo_mapping_t *vo_mapping = NULL;
    int                  cnt_vo_mapping = 0;
    long                  voms_fqan_id = -1;
    /* int                  unix_uid_id = -1; */
    long                  unix_uid_voms_fqan_id = -1;

    if ((db_handle == NULL) || (unix_uid_id < 0) || (eff_cred_id < 0)) {
        return -1;
    }

    /* VO information */
    vo_mapping = getCredentialData(LCMAPS_VO_CRED_MAPPING, &cnt_vo_mapping);
    if (cnt_vo_mapping > 0) {
        /* Fetch the voms_fqan_id */
        voms_fqan_id = jobrep_get_voms_fqan_id_from_fqan(db_handle, vo_mapping[0].vostring);
        if (voms_fqan_id < 0) {
            lcmaps_log(LOG_ERR,
                "%s: Database integrity error. Failed to find the VOMS FQAN \"%s\" in the database for further processing.\n",
                __func__, vo_mapping[i].vostring);
            goto fail_jobrep;
        }

        /* unix_uid_voms_fqans */
        unix_uid_voms_fqan_id = jobrep_insert_unix_uid_voms_fqans(db_handle, voms_fqan_id, unix_uid_id);

        /*** Insert the effective_credentials_voms ***/
        if (SQL_Prepare(db_handle,
                (unsigned char *)"insert into effective_credentials_unix_uid_voms "\
                                 " (eff_cred_id, unix_uid_voms_fqan_id)" \
                                 " values (?, ?)") < 0) {
            lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to insert eff_cred_id and unix_uid_voms_fqan_id into \"effective_credentials_voms\"\n", __func__);
            goto fail_jobrep;
        }

        /* Bind data to Query */
        if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &eff_cred_id) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the \"eff_cred_id\" value to the query\n", __func__);
            goto fail_jobrep;
        }
        if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &unix_uid_voms_fqan_id) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the \"unix_uid_voms_fqan_id\" value to the query\n", __func__);
            goto fail_jobrep;
        }

        /* Fail on existance */
        SQL_Exec(db_handle);

        /* Clean up */
        SQL_QueryCloseAndClean(db_handle);
    }

    /*** Insert the effective_credentials_users ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"insert into effective_credential_unix_uids "\
                             " (eff_cred_id, unix_uid_id)" \
                             " values (?, ?)") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to insert eff_cred_id and unix_uid_id into \"effective_credential_unix_uids\"\n", __func__);
        goto fail_jobrep;
    }

    /* Bind data to Query */
    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &eff_cred_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"eff_cred_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &unix_uid_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"unix_uid_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }

    /* Fail on existance */
    SQL_Exec(db_handle);

    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);

    return 0;
fail_jobrep:
    return -1;
}

int
jobrep_push_effective_credential_unix_gid(struct jr_db_handle_s *db_handle, long unix_gid_id, long eff_cred_id, int is_primary) {
    if ((db_handle == NULL) || (unix_gid_id < 0) || (eff_cred_id < 0)) {
        return -1;
    }

    /*** Insert the effective_credentials_users ***/
    if (SQL_Prepare(db_handle,
            (unsigned char *)"insert into effective_credential_unix_gids "\
                             " (eff_cred_id, unix_gid_id, is_primary)" \
                             " values (?, ?, ?)") < 0) {
        lcmaps_log(LOG_ERR, "%s: Failed to prepare a query to insert eff_cred_id, unix_gid_id and is_primary into \"effective_credential_unix_gids\"\n", __func__);
        goto fail_jobrep;
    }

    /* Bind data to Query */
    if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &eff_cred_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"eff_cred_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 2, SQL_C_LONG, SQL_INTEGER, &unix_gid_id) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"unix_gid_id\" value to the query\n", __func__);
        goto fail_jobrep;
    }
    if (SQL_BindParam(db_handle, 3, SQL_C_LONG, SQL_INTEGER, &is_primary) < 0) {
        lcmaps_log(LOG_ERR, "%s: Unable to bind the \"is_primary\" value to the query\n", __func__);
        goto fail_jobrep;
    }

    /* Fail on existance */
    SQL_Exec(db_handle);

    /* Clean up */
    SQL_QueryCloseAndClean(db_handle);

    return 0;
fail_jobrep:
    return -1;
}

int
jobrep_push_effective_credential_unix_gids(struct jr_db_handle_s *db_handle, long eff_cred_id) {
    int                    i = 0;
    gid_t                 *priGid = NULL;
    int                    cntPriGid = 0;
    gid_t                 *secGid = NULL;
    int                    cntSecGid = 0;
    struct group          *group_info = NULL;
    long                    unix_gid_id = -1;

    if ((db_handle == NULL) || (eff_cred_id < 0)) {
        return -1;
    }

    /* Add the Primary GroupID info */
    priGid = getCredentialData(PRI_GID, &cntPriGid);
    if (cntPriGid > 0) {
        group_info = getgrgid(priGid[0]);
        unix_gid_id = jobrep_get_unix_gid_id_from_gid(db_handle, priGid[0], (group_info) ? group_info->gr_name : NULL);
        if (unix_gid_id < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to insert an effective_credential_unix_gids record based on the primary GID %d(%s).\n",
                                __func__,
                                priGid[0],
                                (group_info) ? group_info->gr_name : "n/a");
            goto fail_jobrep;
        }

        if (jobrep_push_effective_credential_unix_gid(db_handle, unix_gid_id, eff_cred_id, 1) < 0) {
            lcmaps_log(LOG_ERR, "%s: Failed to push the effective_credential_unix_gid record based on the unix_gid_id %d and eff_cred_id %d\n",
                                __func__,
                                unix_gid_id,
                                eff_cred_id);
            goto fail_jobrep;
        }
    }

    /* Add the Secondary GroupID info */
    secGid = getCredentialData(SEC_GID, &cntSecGid);
    for (i = 0; i < cntSecGid; i++) {
        group_info = getgrgid(secGid[i]);
        unix_gid_id = jobrep_get_unix_gid_id_from_gid(db_handle, secGid[i], (group_info) ? group_info->gr_name : NULL);
        if (unix_gid_id < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to insert an effective_credential_unix_gids record based on the secondary GID %d(%s).\n",
                                __func__,
                                secGid[i],
                                (group_info) ? group_info->gr_name : "n/a");
            goto fail_jobrep;
        }

        if (jobrep_push_effective_credential_unix_gid(db_handle, unix_gid_id, eff_cred_id, 0) < 0) {
            lcmaps_log(LOG_ERR, "%s: Failed to push the effective_credential_unix_gid record based on the unix_gid_id %d and eff_cred_id %d\n",
                                __func__,
                                unix_gid_id,
                                eff_cred_id);
            goto fail_jobrep;
        }
    }

    return 0;
fail_jobrep:
    return -1;
}

long
jobrep_push_unix_cred(struct jr_db_handle_s *db_handle) {
    int                    i = 0;
    long                    unix_uid_id_and_res = -1;
    uid_t                 *uid = NULL;
    int                    cntUid = 0;
    gid_t                 *priGid = NULL;
    int                    cntPriGid = 0;
    gid_t                 *secGid = NULL;
    int                    cntSecGid = 0;
    struct passwd         *user_info = NULL;
    struct group          *group_info = NULL;

    if (db_handle == NULL) {
        lcmaps_log(LOG_ERR, "%s called with an empty Database handle\n");
        return -1;
    }

    /* Add the UserID info */
    uid = getCredentialData(UID, &cntUid);
    if (cntUid > 0) {
        SQL_Prepare(db_handle, (unsigned char *)"insert into unix_uids (uid, uid_name) values(?, ?)");

        /* Bind data to Query */
        if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &(uid[0])) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the UID value to the query\n", __func__);
            goto fail_jobrep;
        }
        user_info = getpwuid(uid[0]);
        if (user_info) {
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, user_info->pw_name) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Username value to the query\n", __func__);
                goto fail_jobrep;
            }
        } else {
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, "NULL") < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Username value to the query\n", __func__);
                goto fail_jobrep;
            }
        }
        /* Insert the User account */
        SQL_IgnoreErrors_Set(db_handle);
        SQL_Exec(db_handle);

        /* Clean up */
        SQL_QueryCloseAndClean(db_handle);

        /** Select the unix_uid_id from the uid and uid_name **/
        SQL_Prepare(db_handle,
                    (unsigned char *)"select unix_uid_id from unix_uids where uid = ? and uid_name = ?");

        /* Bind data to Query */
        if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &(uid[0])) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the UID value to the query\n", __func__);
            goto fail_jobrep;
        }
        /* user_info = getpwuid(uid[0]); - already got this */
        if (user_info) {
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, user_info->pw_name) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Username value to the query\n", __func__);
                goto fail_jobrep;
            }
        } else {
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, "NULL") < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Username value to the query\n", __func__);
                goto fail_jobrep;
            }
        }

        SQL_Query(db_handle);

        /* Read the ResultSet - On existence, expecting 1 TField as result, type SQL_C_LONG */
        if ((db_handle->resultset->rowCnt <= 0) || (db_handle->resultset->colCnt <= 0)) {
            lcmaps_log(LOG_ERR, "%s: No results from the query to select the unix_uid_id from \"unix_uids\"\n", __func__);
            goto fail_jobrep;
        } else if ((db_handle->resultset->rowCnt > 1) || (db_handle->resultset->colCnt > 1)) {
            lcmaps_log(LOG_ERR, "%s: Too many results returned. The query is only expected to return 1 column and 1 row.\n", __func__);
            goto fail_jobrep;
        }

        /* One result please, only one */
        if (db_handle->resultset->data[0][0].type != SQL_C_LONG) {
            lcmaps_log(LOG_ERR,
                       "%s: result for the \"unix_uids\" provided in the wrong datatype. " \
                       "Expected SQL_C_LONG (and equivalent)\n",
                       __func__);
            goto fail_jobrep;
        }

        /* Store the cert_id */
        unix_uid_id_and_res = db_handle->resultset->data[0][0].v_long;

    } else {
        /* Non-result */
        unix_uid_id_and_res = 0;
    }

    /* Add the Primary GroupID info */
    priGid = getCredentialData(PRI_GID, &cntPriGid);
    if (cntPriGid > 0) {
        SQL_Prepare(db_handle, (unsigned char *)"insert into unix_gids (gid, gid_name) values(?, ?)");

        /* Bind data to Query */
        if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &(priGid[0])) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the PriGID value to the query\n", __func__);
            goto fail_jobrep;
        }
        group_info = getgrgid(priGid[0]);
        if (group_info) {
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, group_info->gr_name) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the primary groupname value to the query\n", __func__);
                goto fail_jobrep;
            }
        } else {
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, "NULL") < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the primary groupname value to the query\n", __func__);
                goto fail_jobrep;
            }
        }
        /* Insert the User account */
        SQL_IgnoreErrors_Set(db_handle);
        SQL_Exec(db_handle);
    }

    /* Add the Secondary GroupID info */
    secGid = getCredentialData(SEC_GID, &cntSecGid);
    for (i = 0; i < cntSecGid; i++) {
        SQL_Prepare(db_handle, (unsigned char *)"insert into unix_gids (gid, gid_name) values(?, ?)");

        /* Bind data to Query */
        if (SQL_BindParam(db_handle, 1, SQL_C_LONG, SQL_INTEGER, &(secGid[i])) < 0) {
            lcmaps_log(LOG_ERR, "%s: Unable to bind the PriGID value to the query\n", __func__);
            goto fail_jobrep;
        }
        group_info = getgrgid(secGid[i]);
        if (group_info) {
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, group_info->gr_name) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the primary groupname value to the query\n", __func__);
                goto fail_jobrep;
            }
        } else {
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, "NULL") < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the primary groupname value to the query\n", __func__);
                goto fail_jobrep;
            }
        }
        /* Insert the User account */
        SQL_IgnoreErrors_Set(db_handle);
        SQL_Exec(db_handle);
    }

    return unix_uid_id_and_res;
fail_jobrep:
    return -1;
}

int
jobrep_push_certificates(struct jr_db_handle_s *db_handle, STACK_OF(X509) *px509_chain) {
    int      i = 0;
    int      depth = 0, purpose = 0;
    char    *subject_DN = NULL, *issuer_DN = NULL, *not_before_str = NULL, *not_after_str = NULL;
    time_t   not_before = 0, not_after = 0;
    X509    *cert = NULL;
    char *serialnr = NULL;

    if ((db_handle == NULL) || (px509_chain == NULL)) {
        return -1;
    }

    depth = sk_X509_num(px509_chain);
    for (i = 0; i < depth; i++) {
        if ((cert = sk_X509_value(px509_chain, i))) {
            subject_DN = X509_NAME_oneline(X509_get_subject_name(cert),NULL,0);
            if (subject_DN == NULL) {
                lcmaps_log(LOG_INFO, "%s: No Subject DN found in the certificate.\n", __func__);
                goto fail_jobrep;
            }

            issuer_DN  = X509_NAME_oneline(X509_get_issuer_name(cert),NULL,0);
            if (issuer_DN == NULL) {
                lcmaps_log(LOG_INFO, "%s: No Issuer DN found in the certificate.\n", __func__);
                goto fail_jobrep;
            }

            serialnr = jobrep_get_serialnumber_as_string(cert);
            if (serialnr == NULL) {
                lcmaps_log(LOG_INFO, "%s: No serial number found in the certificate\n", __func__);
                goto fail_jobrep;
            }

            /* Gain the purpose, CA or not */
            purpose = jobrep_grid_x509IsCA(cert);

            not_before = jobrep_asn1TimeToTimeT(ASN1_STRING_data(X509_get_notBefore(cert)),0);
            not_before_str = jobrep_time_to_string(not_before);
            if (not_before_str == NULL) {
                lcmaps_log(LOG_INFO, "%s: Conversion from a ASN1_TIME to a string failed for the Not Before time\n",
                                     __func__);
                goto fail_jobrep;
            }

            not_after  = jobrep_asn1TimeToTimeT(ASN1_STRING_data(X509_get_notAfter(cert)),0);
            not_after_str = jobrep_time_to_string(not_after);
            if (not_after_str == NULL) {
                lcmaps_log(LOG_INFO, "%s: Conversion from a ASN1_TIME to a string failed for the Not After time\n",
                                     __func__);
                goto fail_jobrep;
            }

            SQL_Prepare(db_handle,
                (unsigned char *)"insert into " \
                                 "certificates (subject, issuer, purpose, serialnr, valid_from, valid_until) " \
                                 "      values (?,       ?,      ?,       ?,        ?,          ?)");

            if (SQL_BindParam(db_handle, 1, SQL_C_CHAR, SQL_VARCHAR, subject_DN) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Subject DN value to the query\n", __func__);
                goto fail_jobrep;
            }
            if (SQL_BindParam(db_handle, 2, SQL_C_CHAR, SQL_VARCHAR, issuer_DN) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Issuer DN value to the query\n", __func__);
                goto fail_jobrep;
            }
            if (SQL_BindParam(db_handle, 3, SQL_C_LONG, SQL_INTEGER, &purpose) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Purpose value to the query\n", __func__);
                goto fail_jobrep;
            }
            if (SQL_BindParam(db_handle, 4, SQL_C_CHAR, SQL_VARCHAR, serialnr) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Serial Nr value to the query\n", __func__);
                goto fail_jobrep;
            }
            if (SQL_BindParam(db_handle, 5, SQL_C_CHAR, SQL_VARCHAR, not_before_str) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Valid From value to the query\n", __func__);
                goto fail_jobrep;
            }
            if (SQL_BindParam(db_handle, 6, SQL_C_CHAR, SQL_VARCHAR, not_after_str) < 0) {
                lcmaps_log(LOG_ERR, "%s: Unable to bind the Valid Until value to the query\n", __func__);
                goto fail_jobrep;
            }

            /* The UNIQUE statement in the DB will purposely cast an error. This query is very unlikely to fail */
            SQL_IgnoreErrors_Set(db_handle);
            SQL_Exec(db_handle);

            free(not_before_str);
            free(not_after_str);
            free(serialnr);
            free(subject_DN);
            free(issuer_DN);
        }
    }
    return 0;
fail_jobrep:
    if (not_before_str) free(not_before_str);
    if (not_after_str)  free(not_after_str);
    if (serialnr)       free(serialnr);
    if (subject_DN)     free(subject_DN);
    if (issuer_DN)      free(issuer_DN);
    return -1;
}

char *
jobrep_get_serialnumber_as_string(X509 *cert) {
    ASN1_INTEGER   *serial = NULL;
    char           *subject_DN = NULL;
    BIGNUM         *bn_serial;
    char           *serialStr = NULL;

    if (cert == NULL)
        return NULL;

    if ( (serial = X509_get_serialNumber(cert)) == NULL) {
        /* For debugging purposes extract the Subject DN */
        subject_DN = X509_NAME_oneline(X509_get_subject_name(cert),NULL,0);
        if (subject_DN) {
            lcmaps_log(LOG_WARNING, "%s: certificate passed with subject DN \"%s\" didn't contain a serial number.\n",
                       __func__, subject_DN);
            free(subject_DN);
        } else {
            lcmaps_log(LOG_WARNING, "%s: certificate passed doesn't have a serialnumber and also lacks a subject DN. " \
                                  "This is completely weird and doesn't even look like a certificate. " \
                                  "Are you a waiter because you seem to be feeding me soup?\n",
                                  __func__);
        }
        return NULL;
    }

    if ( (bn_serial = ASN1_INTEGER_to_BN(serial, NULL)) == NULL ||
	 (serialStr = BN_bn2hex(bn_serial)) == NULL )
    {
	lcmaps_log(LOG_WARNING, "%s: Cannot convert ASN1_INTEGER serial to char *", __func__);
	return NULL;
    }

    BN_clear_free(bn_serial);
    return serialStr;
}


char *
jobrep_time_to_string(time_t some_time) {
    struct tm *tmpTime = NULL;
    char *datetime = NULL;

    tmpTime = malloc(sizeof(struct tm));
    if (tmpTime == NULL)
        goto cleanup;

    gmtime_r(&some_time, tmpTime);

    datetime = malloc(sizeof(char) * 20);
    if (datetime == NULL)
        goto cleanup;

    snprintf(datetime, 20, "%04d-%02d-%02d %02d:%02d:%02d",
            tmpTime->tm_year + 1900, tmpTime->tm_mon + 1, tmpTime->tm_mday,
            tmpTime->tm_hour, tmpTime->tm_min, tmpTime->tm_sec);

    /* Done */
    if (tmpTime) free(tmpTime);

    return datetime;

    /* Failed */
cleanup:
    if (tmpTime) free(tmpTime);
    if (datetime) free(datetime);

    return NULL;
}

static time_t
jobrep_my_timegm(struct tm *tm) {
    time_t ret;
    char *tz;

    tz = getenv("TZ");
    setenv("TZ", "", 1);
    tzset();
    ret = mktime(tm);
    if (tz)
        setenv("TZ", tz, 1);
    else
        unsetenv("TZ");
    tzset();

    return ret;
}

/* ASN1 time string (in a char *) to time_t */
/**
 *  (Use ASN1_STRING_data() to convert ASN1_GENERALIZEDTIME to char * if
 *   necessary)
 */
time_t
jobrep_asn1TimeToTimeT(unsigned char *asn1time, size_t len) {
    char   zone;
    struct tm time_tm;

    memset(&time_tm,0,sizeof(struct tm));

    if (len == 0) len = strlen((char *)asn1time);

    if ((len != 13) && (len != 15)) return 0; /* dont understand */

    if ((len == 13) &&
            ((sscanf((char *) asn1time, "%02d%02d%02d%02d%02d%02d%c",
                     &(time_tm.tm_year),
                     &(time_tm.tm_mon),
                     &(time_tm.tm_mday),
                     &(time_tm.tm_hour),
                     &(time_tm.tm_min),
                     &(time_tm.tm_sec),
                     &zone) != 7) || (zone != 'Z'))) return 0; /* dont understand */

    if ((len == 15) &&
            ((sscanf((char *) asn1time, "20%02d%02d%02d%02d%02d%02d%c",
                     &(time_tm.tm_year),
                     &(time_tm.tm_mon),
                     &(time_tm.tm_mday),
                     &(time_tm.tm_hour),
                     &(time_tm.tm_min),
                     &(time_tm.tm_sec),
                     &zone) != 7) || (zone != 'Z'))) return 0; /* dont understand */

    /* time format fixups */

    if (time_tm.tm_year < 90) time_tm.tm_year += 100;
    --(time_tm.tm_mon);

   return jobrep_my_timegm(&time_tm);
}

