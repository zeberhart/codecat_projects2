/*
 * Copyright (c) Members of the IGE Project. 2012.
 * See http://ige-project.eu for details on the copyright holders.
 * For license conditions see the license file or
 */


#ifndef JOBREP_DATA_HANDLING_H
    #define JOBREP_DATA_HANDLING_H

/*****************************************************************************
                            Include header files
******************************************************************************/
#include "lcmaps_jobrep_config.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <pwd.h>

#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/x509_vfy.h>
#include <openssl/asn1.h>

#include <lcmaps/lcmaps_modules.h>
#include <lcmaps/lcmaps_arguments.h>
#include <lcmaps/lcmaps_cred_data.h>
#include <lcmaps/lcmaps_vo_data.h>

#include "jobrep_odbc_api.h"


/*****************************************************************************
                              Prototypes
******************************************************************************/

long jobrep_create_effective_credentials_main(struct jr_db_handle_s *db_handle);
int jobrep_push_compute_job_info(struct jr_db_handle_s *db_handle, long eff_cred_id, char *gatekeeper_jm_id);
int jobrep_get_voms_fqan_id_for_fqan(struct jr_db_handle_s *db_handle, char *voms_fqan);
int jobrep_push_unix_gid_voms_fqans(struct jr_db_handle_s *db_handle);
/* Next function is not used, commenting out */
/*int jobrep_get_unix_uid_id_from_uid(struct jr_db_handle_s *db_handle, uid_t uid, char *uid_name);*/
long jobrep_get_unix_gid_id_from_gid(struct jr_db_handle_s *db_handle, gid_t gid, char *gid_name);
long jobrep_get_voms_fqan_id_from_fqan(struct jr_db_handle_s *db_handle, char *voms_fqan);
long jobrep_insert_unix_uid_voms_fqans(struct jr_db_handle_s *db_handle, long voms_fqan_id, long unix_uid_id);
long jobrep_insert_unix_gid_voms_fqans(struct jr_db_handle_s *db_handle, long voms_fqan_id, long unix_gid_id, int is_primary);

int jobrep_push_voms_fqans(struct jr_db_handle_s *db_handle, long eff_cred_id);
int jobrep_push_effective_credential_unix_uid(struct jr_db_handle_s *db_handle, long unix_uid_id, long eff_cred_id);
int jobrep_push_effective_credential_unix_gid(struct jr_db_handle_s *db_handle, long unix_gid_id, long eff_cred_id, int is_primary);
int jobrep_push_effective_credential_unix_gids(struct jr_db_handle_s *db_handle, long eff_cred_id);
int jobrep_push_effective_credential_user(struct jr_db_handle_s *db_handle, long user_id, long eff_cred_id);
long jobrep_assign_userid(struct jr_db_handle_s *db_handle, STACK_OF(X509) *px509_chain, char *eec_subject_dn);
long jobrep_push_unix_cred(struct jr_db_handle_s *db_handle);
int jobrep_push_certificates(struct jr_db_handle_s *db_handle, STACK_OF(X509) *px509_chain);

char *jobrep_get_serialnumber_as_string(X509 *cert);
char *jobrep_time_to_string(time_t some_time);
time_t jobrep_asn1TimeToTimeT(unsigned char *asn1time, size_t len);

#endif /* JOBREP_DATA_HANDLING_H */
