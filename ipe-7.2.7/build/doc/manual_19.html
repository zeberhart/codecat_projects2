<!DOCTYPE html>
<html>
<!-- XML file produced from file: manual.tex
     using Hyperlatex v 15.02 (c) Otfried Cheong
     on Emacs 24.5.1, Fri Dec  9 15:05:35 2016 -->
<head>
<title>Ipe Manual -- 4.4 Group objects</title>
<meta charset="UTF-8" /><meta name="viewport" content="width=device-width,
    initial-scale=1" /><link rel="stylesheet" href="manual.css" />
<style type="text/css">
.tex-maketitle { text-align: center; }
div.tex-abstract { margin-left: 20%; margin-right: 10%; }
h3.tex-abstract  { text-align: center; }
div.tex-verse, div.tex-quote, div.tex-quotation, div.tex-math {
  margin-left : 10%; 
  margin-right : 10%;
}
.tex-normalsize { font-size: medium; }
.tex-large { font-size: large; }
.tex-Large { font-size: x-large; }
.tex-LARGE { font-size: xx-large; }
.tex-huge { font-size: xx-large; }
.tex-Huge { font-size: xx-large; }
.tex-small { font-size: small; }
.tex-footnotesize { font-size: x-small; }
.tex-scriptsize { font-size: x-small; }
.tex-tiny { font-size: xx-small; }
.tex-emph { font-style: italic; }
.tex-strong { font-weight: bold; }
.tex-bf { font-weight: bold; }
.tex-it { font-style: italic; }
.tex-tt { font-family: monospace; }
.tex-underline { text-decoration: underline; }
.tex-sf { font: sans-serif; }
.tex-sc { font-variant: small-caps; }
.tex-center { text-align: center; }
.tex-mbox { white-space: nowrap; }
.tex-verbatim, .tex-example {
  border-radius: 16px;
  box-shadow: #387BBE 0px 0px 12px 0px;
  padding: 1em 1em;
  margin-left: 1em;
  margin-right: 1em;
}
dl.tex-description {
  padding-left: 2em;
  padding-right: 2em;
}
dl.tex-description dt {
  float: left;
  clear: left;
  color: green;
  width: 200px;
}
dl.tex-description dd {
  margin-left: 200px;
  margin-right: 100px;
}
</style>

</head>
<body bgcolor="#ffffe6">
<div data-role="page">
<table width="100%" cellpadding=0 cellspacing=2><tr>
    <td bgcolor="#99ccff"><a href="manual_20.html"><img border="0" alt="4.5 Reference objects and symbols" src="next.png"></a></td><td bgcolor="#99ccff"><a href="manual_15.html"><img border="0" alt="4 Object types" src="up.png"></a></td><td bgcolor="#99ccff"><a href="manual_18.html"><img border="0" alt="4.3 Image objects" src="previous.png"></a></td><td align="center" bgcolor="#99ccff" width="100%"><span class="tex-bf">4.4 Group objects</span></td></tr></table>
<div data-role="content">
<h2>4.4 Group objects</h2>

<p>Group objects are created by selecting any number of objects and using
the <span class="tex-emph">Group</span> function from the <span class="tex-emph">Edit</span> menu.  The grouped
objects then behave like a single object.  To modify a group object,
it has to be decomposed into its parts using <span class="tex-emph">Ungroup</span>.

<h4 id="id1">Clipping</h4>
<p>You can set a <span class="tex-emph">clipping path</span> for a group.  The group will then
be clipped to this path&mdash;nothing will be drawn outside the clipping
path.  This is useful, for instance, to clip out an interesting part
of an existing drawing or bitmap.
<p>To add a clipping path, select a group as the primary selection, and a
path object as the secondary selection.  Then select <span class="tex-emph">Add
  clipping path</span> from the group's context menu.

<h4 id="id2">External links</h4>
<p>Group objects allow you to create <span class="tex-emph">links</span> to external documents
or websites.  Bring up the object menu, and use <span class="tex-emph">Set link URL</span>.
When saving your document in PDF format, every top-level group object
on a page (that is, an object that is not inside another group) will
be turned into an active link if it has a destination URL set.

<h4 id="id3">Decorations</h4>
<p>Group objects can be <span class="tex-emph">decorated</span>.  A decoration consists of one
or more path objects that are drawn around the group.  The decoration
is automatically resized to fit the bounding box of the group.
<p>To use decorations, you first need to add a
<a href="manual_27.html">stylesheet</a> to your document that defines
decoration symbols&mdash;you may want to start with the provided style
sheet <span class="tex-emph">decorations.isy</span>.  Then use the group's object menu to
choose a decoration for the group.

<h4 id="id4">Editing text in group</h4>
<p>Groups often contain some text.  For instance, a graph vertex is
nicely represented as a group consisting of a text label and either a
mark symbol such as a disk, or a path object (a circle, rectangle, or
a more complicated shape).  When drawing a graph, one can place the
vertices by copying and pasting these vertex objects, but then one
needs to set the text label in each vertex. 
<p>To make this easy, the <span class="tex-emph">Edit object</span> operation (which is
otherwise used to edit the text in text objects and the shape of path
objects) can also be used for group objects that contain at lease one
text object.  It allows you to update the text inside the
<span class="tex-emph">top-most</span> text object of the group.

<h4 id="id5">Recursive group edit</h4>
<p>Often you want to modify the contents of a group without disturbing
the rest of your drawing.  To make this easy, Ipe provides the
<span class="tex-emph">Edit group</span> operation, available either from the <span class="tex-emph">Edit</span>
menu or from the group's context menu.
<p>Group editing is implemented by un-grouping the group into a newly
created layer whose name will start with <span class="tex-emph">EDIT-GROUP</span>.  Ipe locks
all other layers, so that you can concentrate on editing the objects
in the group.  When you are done, you select <span class="tex-emph">End group edit</span>
from the <span class="tex-emph">Edit</span> menu.  Ipe will take all the objects in the group
edit layer, group them together, and place the group back in its
original layer.
<p>You can edit groups recursively: If your group contains another group
that you want to modify, you can perform another group edit operation.
Each <span class="tex-emph">End group edit</span> closes one group edit layer, until you
return to your normal drawing workflow.
<p>Group edit is not a special mode&mdash;all the state needed by Ipe to
manage editing the group is stored inside the drawing.  This means
that you can save your drawing during a group edit (also, auto-saving
works during a group edit).  If you have a document with multiple
pages, you can also start group edits on several pages in parallel,
for instance to copy and paste objects between groups.
<p>It is legal to unlock the other layers of the page so that you can
move objects into and out of the group.  You should, however, be
careful with changing anything about the layers of the page&mdash;do not
re-order or rename the layers.  If you need new layers, create them at
the end of the layer list. 
<p>When you perform the <span class="tex-emph">End group edit</span> operation, the group edit
layer must be the active layer.  If you changed the active layer, you
will have to change it back to be able to return from the group edit. 
<p>Note that group edit does not currently preserve a clipping path or a link
destination set on the group.  It does preserve the group's decoration.
</div>
</div></body></html>
