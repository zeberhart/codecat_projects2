// Simple example file for autopkgtest: 
// Reads data from file and prints to output stream.
// Author: Tatiana Malygina <merlettaia@gmail.com>

#include <iostream>
#include <zerg.h>


int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cout << "Please, provide file name with BLAST results\n";
    return 0;
  }
  zerg_open_file(argv[1]);
  // This program doesn't check if given file is not .ali, 
  // it is made for testing `libzerg` library is correctly installed and 
  // supposes that provided file exists and there is a read permission.
  
  int code = 0;
  char* value = new char[1024](); 
  // I picked buffer size at random - 
  // if you want to use libzerg in a real application, you probably should 
  // allocate more space.
  
  while (zerg_get_token(&code, &value)) {
    std::cout << "Parsed: " << code << ", value: " << value << std::endl;
  }
  zerg_close_file();
  return 0;
}

// Compile with:
// g++ -Wall -lzerg example.cpp -o example 
