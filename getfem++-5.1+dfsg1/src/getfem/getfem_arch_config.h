#ifndef _SRC_GETFEM_GETFEM_ARCH_CONFIG_H
#define _SRC_GETFEM_GETFEM_ARCH_CONFIG_H 1
 
/* src/getfem/getfem_arch_config.h. Generated automatically at end of configure. */
/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to dummy `main' function (if any) required to link to the Fortran
   libraries. */
/* #undef GETFEM_FC_DUMMY_MAIN */

/* Define if F77 and FC dummy `main' functions are identical. */
/* #undef GETFEM_FC_DUMMY_MAIN_EQ_F77 */

/* glibc backtrace function */
#ifndef GETFEM_HAVE_BACKTRACE 
#define GETFEM_HAVE_BACKTRACE  1 
#endif

/* define if the Boost library is available */
/* #undef GETFEM_HAVE_BOOST */

/* define if the Boost::System library is available */
/* #undef GETFEM_HAVE_BOOST_SYSTEM */

/* define if the Boost::Thread library is available */
/* #undef GETFEM_HAVE_BOOST_THREAD */

/* Define to 1 if you have the <cmumps_c.h> header file. */
#ifndef GETFEM_HAVE_CMUMPS_C_H 
#define GETFEM_HAVE_CMUMPS_C_H  1 
#endif

/* Define to 1 if you have the <cxxabi.h> header file. */
#ifndef GETFEM_HAVE_CXXABI_H 
#define GETFEM_HAVE_CXXABI_H  1 
#endif

/* Define to 1 if you have the <dlfcn.h> header file. */
#ifndef GETFEM_HAVE_DLFCN_H 
#define GETFEM_HAVE_DLFCN_H  1 
#endif

/* Define to 1 if you have the <dmumps_c.h> header file. */
#ifndef GETFEM_HAVE_DMUMPS_C_H 
#define GETFEM_HAVE_DMUMPS_C_H  1 
#endif

/* glibc floating point exceptions control */
#ifndef GETFEM_HAVE_FEENABLEEXCEPT 
#define GETFEM_HAVE_FEENABLEEXCEPT  1 
#endif

/* Define to 1 if you have the <inttypes.h> header file. */
#ifndef GETFEM_HAVE_INTTYPES_H 
#define GETFEM_HAVE_INTTYPES_H  1 
#endif

/* Define to 1 if you have the `mpi_cxx' library (-lmpi_cxx). */
/* #undef GETFEM_HAVE_LIBMPI_CXX */

/* Define to 1 if you have the `qhull' library (-lqhull). */
#ifndef GETFEM_HAVE_LIBQHULL 
#define GETFEM_HAVE_LIBQHULL  1 
#endif

/* Define to 1 if you have the <libqhull/qhull_a.h> header file. */
#ifndef GETFEM_HAVE_LIBQHULL_QHULL_A_H 
#define GETFEM_HAVE_LIBQHULL_QHULL_A_H  1 
#endif

/* Define to 1 if you have the `superlu' library (-lsuperlu). */
/* #undef GETFEM_HAVE_LIBSUPERLU */

/* Define to 1 if you have the <memory.h> header file. */
#ifndef GETFEM_HAVE_MEMORY_H 
#define GETFEM_HAVE_MEMORY_H  1 
#endif

/* defined if the Metis library was found and is working */
/* #undef GETFEM_HAVE_METIS */

/* Define to 1 if you have the <metis.h> header file. */
/* #undef GETFEM_HAVE_METIS_H */

/* defined if the Metis library found is older than version 4 */
/* #undef GETFEM_HAVE_METIS_OLD_API */

/* Define if you have the MPI library. */
/* #undef GETFEM_HAVE_MPI */

/* gcc style __PRETTY_FUNCTION__ macro */
#ifndef GETFEM_HAVE_PRETTY_FUNCTION 
#define GETFEM_HAVE_PRETTY_FUNCTION  1 
#endif

/* defined if the qd library was found and is working */
/* #undef GETFEM_HAVE_QDLIB */

/* Defined to 1 if Scilab is present on the system */
#ifndef GETFEM_HAVE_SCILAB 
#define GETFEM_HAVE_SCILAB  1 
#endif

/* Define to 1 if you have the <smumps_c.h> header file. */
#ifndef GETFEM_HAVE_SMUMPS_C_H 
#define GETFEM_HAVE_SMUMPS_C_H  1 
#endif

/* Define to 1 if you have the <stdint.h> header file. */
#ifndef GETFEM_HAVE_STDINT_H 
#define GETFEM_HAVE_STDINT_H  1 
#endif

/* Define to 1 if you have the <stdlib.h> header file. */
#ifndef GETFEM_HAVE_STDLIB_H 
#define GETFEM_HAVE_STDLIB_H  1 
#endif

/* Define to 1 if you have the <strings.h> header file. */
#ifndef GETFEM_HAVE_STRINGS_H 
#define GETFEM_HAVE_STRINGS_H  1 
#endif

/* Define to 1 if you have the <string.h> header file. */
#ifndef GETFEM_HAVE_STRING_H 
#define GETFEM_HAVE_STRING_H  1 
#endif

/* Define to 1 if you have the <superlu/colamd.h> header file. */
/* #undef GETFEM_HAVE_SUPERLU_COLAMD_H */

/* Define to 1 if you have the <superlu/slu_cdefs.h> header file. */
/* #undef GETFEM_HAVE_SUPERLU_SLU_CDEFS_H */

/* Define to 1 if you have the <superlu/slu_Cnames.h> header file. */
/* #undef GETFEM_HAVE_SUPERLU_SLU_CNAMES_H */

/* Define to 1 if you have the <superlu/slu_dcomplex.h> header file. */
/* #undef GETFEM_HAVE_SUPERLU_SLU_DCOMPLEX_H */

/* Define to 1 if you have the <superlu/slu_ddefs.h> header file. */
/* #undef GETFEM_HAVE_SUPERLU_SLU_DDEFS_H */

/* Define to 1 if you have the <superlu/slu_scomplex.h> header file. */
/* #undef GETFEM_HAVE_SUPERLU_SLU_SCOMPLEX_H */

/* Define to 1 if you have the <superlu/slu_sdefs.h> header file. */
/* #undef GETFEM_HAVE_SUPERLU_SLU_SDEFS_H */

/* Define to 1 if you have the <superlu/slu_zdefs.h> header file. */
/* #undef GETFEM_HAVE_SUPERLU_SLU_ZDEFS_H */

/* Define to 1 if you have the <sys/stat.h> header file. */
#ifndef GETFEM_HAVE_SYS_STAT_H 
#define GETFEM_HAVE_SYS_STAT_H  1 
#endif

/* Define to 1 if you have the <sys/times.h> header file. */
#ifndef GETFEM_HAVE_SYS_TIMES_H 
#define GETFEM_HAVE_SYS_TIMES_H  1 
#endif

/* Define to 1 if you have the <sys/types.h> header file. */
#ifndef GETFEM_HAVE_SYS_TYPES_H 
#define GETFEM_HAVE_SYS_TYPES_H  1 
#endif

/* Define to 1 if you have the <unistd.h> header file. */
#ifndef GETFEM_HAVE_UNISTD_H 
#define GETFEM_HAVE_UNISTD_H  1 
#endif

/* Define to 1 if you have the <zmumps_c.h> header file. */
#ifndef GETFEM_HAVE_ZMUMPS_C_H 
#define GETFEM_HAVE_ZMUMPS_C_H  1 
#endif

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#ifndef GETFEM_LT_OBJDIR 
#define GETFEM_LT_OBJDIR  ".libs/" 
#endif

/* Name of package */
#ifndef GETFEM_PACKAGE 
#define GETFEM_PACKAGE  "getfem" 
#endif

/* Define to the address where bug reports for this package should be sent. */
#ifndef GETFEM_PACKAGE_BUGREPORT 
#define GETFEM_PACKAGE_BUGREPORT  "" 
#endif

/* Define to the full name of this package. */
#ifndef GETFEM_PACKAGE_NAME 
#define GETFEM_PACKAGE_NAME  "getfem" 
#endif

/* Define to the full name and version of this package. */
#ifndef GETFEM_PACKAGE_STRING 
#define GETFEM_PACKAGE_STRING  "getfem 5.1" 
#endif

/* Define to the one symbol short name of this package. */
#ifndef GETFEM_PACKAGE_TARNAME 
#define GETFEM_PACKAGE_TARNAME  "getfem" 
#endif

/* Define to the home page for this package. */
#ifndef GETFEM_PACKAGE_URL 
#define GETFEM_PACKAGE_URL  "" 
#endif

/* Define to the version of this package. */
#ifndef GETFEM_PACKAGE_VERSION 
#define GETFEM_PACKAGE_VERSION  "5.1" 
#endif

/* defined if quad-doubles are to be used instead of double-double */
/* #undef GETFEM_QDLIB_USE_QUAD */

/* Define to 1 if you have the ANSI C header files. */
#ifndef GETFEM_STDC_HEADERS 
#define GETFEM_STDC_HEADERS  1 
#endif

/* Use rpc for getfem communication with matlab */
/* #undef GETFEM_USE_RPC */

/* Version number of package */
#ifndef GETFEM_VERSION 
#define GETFEM_VERSION  "5.1" 
#endif
 
/* once: _SRC_GETFEM_GETFEM_ARCH_CONFIG_H */
#endif
