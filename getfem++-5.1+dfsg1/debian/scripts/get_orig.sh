#!/bin/sh
# The script creates a tar.xz tarball from svn-repository of getfem
# atool, subversion


rm -rf getfem
echo Cloning the svn-repo
svn co svn://svn.gna.org/getfem/trunk/getfem

REVISION=`svn info getfem ./ | grep '^Revision:' | sed -e 's/^Revision: //'`
echo Revision ID is ${REVISION}

find . -name .svn -print0 | xargs -0 rm -r
rm -rf getfem/msvc2010

echo Making dfsg-compatible

find getfem/ -name \*.so | xargs rm

folderName=getfem-4.3.1~beta1~svn${REVISION}~dfsg
fileName=getfem_4.3.1~beta1~svn${REVISION}~dfsg.orig.tar.xz

mv getfem ${folderName}
echo Creating a tarball

apack ${fileName} ${folderName}
rm -rf ${folderName}
