/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * LADSPA bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

API int
lv2_dyn_manifest_open(LV2_Dyn_Manifest_Handle *handle,
		      const LV2_Feature * const * features)
{
	return bridge_load_all();
}

API int
lv2_dyn_manifest_get_subjects(LV2_Dyn_Manifest_Handle handle, FILE *fp)
{
	nabrit_manifest_print_subjects(bridge, fp);
	return 0;
}

API int
lv2_dyn_manifest_get_data(LV2_Dyn_Manifest_Handle handle, FILE *fp,
			  const char *uri)
{
	return nabrit_manifest_print_data(bridge, fp, uri);
}

API void
lv2_dyn_manifest_close(LV2_Dyn_Manifest_Handle handle)
{
}
