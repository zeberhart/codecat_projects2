/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * LADSPA bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

NACORE_PRIVATE void
plugin_load(nabrit_pluglib pluglib, const LADSPA_Descriptor *desc);

NACORE_PRIVATE void
plugin_unload(void *value, void *opaque);
