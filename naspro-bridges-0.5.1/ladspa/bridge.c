/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * LADSPA bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

NACORE_PRIVATE nabrit_bridge bridge;

NACORE_PRIVATE int
bridge_load_all()
{
	char *path;
	int err;

	bridge = nabrit_bridge_new("ladspa" MODULE_EXT);
	if (bridge == NULL)
		return errno;

	err = nabrit_util_load_all_in_env_path(bridge, "LADSPA_PATH",
					       nabrit_util_filter_by_suffix,
					       MODULE_EXT, pluglib_load, NULL);
	if (err == ENOENT)
	  {
#ifdef _WIN32
		const char *dir;
		char *path2, *path3;

		path = NULL;
		path2 = NULL;
		path3 = NULL;

		dir = nacore_env_get("AppData");
		if (dir != NULL)
		  {
			nacore_asprintf_nl(&path, "%s\\LADSPA Plugins", dir);
			nacore_env_free(dir);
			if (path == NULL)
				return ENOMEM;
		  }

		dir = nacore_env_get("ProgramFiles");
		if (dir != NULL)
		  {
			nacore_asprintf_nl(&path2, "%s\\LADSPA Plugins", dir);
			if (path2 == NULL)
			  {
				nacore_env_free(dir);
				if (path != NULL)
					free(path);
				return ENOMEM;
			  }

			nacore_asprintf_nl(&path3, "%s\\Audacity\\Plug-Ins",
					   dir);
			nacore_env_free(dir);
			if (path3 == NULL)
			  {
				free(path2);
				if (path != NULL)
					free(path);
				return ENOMEM;
			  }
		  }

		if (path != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path);
		  }

		if (path2 != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path2,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path2);
		  }

		if (path3 != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path3,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path3);
		  }
#elif defined(__HAIKU__)
		char dir[B_PATH_NAME_LENGTH + 1];
		char *path2;

		path  = NULL;
		path2 = NULL;

		if (find_directory(B_USER_ADDONS_DIRECTORY, NULL, false, dir,
				   B_PATH_NAME_LENGTH) == B_OK)
		  {
			nacore_asprintf_nl(&path, "%s/ladspa", dir);
			if (path == NULL)
			  {
				nabrit_bridge_free(bridge, NULL, NULL);
				return ENOMEM;
			  }
		  }

		if (find_directory(B_COMMON_ADDONS_DIRECTORY, NULL, false, dir,
				   B_PATH_NAME_LENGTH) == B_OK)
		  {
			nacore_asprintf_nl(&path2, "%s/ladspa", dir);
			if (path2 == NULL)
			  {
				if (path != NULL)
					free(path);
				nabrit_bridge_free(bridge, NULL, NULL);
				return ENOMEM;
			  }
		  }

		if (path != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path);
		  }

		if (path2 != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path2,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path2);
		  }
#else
		const char *home;

		home = nacore_env_get("HOME");
		if (home != NULL)
		  {
# ifdef __APPLE__
			char *path2;

			nacore_asprintf_nl(&path,
					   "%s/Library/Audio/Plug-Ins/LADSPA",
					   home);
			if (path == NULL)
			  {
				nabrit_bridge_free(bridge, NULL, NULL);
				nacore_env_free(home);
				return ENOMEM;
			  }

			nacore_asprintf_nl(&path2, "%s/.ladspa", home);
			if (path2 == NULL)
			  {
				nabrit_bridge_free(bridge, NULL, NULL);
				free(path);
				nacore_env_free(home);
				return ENOMEM;
			  }

			nabrit_util_load_all_in_dir(bridge, path,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);

			nabrit_util_load_all_in_dir(bridge, path2,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);

			free(path2);
			free(path);
# else
#  ifdef __SYLLABLE__
			nacore_asprintf_nl(&path, "%s/extensions/ladspa", home);
#  else
			nacore_asprintf_nl(&path, "%s/.ladspa", home);
#  endif
			if (path == NULL)
			  {
				nabrit_bridge_free(bridge, NULL, NULL);
				nacore_env_free(home);
				return ENOMEM;
			  }
# endif

			nacore_env_free(home);

			nabrit_util_load_all_in_dir(bridge, path,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);

			free(path);
		  }

# ifdef __SYLLABLE__
		nabrit_util_load_all_in_dir(bridge, "/system/extensions/ladspa",
			nabrit_util_filter_by_suffix, MODULE_EXT,
			pluglib_load, NULL);
# else
#  ifdef __APPLE__
		nabrit_util_load_all_in_dir(bridge,
			"/Library/Audio/Plug-Ins/LADSPA",
			nabrit_util_filter_by_suffix, MODULE_EXT,
			pluglib_load, NULL);
#  endif

		nabrit_util_load_all_in_dir(bridge, "/usr/local/lib/ladspa",
			nabrit_util_filter_by_suffix, MODULE_EXT,
			pluglib_load, NULL);

		nabrit_util_load_all_in_dir(bridge, "/usr/lib/ladspa",
			nabrit_util_filter_by_suffix, MODULE_EXT,
			pluglib_load, NULL);
# endif
#endif
		return 0;
	  }
	else if (err != 0)
	  {
		nabrit_bridge_free(bridge, NULL, NULL);
		return err;
	  }

	return err;
}

NACORE_PRIVATE void
bridge_unload_all()
{
	nabrit_bridge_free(bridge, pluglib_unload, NULL);
}
