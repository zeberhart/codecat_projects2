/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * LADSPA bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

NACORE_PRIVATE nabrit_bridge bridge;

NACORE_PRIVATE int
bridge_load_all();

NACORE_PRIVATE void
bridge_unload_all();
