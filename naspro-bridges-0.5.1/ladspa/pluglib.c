/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * LADSPA bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

NACORE_PRIVATE void
pluglib_load(void *value, void *opaque)
{
	const char *filename;
	nacore_dl_handle handle;
	LADSPA_Descriptor_Function get_desc;
	const LADSPA_Descriptor *desc;
	nabrit_pluglib pluglib;
	unsigned long i;

	filename = (const char *)value;

	handle = nacore_dl_open(filename, NULL, NULL);
	if (handle == NULL)
		return;

	*(void **)(&get_desc) = nacore_dl_sym(handle, "ladspa_descriptor", NULL,
					      NULL);
	if (get_desc == NULL)
	  {
		nacore_dl_close(handle, NULL, NULL);
		free((void *)filename);
		return;
	  }

	pluglib = nabrit_pluglib_new(bridge, filename);
	if (pluglib == NULL)
	  {
		nacore_dl_close(handle, NULL, NULL);
		free((void *)filename);
		return;
	  }

	nabrit_pluglib_set_opaque(pluglib, handle);

	for (i = 0; (desc = get_desc(i)) != NULL; i++)
		plugin_load(pluglib, desc);
}

NACORE_PRIVATE void
pluglib_unload(void *value, void *opaque)
{
	nabrit_pluglib pluglib;
	const char *filename;

	pluglib = (nabrit_pluglib)value;

	nabrit_pluglib_free_plugins(bridge, pluglib, plugin_unload, NULL);

	filename = nabrit_pluglib_get_filename(pluglib);
	free((void *)filename);

	nacore_dl_close(nabrit_pluglib_get_opaque(pluglib), NULL, NULL);
}
