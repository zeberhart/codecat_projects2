/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * DSSI bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

static void
port_free(void *value, void *opaque)
{
	nabrit_port port;

	port = (nabrit_port)value;

	if (nabrit_port_get_type(port) == nabrit_port_type_midi)
		return;

	free((void *)nabrit_port_get_symbol(port));
}

NACORE_PRIVATE void
plugin_load(nabrit_pluglib pluglib, const DSSI_Descriptor *desc)
{
	LV2_Descriptor lv2desc;
	nabrit_plugin plugin;
	nabrit_port ports[desc->LADSPA_Plugin->PortCount + 1];
	char *port_symbol;
	const char *filename, *basename, *tmp;
	size_t sep_len;
	unsigned long i;
#if ENABLE_DSSI_PRESETS
	float values[desc->LADSPA_Plugin->PortCount];
	LADSPA_Handle *instance;
	const DSSI_Program_Descriptor *prog;
	nabrit_preset preset;
	char *buf;
	unsigned long j;
#endif

	if (desc->DSSI_API_Version != 1)
		return;

	if ((desc->run_synth == NULL) && (desc->run_multiple_synths != NULL))
		return;

	lv2desc = stub_desc;

	filename = nabrit_pluglib_get_filename(pluglib);

	sep_len = strlen(nacore_fs_dir_sep);
	basename = filename;	/* make GCC happy */
	for (tmp = filename; tmp != NULL;
	     basename = tmp,
	     tmp = strstr(basename + sep_len, nacore_fs_dir_sep)) ;
	basename += sep_len;

	nacore_asprintf_nl((char **)&lv2desc.URI, "urn:dssi:%.*s:%s",
			   (int)(strlen(basename) - sizeof(MODULE_EXT) + 1),
			   basename, desc->LADSPA_Plugin->Label);
	if (lv2desc.URI == NULL)
		return;

	if (desc->LADSPA_Plugin->activate == NULL)
		lv2desc.activate = NULL;
	if (desc->LADSPA_Plugin->deactivate == NULL)
		lv2desc.deactivate = NULL;
	if (desc->run_synth != NULL)
		lv2desc.run = run_synth;

	plugin = nabrit_plugin_new(bridge, pluglib, &lv2desc);
	if (plugin == NULL)
	  {
		free((void *)lv2desc.URI);
		return;
	  }

	nabrit_plugin_set_opaque(plugin, (void *)desc);

	nabrit_plugin_set_name(plugin, desc->LADSPA_Plugin->Name);
	nabrit_plugin_set_creator(plugin, desc->LADSPA_Plugin->Maker);
	nabrit_plugin_set_rights(plugin, desc->LADSPA_Plugin->Copyright);

	nabrit_plugin_set_is_live(plugin,
		LADSPA_IS_REALTIME(desc->LADSPA_Plugin->Properties));
	nabrit_plugin_set_in_place_broken(plugin,
		LADSPA_IS_INPLACE_BROKEN(desc->LADSPA_Plugin->Properties));
	nabrit_plugin_set_hard_rt_capable(plugin,
		LADSPA_IS_HARD_RT_CAPABLE(desc->LADSPA_Plugin->Properties));

	for (i = 0; i < desc->LADSPA_Plugin->PortCount; i++)
	  {
		nacore_asprintf_nl(&port_symbol, "port%lu", i);
		if (port_symbol == NULL)
		  {
			nabrit_plugin_free_ports(plugin, port_free, NULL);
			nabrit_plugin_free(bridge, pluglib, plugin);
			return;
		  }

		ports[i] = nabrit_port_new(plugin, port_symbol,
		  LADSPA_IS_PORT_AUDIO(desc->LADSPA_Plugin->PortDescriptors[i])
		  ? nabrit_port_type_audio : nabrit_port_type_control,
		  LADSPA_IS_PORT_INPUT(desc->LADSPA_Plugin->PortDescriptors[i])
		  ? nabrit_port_direction_in : nabrit_port_direction_out);
		if (ports[i] == NULL)
		  {
			free(port_symbol);
			nabrit_plugin_free_ports(plugin, port_free, NULL);
			nabrit_plugin_free(bridge, pluglib, plugin);
			return;
		  }

		nabrit_port_set_name(ports[i],
				     desc->LADSPA_Plugin->PortNames[i]);

		if (LADSPA_IS_PORT_AUDIO(
				desc->LADSPA_Plugin->PortDescriptors[i]))
			continue;

		nabrit_port_set_reports_latency(ports[i],
			!strcmp(desc->LADSPA_Plugin->PortNames[i], "latency")
			|| !strcmp(desc->LADSPA_Plugin->PortNames[i],
				   "_latency"));
		nabrit_port_set_toggled(ports[i],
			LADSPA_IS_HINT_TOGGLED(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor));
		nabrit_port_set_sample_rate(ports[i],
			LADSPA_IS_HINT_SAMPLE_RATE(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor));
		nabrit_port_set_integer(ports[i],
			LADSPA_IS_HINT_INTEGER(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor));
		nabrit_port_set_logarithmic(ports[i],
			LADSPA_IS_HINT_LOGARITHMIC(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor));

		if (LADSPA_IS_HINT_BOUNDED_BELOW(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_min(ports[i],
			    desc->LADSPA_Plugin->PortRangeHints[i].LowerBound);
		if (LADSPA_IS_HINT_BOUNDED_ABOVE(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_max(ports[i],
			    desc->LADSPA_Plugin->PortRangeHints[i].UpperBound);

		if (!LADSPA_IS_HINT_HAS_DEFAULT(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			continue;

		if (LADSPA_IS_HINT_DEFAULT_MINIMUM(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor)
		    && !LADSPA_IS_HINT_SAMPLE_RATE(
			desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(ports[i],
			    desc->LADSPA_Plugin->PortRangeHints[i].LowerBound);
		else if (LADSPA_IS_HINT_DEFAULT_LOW(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor)
			 && !LADSPA_IS_HINT_SAMPLE_RATE(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
		  {
			if (LADSPA_IS_HINT_LOGARITHMIC(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
				nabrit_port_set_deflt(ports[i],
				  exp(0.75 * log(desc->LADSPA_Plugin->PortRangeHints[i].LowerBound)
				    + 0.25 * log(desc->LADSPA_Plugin->PortRangeHints[i].UpperBound)));
			else
				nabrit_port_set_deflt(ports[i],
				  0.75 * desc->LADSPA_Plugin->PortRangeHints[i].LowerBound
				  + 0.25 * desc->LADSPA_Plugin->PortRangeHints[i].UpperBound);
		  }
		else if (LADSPA_IS_HINT_DEFAULT_MIDDLE(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor)
			 && !LADSPA_IS_HINT_SAMPLE_RATE(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
		  {
			if (LADSPA_IS_HINT_LOGARITHMIC(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
				nabrit_port_set_deflt(ports[i],
				  exp(0.5 * (log(desc->LADSPA_Plugin->PortRangeHints[i].LowerBound)
					     + log(desc->LADSPA_Plugin->PortRangeHints[i].UpperBound))));
			else
				nabrit_port_set_deflt(ports[i],
				  0.5 * desc->LADSPA_Plugin->PortRangeHints[i].LowerBound
				  + 0.5 * desc->LADSPA_Plugin->PortRangeHints[i].UpperBound);
		  }
		else if (LADSPA_IS_HINT_DEFAULT_HIGH(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor)
			 && !LADSPA_IS_HINT_SAMPLE_RATE(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
		  {
			if (LADSPA_IS_HINT_LOGARITHMIC(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
				nabrit_port_set_deflt(ports[i],
				  exp(0.25 * log(desc->LADSPA_Plugin->PortRangeHints[i].LowerBound)
				    + 0.75 * log(desc->LADSPA_Plugin->PortRangeHints[i].UpperBound)));
			else
				nabrit_port_set_deflt(ports[i],
				  0.25 * desc->LADSPA_Plugin->PortRangeHints[i].LowerBound
				  + 0.75 * desc->LADSPA_Plugin->PortRangeHints[i].UpperBound);
		  }
		else if (LADSPA_IS_HINT_DEFAULT_MAXIMUM(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor)
			 && !LADSPA_IS_HINT_SAMPLE_RATE(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(ports[i],
			    desc->LADSPA_Plugin->PortRangeHints[i].UpperBound);
		else if (LADSPA_IS_HINT_DEFAULT_0(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(ports[i], 0.0);
		else if (LADSPA_IS_HINT_DEFAULT_1(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(ports[i], 1.0);
		else if (LADSPA_IS_HINT_DEFAULT_100(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(ports[i], 100.0);
		else if (LADSPA_IS_HINT_DEFAULT_440(
			 desc->LADSPA_Plugin->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(ports[i], 440.0);
	  }

	if (desc->run_synth != NULL)
	  {
		ports[i] = nabrit_port_new(plugin, "midi_input",
					   nabrit_port_type_midi,
					   nabrit_port_direction_in);
		if (ports[i] == NULL)
		  {
			nabrit_plugin_free_ports(plugin, port_free, NULL);
			nabrit_plugin_free(bridge, pluglib, plugin);
			return;
		  }

		nabrit_port_set_name(ports[i], "MIDI Input");
	  }

	if (desc->get_program == NULL)
		return;

#if ENABLE_DSSI_PRESETS
	/* Let's try to steal MIDI programs as presets... */

	instance = desc->LADSPA_Plugin->instantiate(desc->LADSPA_Plugin,
						    480000);
	if (instance == NULL)
		return;

	for (i = 0; (prog = desc->get_program(instance, i)) != NULL; i++)
	  {
		buf = nacore_strdup(prog->Name, NULL);
		if (buf == NULL)
			break;

		preset = nabrit_preset_new(plugin, buf);
		if (preset == NULL)
		  {
			free(buf);
			break;
		  }

		for (j = 0; j < desc->LADSPA_Plugin->PortCount; j++)
		  {
			values[j] = NAN;
			desc->LADSPA_Plugin->connect_port(instance, j,
							  values + j);
		  }

		desc->select_program(instance, prog->Bank, prog->Program);

		for (j = 0; j < desc->LADSPA_Plugin->PortCount; j++)
			if (!isnan(values[j])
			    && !LADSPA_IS_HINT_SAMPLE_RATE(
			 desc->LADSPA_Plugin->PortRangeHints[j].HintDescriptor))
				nabrit_preset_add_value(preset, ports[j],
							values[j]);
	  }

	desc->LADSPA_Plugin->cleanup(instance);
#endif
}

#if ENABLE_DSSI_PRESETS
static void
preset_free(void *value, void *opaque)
{
	free((void*)nabrit_preset_get_name((nabrit_preset)value));
}
#endif

NACORE_PRIVATE void
plugin_unload(void *value, void *opaque)
{
	nabrit_plugin plugin;
	LV2_Descriptor *desc;

	plugin = (nabrit_plugin)value;

	nabrit_plugin_free_ports(plugin, port_free, NULL);
#if ENABLE_DSSI_PRESETS
	nabrit_plugin_free_presets(plugin, preset_free, NULL);
#endif

	desc = nabrit_plugin_get_descriptor(plugin);

	free((void *)desc->URI);
}
