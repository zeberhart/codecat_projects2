/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * DSSI bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

NACORE_PRIVATE LV2_Descriptor stub_desc;

NACORE_PRIVATE void
run_synth(LV2_Handle instance, uint32_t sample_count);
