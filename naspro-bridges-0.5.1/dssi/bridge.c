/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * DSSI bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

NACORE_PRIVATE nabrit_bridge bridge;

NACORE_PRIVATE int
bridge_load_all()
{
	char *path, *path2;
	int err;

	bridge = nabrit_bridge_new("dssi" MODULE_EXT);
	if (bridge == NULL)
		return errno;

	path = NULL;
	path2 = NULL;

	err = nabrit_util_load_all_in_env_path(bridge, "DSSI_PATH",
					       nabrit_util_filter_by_suffix,
					       MODULE_EXT, pluglib_load, NULL);
	if (err == ENOENT)
	  {
#ifdef _WIN32
		const char *dir;
		char *path2;

		path = NULL;
		path2 = NULL;

		dir = nacore_env_get("AppData");
		if (dir != NULL)
		  {
			nacore_asprintf_nl(&path, "%s\\DSSI Plugins", dir);
			nacore_env_free(dir);
			if (path == NULL)
				return ENOMEM;
		  }

		dir = nacore_env_get("ProgramFiles");
		if (dir != NULL)
		  {
			nacore_asprintf_nl(&path2, "%s\\DSSI Plugins", dir);
			nacore_env_free(dir);
			if (path2 == NULL)
			  {
				if (path != NULL)
					free(path);
				return ENOMEM;
			  }
		  }

		nabrit_util_load_all_in_env_path(bridge, "LADSPA_PATH",
						 nabrit_util_filter_by_suffix,
						 MODULE_EXT, pluglib_load,
						 NULL);

		if (path != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path);
		  }

		if (path2 != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path2,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path2);
		  }
#elif defined(__HAIKU__)
		char dir[B_PATH_NAME_LENGTH + 1];

		if (find_directory(B_USER_ADDONS_DIRECTORY, NULL, false, dir,
				   B_PATH_NAME_LENGTH) == B_OK)
		  {
			nacore_asprintf_nl(&path, "%s/dssi", dir);
			if (path == NULL)
			  {
				nabrit_bridge_free(bridge, NULL, NULL);
				return ENOMEM;
			  }
		  }

		if (find_directory(B_COMMON_ADDONS_DIRECTORY, NULL, false, dir,
				   B_PATH_NAME_LENGTH) == B_OK)
		  {
			nacore_asprintf_nl(&path2, "%s/dssi", dir);
			if (path2 == NULL)
			  {
				if (path != NULL)
					free(path);
				nabrit_bridge_free(bridge, NULL, NULL);
				return ENOMEM;
			  }
		  }

		nabrit_util_load_all_in_env_path(bridge, "LADSPA_PATH",
						 nabrit_util_filter_by_suffix,
						 MODULE_EXT, pluglib_load,
						 NULL);

		if (path != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path);
		  }

		if (path2 != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path2,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path2);
		  }
#else
		const char *home;

		home = nacore_env_get("HOME");
		if (home != NULL)
		  {
# ifdef __APPLE__
			nacore_asprintf_nl(&path,
					   "%s/Library/Audio/Plug-Ins/DSSI",
					   home);
			if (path == NULL)
			  {
				nabrit_bridge_free(bridge, NULL, NULL);
				nacore_env_free(home);
				return ENOMEM;
			  }

			nacore_asprintf_nl(&path2, "%s/.dssi", home);
			if (path2 == NULL)
			  {
				nabrit_bridge_free(bridge, NULL, NULL);
				free(path);
				nacore_env_free(home);
				return ENOMEM;
			  }
# else
#  ifdef __SYLLABLE__
			nacore_asprintf_nl(&path, "%s/extensions/dssi", home);
#  else
			nacore_asprintf_nl(&path, "%s/.dssi", home);
#  endif
			if (path == NULL)
			  {
				nabrit_bridge_free(bridge, NULL, NULL);
				nacore_env_free(home);
				return ENOMEM;
			  }
# endif

			nacore_env_free(home);
		  }

		nabrit_util_load_all_in_env_path(bridge, "LADSPA_PATH",
						 nabrit_util_filter_by_suffix,
						 MODULE_EXT, pluglib_load,
						 NULL);

		if (path != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path);
		  }

		if (path2 != NULL)
		  {
			nabrit_util_load_all_in_dir(bridge, path2,
				nabrit_util_filter_by_suffix, MODULE_EXT,
				pluglib_load, NULL);
			free(path2);
		  }

# ifdef __SYLLABLE__
		nabrit_util_load_all_in_dir(bridge, "/system/extensions/dssi",
			nabrit_util_filter_by_suffix, MODULE_EXT,
			pluglib_load, NULL);
# else
#  ifdef __APPLE__
		nabrit_util_load_all_in_dir(bridge,
			"/Library/Audio/Plug-Ins/DSSI",
			nabrit_util_filter_by_suffix, MODULE_EXT,
			pluglib_load, NULL);
#  endif

		nabrit_util_load_all_in_dir(bridge, "/usr/local/lib/dssi",
			nabrit_util_filter_by_suffix, MODULE_EXT,
			pluglib_load, NULL);

		nabrit_util_load_all_in_dir(bridge, "/usr/lib/dssi",
			nabrit_util_filter_by_suffix, MODULE_EXT,
			pluglib_load, NULL);
# endif

		return 0;
#endif
	  }
	else if (err != 0)
	  {
		nabrit_bridge_free(bridge, NULL, NULL);
		return err;
	  }

	return err;
}

NACORE_PRIVATE void
bridge_unload_all()
{
	nabrit_bridge_free(bridge, pluglib_unload, NULL);
}
