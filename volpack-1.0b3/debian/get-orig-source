#!/bin/sh -e

NAME=volpack
VERSION=1.0b3
UPSTREAMTAR="$NAME"-"$VERSION".tar.Z
UPSTREAMDIR=`basename $UPSTREAMTAR .tar.Z`

patchname=get-orig-source_patch
patchfile="$patchname"
if [ ! -f "$patchfile" ] ; then
    if [ -d debian -a -f "debian/$patchname" ] ; then
	patchfile="debian/$patchname"
    else
	echo "File not found: $patchname"
	exit -1
    fi
fi
patchfile="`pwd`/$patchfile"

# To rebuild the new upstream tarball you need these
# packages installed.  This will be checked later on.
# The rationale behind this dependency is that the
# script should work for later upstream releases as well
# and thus a simple patch for Makefile.am and
# configure.in would not work
BUILDTARBALLDEPENDS="autoconf automake libtool libgconf2-dev intltool"
missingdepends=`dpkg -l ${BUILDTARBALLDEPENDS} | \
    grep -v -e "^ii" -e "^|" -e "^++" -e "^ " -e "^..[^[:space:]]" | \
    sed 's/^[a-z][a-z][[:space:]]\+\([^[:space:]]\+\)[[:space:]]\+.*/\1/'`

if [ "$missingdepends" != "" ] ; then
    echo "Please install the following packages to rebuild the upstream source tarball:"
    echo $missingdepends
    exit -1
fi

mkdir -p ../tarballs
cd ../tarballs

if [ ! -f "$UPSTREAMTAR" ] ; then
    wget http://graphics.stanford.edu/software/volpack/"$UPSTREAMTAR"
fi
tar -xzf "$UPSTREAMTAR"

# Apply patch that removes dcmtk stuff from Makefile.in and configure.in
patch -p0 < "$patchfile"

cd "$UPSTREAMDIR"
chmod 755 makeopts

# gettextize --force --copy
libtoolize --force --copy
aclocal 
automake --add-missing --force-missing --gnu
autoconf
./configure
make dist
mv "$UPSTREAMDIR".tar.gz ../"$NAME"_"$VERSION".orig.tar.gz
cd ..
rm -rf "$UPSTREAMDIR"
