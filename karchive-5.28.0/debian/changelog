karchive (5.28.0-2) unstable; urgency=medium

  * Add upstream patches for KCompressionDevice::seek:
    + Fix-KCompressionDevice-to-work-with-Qt-5.7.patch
    + Fix-my-fix-for-KCompressionDevice-seek.patch
  * Backport upstream patch:
    + Fix-Potential-leak-of-memory-pointed-to-by-limitedDev.patch

 -- Maximiliano Curia <maxy@debian.org>  Wed, 29 Mar 2017 15:09:44 +0200

karchive (5.28.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * New upstream release (5.28)
  * Bump group breaks (5.28)

 -- Maximiliano Curia <maxy@debian.org>  Fri, 18 Nov 2016 16:00:41 +0100

karchive (5.27.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * New upstream release (5.27)
  * Bump group breaks (5.27)

 -- Maximiliano Curia <maxy@debian.org>  Sat, 15 Oct 2016 16:48:16 +0200

karchive (5.26.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * Bump group breaks (5.26)

 -- Maximiliano Curia <maxy@debian.org>  Thu, 29 Sep 2016 11:55:32 +0200

karchive (5.25.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * Bump group breaks (5.25)

 -- Maximiliano Curia <maxy@debian.org>  Thu, 18 Aug 2016 17:20:10 +0200

karchive (5.24.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake
  
  [ Maximiliano Curia ]
  * New upstream release.
    -  Fixes: CVE-2016-6232

 -- Maximiliano Curia <maxy@debian.org>  Tue, 19 Jul 2016 10:08:16 +0200

karchive (5.23.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake
  * Update symbols files from buildds logs (5.22.0-1).

 -- Maximiliano Curia <maxy@debian.org>  Sat, 18 Jun 2016 08:05:10 +0200

karchive (5.22.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update symbols files from buildds logs (5.21.0-1).
  * Update symbols files.
  * Update build-deps and deps with the info from cmake

 -- Maximiliano Curia <maxy@debian.org>  Thu, 19 May 2016 00:19:49 +0200

karchive (5.21.0-1) experimental; urgency=medium

  [ Maximiliano Curia ]
  * Replace the "Historical name" ddeb-migration by its "Modern, clearer" replacement dbgsym-migration.
  * Add upstream metadata (DEP-12)
  * debian/control: Update Vcs-Browser and Vcs-Git fields
  * Update acc test script
  * uscan no longer supports more than one main upstream tarball being listed

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake
  * Update symbols files.
  * Bump Standards-Version to 3.9.8

 -- Maximiliano Curia <maxy@debian.org>  Thu, 05 May 2016 15:04:24 +0200

karchive (5.19.0-1) experimental; urgency=medium

  * New upstream release (5.19.0).
  * Update symbols files from buildds logs (5.18.0-1).

 -- Maximiliano Curia <maxy@debian.org>  Sat, 13 Feb 2016 15:16:39 +0100

karchive (5.18.0-1) experimental; urgency=medium

  * New upstream release (5.17.0).
  * Update symbols files from buildds logs (5.16.0-1).
  * New upstream release (5.18.0).

 -- Maximiliano Curia <maxy@debian.org>  Wed, 27 Jan 2016 13:34:38 +0100

karchive (5.16.0-1) unstable; urgency=medium

  * New upstream release (5.16.0).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 30 Nov 2015 12:13:07 +0100

karchive (5.15.0-1) unstable; urgency=medium

  * New upstream release (5.15.0).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 09 Oct 2015 19:16:51 +0200

karchive (5.14.0-1) unstable; urgency=medium

  * New upstream release (5.14.0).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 15 Sep 2015 13:50:01 +0200

karchive (5.13.0-1) unstable; urgency=medium

  * New upstream release (5.13.0).
  * Update symbols files from buildds logs (5.12.0-1).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 27 Aug 2015 18:36:01 +0200

karchive (5.12.0-1) unstable; urgency=medium

  * New upstream release (5.12.0).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 09 Jul 2015 12:44:09 +0200

karchive (5.11.0-2) unstable; urgency=medium

  * Update symbols files.

 -- Maximiliano Curia <maxy@debian.org>  Thu, 02 Jul 2015 20:38:37 +0200

karchive (5.11.0-1) unstable; urgency=medium

  * New upstream release (5.10.0).
  * Update symbols files from buildds logs (5.9.0-1).
  * New upstream release (5.11.0).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 26 Jun 2015 18:59:43 +0200

karchive (5.9.0-1) experimental; urgency=medium

  * New upstream release (5.9.0).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Apr 2015 08:26:38 +0200

karchive (5.9.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 13 Apr 2015 22:17:34 +0200

karchive (5.8.0-1) experimental; urgency=medium

  * New upstream release (5.8.0).

 -- Maximiliano Curia <maxy@debian.org>  Sat, 14 Mar 2015 22:27:10 +0100

karchive (5.8.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 17 Mar 2015 15:29:21 +0100

karchive (5.7.0-1) experimental; urgency=medium

  * New upstream release (5.7.0).
  * Update build dependencies.

 -- Maximiliano Curia <maxy@debian.org>  Fri, 27 Feb 2015 23:31:44 +0100

karchive (5.7.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Harald Sitter <sitter@kde.org>  Tue, 10 Feb 2015 17:00:04 +0100

karchive (5.6.0-1) experimental; urgency=medium

  * New upstream release (5.6.0).
  * Bump build dependencies to build against the experimental versions
  * Bump Standards-Version to 3.9.6, no changes needed
  * Bump qtbase5-dev build dep version to follow the cmake check.
  * Be a bit more specific for the KArchive headers.
  * Add basic autopkgtests support.
  * Update copyright information.

 -- Maximiliano Curia <maxy@debian.org>  Wed, 21 Jan 2015 16:08:21 +0100

karchive (5.6.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 06 Jan 2015 20:17:18 +0100

karchive (5.5.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 15 Dec 2014 11:10:13 +0100

karchive (5.4.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Fri, 07 Nov 2014 15:37:25 +0100

karchive (5.3.0-0ubuntu1) utopic; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 07 Oct 2014 11:33:32 +0100

karchive (5.2.0-0ubuntu1) utopic; urgency=medium

  * New upstream release
  * Use pkg-kde-tools version 3 scripts

 -- Jonathan Riddell <jriddell@ubuntu.com>  Mon, 22 Sep 2014 19:47:50 +0200

karchive (5.1.0-0ubuntu1) utopic; urgency=medium

  [ Scarlett Clark ]
  * Improve copyright
  * Update extra-cmake-modules depends version

  [ José Manuel Santamaría Lema ]
  * Update libraries *.install paths so a soname bump doesn't go
    unnoticed.

  [ Scarlett Clark ]
  * New upstream release

 -- Scarlett Clark <scarlett@scarlettgatelyclark.com>  Tue, 05 Aug 2014 17:16:46 +0200

karchive (5.0.0-1) unstable; urgency=medium

  [ Maximiliano Curia ]
  * Update Maintainers fields.
  * Update VCS fields.
  * Update debhelper build-depends versioning
  * Update packages descriptions
  * Set copyright source uri to the upstream git repository
  * Update watch file
  * Update copyright information

 -- Maximiliano Curia <maxy@debian.org>  Tue, 15 Jul 2014 12:38:51 +0200

karchive (5.0.0-0ubuntu1) utopic; urgency=medium

  * Initial stable upstream release

 -- Scarlett Clark <scarlett@scarlettgatelyclark.com>  Tue, 08 Jul 2014 15:53:00 +0200
