/*
 *  Copyright (c) 2004-2013 Gert Wollny <gw.fossdev@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  As an exception to this license, "NEC C&C Research Labs" may use
 *  this software under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation.
 *
 */

#ifndef __vector3d_h
#define __vector3d_h

#include <glib.h>
#include <glib-object.h>
#include <mialm/miaxmlio.h>


#define MIA_TYPE_VECTOR3D (mia_vector3d_get_type())
#define MIA_VECTOR3D(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_VECTOR3D, MiaVector3d))
#define MIA_VECTOR3D_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIA_TYPE_VECTOR3D, MiaVector3dClass))
#define MIA_IS_VECTOR3D(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIA_TYPE_VECTOR3D))
#define MIA_IS_VECTOR3D_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIA_TYPE_VECTOR3D))
#define MIA_VECTOR3D_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIA_TYPE_VECTOR3D, MiaVector3dClass))


typedef struct _MiaVector3dClass MiaVector3dClass;
struct _MiaVector3dClass
{
	GObjectClass parent_class;
};


/**
 * MiaVector3d:
 *
 * This defines a 3D vector with elements x,y,z. 
 *
 */
typedef struct _MiaVector3d MiaVector3d;
struct _MiaVector3d {
        /*< private >*/
	GObject parent;
	
	/*< public >*/
	gfloat x, y, z;
};


enum
{
	mia_vector3d_prop_x = 1,
	mia_vector3d_prop_y,
	mia_vector3d_prop_z,
	mia_vector3d_prop_counter
};

G_BEGIN_DECLS

GType mia_vector3d_get_type (void);

MiaVector3d *mia_vector3d_new (gfloat x, gfloat y, gfloat z);

MiaVector3d *mia_vector3d_set (MiaVector3d * self, gfloat x, gfloat y, gfloat z);

MiaVector3d *mia_vector3d_dup (const MiaVector3d *orig);

MiaVector3d *mia_vector3d_normalize (MiaVector3d * self);

gfloat mia_vector3d_dot(const MiaVector3d *a, const MiaVector3d *b);

gfloat mia_vector3d_get_norm (MiaVector3d * self);

MiaVector3d *mia_vector3d_copy (MiaVector3d * dest, const MiaVector3d * src);

MiaVector3d *mia_vector3d_scale (MiaVector3d * self, gfloat f);

MiaVector3d *mia_vector3d_addup(MiaVector3d * self, const MiaVector3d * other);

MiaVector3d *mia_vector3d_add(const MiaVector3d * self, const MiaVector3d * other, MiaVector3d *result);

gboolean mia_vector3d_equal(const MiaVector3d *a, const MiaVector3d *b); 

/*gboolean mia_vector3d_test (void);*/

void
mia_vector3d_xmlio_read(ParserState * state, const gchar *property);

gboolean
mia_vector3d_xmlio_write (xmlNodePtr parent, xmlNsPtr ns, const gchar * tag,
		      const MiaVector3d * v);
G_END_DECLS

#endif
