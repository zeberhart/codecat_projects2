/*
 *  Copyright (c) 2004-2013 Gert Wollny <gw.fossdev@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  As an exception to this license, "NEC C&C Research Labs" may use
 *  this software under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation.
 *
 */

#ifndef __landmark_h
#define __landmark_h

#include <glib.h>
#include <glib-object.h>

#include <mialm/miavector3d.h>
#include <mialm/miaquaternion.h>
#include <mialm/miacamera.h>

extern const char mia_landmark_location_property[];
extern const char mia_landmark_isovalue_property[];
extern const char mia_landmark_camera_property[];
extern const char mia_landmark_name_property[];
extern const char mia_landmark_picfile_property[];



typedef struct _MiaLandmark MiaLandmark;
typedef struct _MiaLandmarkClass MiaLandmarkClass;

#define MIA_TYPE_LANDMARK (mia_landmark_get_type())
#define MIA_LANDMARK(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_LANDMARK, MiaLandmark))
#define MIA_LANDMARK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), MIA_TYPE_LANDMARK, MiaLandmarkClass))
#define MIA_IS_LANDMARK(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), MIA_TYPE_LANDMARK))
#define MIA_IS_LANDMARK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MIA_TYPE_LANDMARK))
#define MIA_LANDMARK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), MIA_TYPE_LANDMARK, MIA_TYPE_LANDMARK, MiaLandmarkClass))

extern const ParserTags landmark_parser_tags[];

struct _MiaLandmarkClass {
	GObjectClass parent;
};

G_BEGIN_DECLS 

GType mia_landmark_get_type ();

MiaLandmark *mia_landmark_new (const gchar * name,
			       MiaVector3d * location, float iso,
			       MiaCamera * camera, gchar *picfile);

void mia_landmark_set_name (MiaLandmark * self, const gchar* name);

const gchar *mia_landmark_get_name (const MiaLandmark * self);

void mia_landmark_set_location (MiaLandmark * self, MiaVector3d * location);

MiaVector3d *mia_landmark_get_location (const MiaLandmark * self);

gfloat mia_landmark_get_iso_value (const MiaLandmark * self);

void mia_landmark_set_iso_value (MiaLandmark * self, gfloat iso);

void mia_landmark_set_camera (MiaLandmark * self, MiaCamera * camera);

MiaCamera *mia_landmark_get_camera (const MiaLandmark * self);

void mia_landmark_set_picfile (MiaLandmark * self, const gchar * picfile);

const gchar *mia_landmark_get_picfile (const MiaLandmark * self);

gboolean
mia_landmark_xmlio_write (xmlNodePtr parent, xmlNsPtr ns, const MiaLandmark * lm);

G_END_DECLS

#endif
