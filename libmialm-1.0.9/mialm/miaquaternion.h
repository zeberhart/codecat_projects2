/*
 *  Copyright (c) 2004-2013 Gert Wollny <gw.fossdev@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  As an exception to this license, "NEC C&C Research Labs" may use
 *  this software under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation.
 *
 */

#ifndef __quaternion_h
#define __quaternion_h

#include <mialm/miavector3d.h>


#define MIA_TYPE_QUATERNION (mia_quaternion_get_type())
#define MIA_QUATERNION(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_QUATERNION, MiaQuaternion))
#define MIA_QUATERNION_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIA_TYPE_QUATERNION, MiaQuaternionClass))
#define MIA_IS_QUATERNION(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIA_TYPE_QUATERNION))
#define MIA_IS_QUATERNION_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIA_TYPE_QUATERNION))
#define MIA_QUATERNION_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIA_TYPE_QUATERNION, MiaQuaternionClass))

typedef struct _MiaQuaternion MiaQuaternion;
typedef struct _MiaQuaternionClass MiaQuaternionClass;

struct _MiaQuaternionClass
{
	MiaVector3dClass parent_class;
};


/**
 * MiaQuaternion:
 *
 * This defines a Quaternion and is used to describe a rotation in 3D space.
 *
 */
struct _MiaQuaternion
{
	MiaVector3d parent;
	/* add your class instance elements here */
	gfloat r;
};

/**
 * RotMatrix:
 *
 * A 4x4 matrix used for rotations.
 *
 */
typedef float RotMatrix[4][4];

G_BEGIN_DECLS

GType mia_quaternion_get_type (void);

MiaQuaternion *mia_quaternion_new (gfloat x, gfloat y, gfloat z, gfloat w);

MiaQuaternion *mia_quaternion_new_from_eulerrot (const MiaVector3d * axis,
						 gfloat angle);

MiaQuaternion *mia_quaternion_copy(MiaQuaternion *dest, const MiaQuaternion *src);


MiaQuaternion *mia_quaternion_dup(const MiaQuaternion *org);

void mia_quaternion_get_rotation(MiaQuaternion *self, RotMatrix m);

gboolean mia_quaternion_equal(const MiaQuaternion *a, const MiaQuaternion *b);

void mia_quaternion_normalize(MiaQuaternion *self);

gfloat mia_quaternion_norm(const MiaQuaternion *self);

MiaQuaternion *
mia_quaternion_multby (MiaQuaternion * self, MiaQuaternion * other);

void 
mia_quaternion_xmlio_read(ParserState * state, const gchar *property);

gboolean
mia_quaternion_xmlio_write(xmlNodePtr root, xmlNsPtr ns, const gchar * tag,
			 const MiaQuaternion * q);

gboolean mia_quaternion_test (void);

G_END_DECLS

#endif
