/*
 *  Copyright (c) 2004-2013 Gert Wollny <gw.fossdev@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  As an exception to this license, "NEC C&C Research Labs" may use
 *  this software under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation.
 *
 */

#include <libxml/parser.h>
#include <libxml/xmlmemory.h>

#include <mialm/mialandmarkio.h>
#include <mialm/miaxmlio.h>
#include <locale.h>

/**
 * SECTION:mialandmarkio
 * @Short_description: Functions related to the in/output of landmark lists. 
 * @Title: Landmark list in- and output 
 * 
 * These functions handle the in- and output of landmark lists to and from XML files. 
 */


enum EParserTagId {
	PTI_START = 0,
	PTI_LIST,
	PTI_LISTNAME,
	PTI_LANDMARK,
	PTI_PICFILE,
	PTI_NAME,
	PTI_LOCATION,
	PTI_ISOVALUE,
	PTI_CAMERA,
	PTI_CLOCATION,
	PTI_CROTATION,
	PTI_CZOOM, 
	PTI_NUMSTATES
};

void __cleanup_parserstate (ParserState * UNUSED(state))
{

}

static const char list_ctag[] = "list";

static void
__start_landmarklist (ParserState * state, const xmlChar ** UNUSED(attrs))
{
	state->pss->data = state->data; 
}



const ParserTags parser_tags[] = {
	{list_ctag, __start_landmarklist, NULL, NULL, landmarklist_parser_tags},
	END_PARSER_TAGS
};


/**
 * mia_landmarklist_save:
 * @lml: a landmark list
 * @filename: a file name string 
 * 
 * Save the landmark list to an XML file. 
 *
 * Returns: #TRUE if saving was successful, #FALSE otherwise
 */
gboolean
mia_landmarklist_save (MiaLandmarklist * lml, const gchar * filename)
{
	xmlDocPtr doc;
	xmlNodePtr root;
	gboolean result = TRUE;

	GString *old_locale = g_string_new(setlocale(LC_NUMERIC,NULL));
	setlocale(LC_NUMERIC, "C");
	
	doc = xmlNewDoc ((const xmlChar *)"1.0");
	g_return_val_if_fail (doc, FALSE);

	root = xmlNewDocNode (doc, NULL, (const xmlChar *)list_ctag, NULL);
	if (root) {
		if (mia_landmarklist_xmlio_write(root, NULL, lml)) {
			xmlDocSetRootElement (doc, root);
			xmlKeepBlanksDefault (0);
			result = xmlSaveFormatFileEnc (filename, doc,"ISO-8859-1", 1) > 0;
		}
	}
	xmlFreeDoc (doc);
	
	setlocale(LC_NUMERIC, old_locale->str);
	g_string_free(old_locale, TRUE);
	
	return result;
}

/**
 * mia_landmarklist_new_from_file:
 * @filename: a file name string 
 * 
 * Create and load a landmark list from an XML file. 
 *
 * Returns: the new #MiaLandmarklist if loading was successful and NULL otherwise
 */
MiaLandmarklist *
mia_landmarklist_new_from_file (const gchar * filename)
{
	gint result; 
	
	MiaLandmarklist *data = mia_landmarklist_new (NULL);
	
	GString *old_locale = g_string_new(setlocale(LC_NUMERIC,NULL));
	
	setlocale(LC_NUMERIC, "C");
	result = xml_sax_parse (filename,  parser_tags, data);
	setlocale(LC_NUMERIC, old_locale->str);
	g_string_free(old_locale, TRUE);
	
	if (!result) {
		return data;
	}else {
		g_object_unref (data);
		return NULL;
	}
}
