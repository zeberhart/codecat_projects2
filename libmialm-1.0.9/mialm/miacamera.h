/*
 *  Copyright (c) 2004-2013 Gert Wollny <gw.fossdev@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  As an exception to this license, "NEC C&C Research Labs" may use
 *  this software under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation.
 *
 */
 
#ifndef __camera_h
#define __camera_h

#include <glib.h>
#include <glib-object.h>
#include <mialm/miaquaternion.h>

#define MIA_TYPE_CAMERA (mia_camera_get_type())
#define MIA_CAMERA(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_CAMERA, MiaCamera))
#define MIA_CAMERA_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIA_TYPE_CAMERA, MiaCameraClass))
#define MIA_IS_CAMERA(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIA_TYPE_CAMERA))
#define MIA_IS_CAMERA_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIA_TYPE_CAMERA))
#define MIA_CAMERA_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst),MIA_TYPE_CAMERA, MiaCameraClass))


typedef struct _MiaCameraClass MiaCameraClass;
/**
 * MiaCameraClass:
 *
 * GObject class type for the MiaCamera
 *
 */
struct _MiaCameraClass {
	/*< private >*/
	GObjectClass parent;
};


typedef struct _MiaCamera MiaCamera;

GType mia_camera_get_type(void);

extern const ParserTags camera_parser_tags[];

G_BEGIN_DECLS

MiaCamera *mia_camera_new(MiaVector3d *loc,
			  MiaQuaternion *rot,
			  gfloat zoom);



gboolean mia_camera_test(void);

gfloat mia_camera_get_zoom(const MiaCamera *self);

void mia_camera_set_zoom(MiaCamera *self, gfloat zoom);

MiaVector3d *mia_camera_get_location(const MiaCamera *self);

void mia_camera_set_location(MiaCamera *self, MiaVector3d *loc);

MiaQuaternion *mia_camera_get_rotation(const MiaCamera *self);

void mia_camera_set_rotation(MiaCamera *self, MiaQuaternion *rot);

gfloat mia_camera_get_dist(const MiaCamera *self);

void mia_camera_set_dist(MiaCamera *self, gfloat dist);

MiaCamera *mia_camera_dup(MiaCamera *orig);

MiaCamera *mia_camera_copy(MiaCamera *dest, const MiaCamera *orig);


gboolean
mia_camera_xmlio_write(xmlNodePtr root, xmlNsPtr ns, const gchar * tag,
		      const MiaCamera *c);
		      

		      
G_END_DECLS

#endif
